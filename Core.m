(* ::Package:: *)

(* :Title:  NLControl`Core` *)
(* :Context: NLControl`Core` *)
(* :Author: Maris Tonso *)
(* :Package Version: 2021 *)
(* :Mathematica Version: 11.3, 12 *)
(* :Institution: Department of Software Science at Tallinn University of Technology, Estonia *)

BeginPackage["NLControl`Core`"]


(* ::Subsection:: *)
(*Usage messages*)


(*Control systems*)
StateSpace::usage = "state space equations"
StateEquations::usage = "state space equations in the form x'=f(x,u)"
StateDefinitions::usage = "Option for StateSpace, indicating the last state transformation"
IO::usage = "io equations"
TimeDerivative::usage = "indicates the time-derivative system."
Shift::usage = "indicates the shift/discrete time system."


SpanK::usage = "SpanK[mat, coords] - Represents a subspace Spanned over the set of one-forms (codistribution)."
ExactSpanK::usage = "Set of Exact one-forms"
SeparatedSpanK::usage = ""

SpanKVectorFields::usage = "SpanKVectorField[mat, coords] - Represents a subspace Spanned over the set vector fields (distribution)"


De::usage = "De[A, {x}] represents a differential one-form A \[DifferentialD]x. De[A, {x, y}] is a two-form A \[DifferentialD]x \[Wedge] \[DifferentialD]y."


(*Pseduo-linear maps - shift and derivative*)
PseudoLinearMap::usage = "applies pseudo-linear operator to expr"
ForwardShift::usage = "computes forward shift of the expr"
BackwardShift::usage = "backward shift "
PseudoD::usage = "computes pseudo-derivative "
MaxPLMOrder::usage = "finds maximal time shift /derivative of the expression."
MinPLMOrder::usage = "finds minimal time shift or derivative of the expression."

IdentityShiftQ::usage = "Gives true, if the shift operator is identity."
ZeroDerivativeQ::usage = "Gives true, if the derivative operator is zero."
ClassicStateQ::usage = "Tests if the system has a classical state space form"

PopovFormQ::usage = "Tests if io equations are in popov form"


NLCModelToRules::usage = "Tramsforms model ino the set of rules"
NLCModelToEquations::usage = "Tramsforms model into the set of equations"


NewVariables::usage = "a little helpful fucntion, which composes a list of new variables from a pure function."
ChangeOfVariables::usage = "carries on change of variables in state-space system."
PrintInfo::usage = "an option."


ModelToRules::usage = "Converts StateSpace and IO objects into the form of rules."
DetectVariables::usage = "DetectVariables[expr, t, type]"
RemoveAppliedOperator::usage = "RemoveAppliedOperator[x[t+i], t, Shift]->x[t], RemoveAppliedOperator[x'[t], t, TimeDerivative]->x[t]"


(*Generalized and Extended state equations*)
FromIOToGeneralizedState::usage = "finds generalized state equations"
ExtendedState::usage = "finds extended state equations"


(*Constructing Inversive closure. Only for discrete-time systems*)
Submersivity::usage = "tests if the system x(t+1) = f defines a submersion."
NegativeTimeShifts::usage = "finds all possible sets of variables, which may have negative time shifts."
BackwardShiftOperator::usage = "finds all possible backwardshit functions."



(*BookForm*)
BookForm::usage  = "Prints an expression in traditional user-friendly form."
TimeArgument::usage = "is an option for BookForm"
Subscripted::usage = "value for TimeArgument"
Superscripted::usage = "value for TimeArgument"
TimeArgumentSymbol::usage = "Option for BookForm, determines the symbol used for time."
Formalt::usage = "Default value of the TimeArgumentSymbol"
PolynomialVariableSymbol::usage = "Option for BookForm, 
	determines the symbol used for polynomial variable, if Ore ring is not given."
FormalZ::usage = "Default value of the PolynomialVariableSymbol" 
RowLabels::usage = "Left hand labels for the rows of BookForm argument. Should be either list of pure function. For instance,
RowLabels->Subscript[H,#]& generates
Subscript[H,1]=...\n
Subscript[H,2]=..." (*Add ot master*)
 (*BookForm - internal functions*)
RowLabelsStartIndex::usage = "Determines the index of the label of the first row. Default is 1."
ReplaceTimeArgumentBoxes::usage = "internal formatting function"
CoefficientBoxes::usage = "internal formatting function"
CoordinateBoxes::usage = "internal formatting function"
PolynomialBoxes::usage = "internal formatting function"
AllowRealNumbers::usage = "Option for BookForm. Causes StateSpace and IO objects to be printed with real numbers instead of rational numbers."


(*TeX exporting and GUI related*)
BookFormHolder::usage = "Holds NLControl expression and time t (or OreRing R) together for BookForm. For GUI only."
GuiForm::usage = "Formated output for graphic user interface. GuiForm is applied to NLControl functions rather than objects."
ToNLCExpression::usage = "Converts LaTeX string to Mathematica/NLControl expression"
NLCToTeX::useage = "Converts Mathematica (NLControl) expression to LaTeX string"
ConstructGUI::usage = "Formating data for GUI"
SubmersivityGUI::usage = "Formating Data for GUI";
GUIFormat::usage ="GUIFormat[data] -> (Pure Mathematica, NLCToTeX[Bookform[neData,TimeArgument->False]], NLCToTeX[Bookform[neData]]"
ConvertToSubscripted::usage = "Converts variables xi into Subscript[x,i]"


(* ::Subsection:: *)
(*Begin programs*)


Begin["`Private`"]

Off[General::spell1]
Off[General::spell]
epsilon=10.^-15;


(* ::Subsection:: *)
(*Control Systems*)


(* ::Subsubsection:: *)
(*StateSpace & IO*)


StateSpace::missingargs = "Missing arguments added.";
Clear[StateSpace]
StateSpace[f:Except[_List|_Pattern|_Blank|_BlankSequence], rest__] := StateSpace[{f}, rest]
StateSpace[f_, x_[t_], Ut_, t_Symbol, rest__] := StateSpace[f, {x[t]}, Ut, t, rest] (*t must be specified*)
StateSpace[f_, Xt_, u_[t_], t_, rest__] := StateSpace[f, Xt, {u[t]}, t, rest]
StateSpace[f_, Xt_, Ut_, t_, type_Symbol, opts___Rule] := StateSpace[f, Xt, Ut, t, {}, {}, type, opts]
StateSpace[f_List, Xt_, Ut_, t_, h:Except[_List|_Pattern], rest__] := StateSpace[f, Xt, Ut, t, {h}, rest]
StateSpace[f_, Xt_, Ut_, t_, h_, y_[t_], type_, opts___Rule] := StateSpace[f, Xt, Ut, t, h, {y[t]}, type, opts]

StateSpace[f_, Xt_, Ut_, t_, h_, rest__] :=
	StateSpace[f/.x_Real :> Rationalize[x,10^-10], Xt, Ut, t, h/.x_Real :> Rationalize[x,10^-10], rest
	] /;!FreeQ[{f, h}, _Real]


Options[StateSpace] = { StateDefinitions -> {} };


RemoveAppliedOperator[expr_, t_, Shift] := expr/.t+n_->t
RemoveAppliedOperator[expr_, t_, TimeDerivative] := expr/.Derivative[n_][x_][t]->x[t]

StateEquations[eqs:{__Equal}, Ut_,t_Symbol, outputEqs_, op_, args2___]:=
Module[{Xt, Xt1, f},
	{Xt1,f} = Transpose[List@@@eqs];
	Xt = RemoveAppliedOperator[Xt1, t, op];
	StateEquations[f, Xt, Ut, t, outputEqs, op, args2]
]

StateEquations[f_, Xt_, Ut_, t_Symbol, {}, args___] := StateSpace[f, Xt, Ut, t, {}, {}, args]

StateEquations[f_, Xt_, Ut_, t_Symbol, outputEq_Equal, args___] := StateEquations[f, Xt, Ut, t, {outputEq}, args]

StateEquations[f_, Xt_, Ut_, t_Symbol, outputEqs:{__Equal}, args___]:= Module[{Yt, h},
	{Yt, h} = Transpose[List@@@outputEqs];
	StateSpace[f, Xt, Ut, t, h, Yt, args]
]


IO::missingargs = "Missing arguments added.";

IO[eq:_Equal|_Rule, rest__] := IO[{eq}, rest]
IO[eqs_, u_[t_], Yt_, t_, type_, opts___Rule] := IO[eqs, {u[t]}, Yt, t, type, opts]
IO[eqs_, Ut_List, y_[t_], t_, type_] := IO[eqs, Ut, {y[t]}, t, type]

IO[eqs_, Ut_, Yt_, t_, type_] := 
	IO[eqs/.x_Real:>Rationalize[x,10^-10], Ut, Yt, t, type] /; !FreeQ[eqs,_Real]


(* ::Subsubsection:: *)
(*ModelToRules*)


(* 15.09.2015 *)
Clear[ModelToRules]
ModelToRules[ ioeq:IO[ {__Rule}, __ ]] := ioeq
ModelToRules[ ioeq:IO[ Null, __ ]] := ioeq (* Only for OrePolynomialsGUI *)
ModelToRules[ IO[ eqs:{__Equal}, Ut_, Yt_,  t_, type_ ]] := 
Module[{ni, Ytni, rules}, 
	ni = MaxPLMOrder[eqs, #, type]& /@ Yt;
	Ytni = MapThread[ PseudoLinearMap[#1, {t,#2}, type]&, {Yt,ni} ];
	rules = Solve[eqs, Ytni];
	IO[rules[[1]], Ut, Yt, t, type] /; rules=!={} && Length@rules[[1]]==Length@Yt
]

(* 01.01.2013 *)
ModelToRules[ steq:StateSpace[{Rule..}, __]] := steq
ModelToRules[ StateSpace[f_List, Xt_,Ut_,t_, h_, Yt_, type_, opts___Rule]] :=
	StateSpace[ Thread[ PseudoLinearMap[Xt, t, type] -> f], Xt, Ut, t, h, Yt, type, opts]


(* ::Subsubsection:: *)
(*NLCModelToRules*)


(*Transforms SateSpace and IO equations into list of rules 
input: StateSpace, IO (\[Equal] and ->)
output:  list of rules 
usage: DefineOreRing*)
Clear[NLCModelToRules]
NLCModelToRules[ ioeq:IO[ eq:{__Rule}, __ ]] := eq
(*Kui ilmutatud v\[OTilde]rrandit saab leida vahetult solve abil. Aga bookFormi jaoks pole seda ju vaja!! *)
NLCModelToRules[ IO[ eqs:{__Equal}, Ut_, Yt_,  t_, type_ ]] := 
Module[{ni, Ytni, rules}, 
	ni = MaxPLMOrder[eqs, #, type]& /@ Yt;
	Ytni = MapThread[ PseudoLinearMap[#1, {t,#2}, type]&, {Yt,ni} ];
	rules = Solve[eqs, Ytni];
	rules[[1]] /; rules=!={} && Length@rules[[1]]==Length@Yt
	(*Veat\[ODoubleDot]\[ODoubleDot]tlus juhuks, kui see kukub l\[ADoubleDot]bi!!*)
]

NLCModelToRules[ StateSpace[f_List, Xt_,Ut_,t_, h_, Yt_, type_, opts___Rule]] := Module[
	{rules, defs},
	rules = Join[ Thread[ PseudoLinearMap[Xt, t, type] -> f], Thread[Yt -> h]];
	defs = StateDefinitions /. {opts} /. Options[ StateSpace ];
	Join[rules, defs]
]


NLCModelToEquations[steq_StateSpace] := Equal @@@ NLCModelToRules[steq]
NLCModelToEquations[IO[phi_,rest__]] := Equal @@@ phi


(* ::Subsubsection:: *)
(*System properties (ClassicStateQ, IdentityShiftQ, ZeroDerivativeQ, PopovFormQ)*)


ClassicStateQ[_IO] := False
ClassicStateQ[ StateSpace[f_, Xt_, Ut_, t_, _, _, TimeDerivative,___]] := FreeQ[f, Derivative[_][_][t]]
ClassicStateQ[ StateSpace[f_, Xt_, Ut_, t_, _, _, Shift,___]] := FreeQ[f, t+_]

IdentityShiftQ[ (StateSpace|IO)[__, TimeDerivative,___]] := True
IdentityShiftQ[ (StateSpace|IO)[__, Shift,___]] := False

ZeroDerivativeQ[ (StateSpace|IO)[__, TimeDerivative,___]] := False
ZeroDerivativeQ[ (StateSpace|IO)[__, Shift,___]] := True



PopovFormQ[ioeq:IO[eqs:{__Rule},Ut_,Yt_, t_, op_], opts___] := 
Module[{prn, YtAtni, phi, ni, nij, sik, rows, cols, cond1, cond2, cond3, cond45, res, niSymbols},
	prn = PrintInfo /. {opts} /. Options[ PopovFormQ ];
	YtAtni = First/@eqs;
	phi=Last/@eqs;
	ni = MapThread[ MaxPLMOrder[ #1, #2, op]&, {YtAtni,Yt} ];
	nij = Outer[ MaxPLMOrder[#1, #2, op]&, phi, Yt];
	sik = Outer[ MaxPLMOrder[#1, #2, op]&, phi, Ut];
	cond1 = LessEqual@@ni;
	cond2 = MapThread[ Less[ Max[#1], #2]&, {Transpose@nij, ni}]; (*cond on cols*)
	cond3 = MapThread[ Less[ Max[#1], #2]&, {sik,ni}]; (*cond on rows*)
	{rows, cols} = Dimensions[nij];
	cond45 = Table[
		If[j<=i, nij[[i,j]]<ni[[i]], nij[[i,j]]<=ni[[i]] ],
		{i,rows}, {j,cols}
	];
	res = And@@Flatten[{cond1,cond2,cond3,cond45}];
	If[prn, 
		Print["ni = ",MatrixForm[Transpose[{ni}]],";   nij = ",MatrixForm[nij],";  si\[Kappa] =  ", MatrixForm[sik]];
		niSymbols = Array[Subscript["n", #]&, Length@ni];
		Print["Checking if ", LessEqual@@niSymbols, ":"];
		Print[cond1];
		Print["Checking if \!\(\*SubscriptBox[\(n\), \(ij\)]\) < \!\(\*SubscriptBox[\(n\), \(j\)]\) (conditions on columns):"];
		Print[cond2];
		Print["Checking if \!\(\*SubscriptBox[\(s\), \(i\[Kappa]\)]\) < \!\(\*SubscriptBox[\(n\), \(i\)]\) (conditions on rows):"];
		Print[cond3];
		Print["Checking if \!\(\*SubscriptBox[\(n\), \(ij\)]\) < \!\(\*SubscriptBox[\(n\), \(i\)]\) (j \[LessEqual] i) (conditions on rows, lower left triangle + diagonal of nij) and"];
		Print["Checking if \!\(\*SubscriptBox[\(n\), \(ij\)]\) \[LessEqual] \!\(\*SubscriptBox[\(n\), \(i\)]\) (j > i) (conditions on rows, upper right triangle of nij):"];
		Print[MatrixForm[cond45]]
	];
	res
]

Options[PopovFormQ] = {PrintInfo -> False}


(* ::Subsubsection:: *)
(*State space: Change of variables*)


Clear[NewVariables]
NewVariables::badnewvar = "The argument should be a list of new variables with the length 
`1` or a pure function fun[#][t]& .";

NewVariables[ stvar_, n_Integer, start_Integer, t_ ] := Switch[ stvar, 
	_List?(Length[#] === n &), stvar,
	_Symbol, Array[Subscript[stvar, #][t]&, n, start],
	_[t]&, Array[ stvar, n, start ],
	_[t] /; n == 1, { stvar },
	_, Message[ NewVariables::badnewvar, n ]; {} 
]

(*If the 3rd argument is integer, it is interpretated as index start value.*)
NewVariables[ stvar_, n_Integer, start_Integer ] := Switch[ stvar, 
	_List?(Length[#] === n &), stvar,
	_Symbol, Array[Subscript[stvar, #]&, n, start],
	_String, Array[Subscript[stvar, #]&, n, start],
	_&, Array[ stvar, n ],
	_ /; n == 1, { stvar },
	_, Message[ NewVariables::badnewvar, n ]; {} 
]
(*If the 3rd argument is other than integer, it is interpretated as time variable and index start value is assumed to be 1.*)
NewVariables[ stvar_, n_Integer, t_ ] := NewVariables[stvar, n, 1, t] 
NewVariables[ stvar_, n_Integer] := NewVariables[stvar, n, 1]


ChangeOfVariables::notfullrank = "Not full rank transformation.";
ChangeOfVariables::recsolve = "Successfully using iRecursiveSolve";
ChangeOfVariables::time ="Time spent on a transformation exceeded `1` seconds, and the transformation 
was aborted. Increasing the value of TimeConstraint option may improve the result.";

iRecursiveSolve[Zt_ == fun_, Xt_List] := iRecursiveSolve[Thread[Zt==fun],Xt]
iRecursiveSolve[sys0:{Equal__}, Xt0_List] := 
Module[ {n, fun, xlist, applicable, pos, sys, Xt, sol, solnew},
	n = Length@Xt0;
	fun = Last/@sys0;
	xlist = Table[{Select[Xt0, !FreeQ[fun[[i]],#]&], i}, {i,n} ];
	xlist = Sort[xlist, (Length[#1[[1]]]<=Length[#2[[1]] ])&];
	pos = Last /@ xlist;
	xlist = Join[{{}},First/@xlist];
	applicable = And@@Table[ Length[Complement[xlist[[i+1]],xlist[[i]]]]===1, {i,n} ];
	sys = sys0[[pos]];
	Xt = Xt0[[pos]];
	sol = {};
	Do[ solnew = Simplify[Solve[sys[[i]], Xt[[i]]]];
		If[solnew === {}|| Head[solnew]===Solve,
			applicable = False; Break[], 
		AppendTo[sol, solnew[[1,1]]/.sol ]
		], 
	{i, n} ];
	sol/;applicable
]

ChangeOfVariables[ steq:StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_, opts1___], stvar_, transf_List, opts___Rule ] :=
Module[ {timelimit, Zt, sol, testsol, inversetransf, fnew, hnew },
	timelimit = TimeConstraint /. {opts} /. Options[ ChangeOfVariables ];
	Zt = NewVariables[ stvar, Length@ Xt, t ];
	sol = TimeConstrained[Solve[ Zt == transf, Xt ],timelimit];
	If[ sol === {}, Message[ ChangeOfVariables::notfullrank] ];
	If[ sol === $Aborted, Message[ ChangeOfVariables::time,timelimit] ];
	(*Attempt to apply recursive solve*)
	If[ Head[sol] === Solve || sol === $Aborted,
		testsol = iRecursiveSolve[Zt == transf, Xt];
		If[ MatchQ[testsol, {Rule__}] && Length@testsol == Length@Xt, 
			Message[ChangeOfVariables::recsolve]; sol = {testsol}
		]
	];
	( inversetransf = Simplify[ First[sol] ];
		fnew = PseudoLinearMap[transf, steq] /. inversetransf;
		fnew  = Simplify[ PowerExpand[ Chop[ Expand[ fnew ]]]];
		hnew = h /. inversetransf;
		StateSpace[ fnew, Zt, Ut, t, hnew, Yt, type, StateDefinitions -> Thread[ Zt -> transf]]
	) /; Zt =!= badinput && sol =!= {} && Head[sol]=!=Solve && sol=!=$Aborted
]
Options[ChangeOfVariables] = {TimeConstraint->3};


(* ::Subsection:: *)
(*Pseudo-linear Maps*)


(* ::Subsubsection:: *)
(*MaxPLMOrder, MinPLMOrder, DetectVariables*)


DetectVariables[expr_,t_,TimeDerivative] := DeleteCases[Union[ Cases[ {expr}, Derivative[_][_][t+i_.]|x_[t], -1] ],{_},1]
DetectVariables[expr_,t_,Shift] := DeleteCases[Union[ Cases[ {expr}, x_[t+i_.], -1] ],{_},1]


MaxPLMOrder[ expr_, vars_List, type_ ]:= Max[ MaxPLMOrder[ expr, #, type ]& /@ vars]

MaxPLMOrder[ expr_, x_[t_], TimeDerivative ] := 
	Max[ Cases[ {expr}, Derivative[_][x][t]|x[t], -1] /. {Derivative[p_][x][t] -> p, x[t] -> 0}]

MaxPLMOrder[ expr_, x_[t_], Shift ] := Max[ Cases[ {expr}, x[t+i_.]->i, -1]]


MinPLMOrder[ expr_, vars_List, type_ ]:= Min[ MinPLMOrder[ expr, #, type ]& /@ vars]

MinPLMOrder[ expr_, x_[t_], TimeDerivative ] := 
	Min[ Cases[ {expr}, Derivative[_][x][t+_.] | x[t], -1] /. {Derivative[p1_][x][t] -> p1, x[t] -> 0}]

MinPLMOrder[ expr_, x_[t_], Shift ] := Min[ Cases[ {expr}, _[x][t+i_.]|x[t+i_.]->i, -1]]


(* ::Subsubsection:: *)
(*ForwardShift*)


ForwardShift[ expr_, {steq_StateSpace, n_Integer} ] := Nest[ Simplify[ ForwardShift[#, steq]]&, expr, n]
ForwardShift[ expr_, {t_, k_}, TimeDerivative ] := expr
ForwardShift[ expr_, {t_, k_}, Shift ] := expr /. t -> t+k  (*If k is pattern then Nest doesn't work*)


ForwardShift[expr_, StateSpace[f_, Xt_, Ut_, t_, _,_, TimeDerivative, ___ ]] := expr
ForwardShift[expr_, StateSpace[f_, Xt_, Ut_, t_, _,_, Shift, ___ ]] := expr/. t->t+1 /. Thread[(Xt/.t->t+1) -> f]

ForwardShift[expr_, t_, Shift ] := expr/. t->t+1 (* DefineOreRing requires *)
ForwardShift[expr_, t_, TimeDerivative] := expr  (* DefineOreRing requires *)


(* ::Subsubsection:: *)
(*BackwardShift*)


BackwardShift[expr_, {steq_StateSpace, n_Integer}, bwSpec___ ] := Nest[ Simplify[ BackwardShift[#, steq, bwSpec]]&, expr, n]
BackwardShift[ expr_, {t_, k_}, Derivative] := expr
BackwardShift[ expr_, {t_, k_}, Shift] := expr /. t->t-k (* for an arbitrary k, used in DefineOreRing *)


BackwardShift[ expr_, StateSpace[args1__, TimeDerivative,___], ___ ] := expr
BackwardShift[ expr_, steq:StateSpace[f_, Xt_, _, t_, _, _, Shift, ___], bwspec___ ] := 
	expr/. t->t-1 /. BackwardShiftOperator[steq, bwspec]

BackwardShift[expr_, t_, Shift ] := expr/. t->t-1
BackwardShift[expr_, t_, TimeDerivative] := expr


(* ::Subsubsection:: *)
(*PseudoD*)


(*These are only defs actually required for PLM[Sp,...]*)
PseudoD[expr_, StateSpace[f_, Xt_, Ut_, t_, _,_, TimeDerivative, ___ ]] := D[ expr, t] /. Thread[ D[Xt, t] -> f]
PseudoD[expr_, StateSpace[f_, Xt_, Ut_, t_, _,_, Shift,___ ]] := 0


(* ::Subsubsection:: *)
(*PseudoLinearMap (1-to-n )*)


PseudoLinearMap[ f:(_List|_Equal|_Plus), args__] := PseudoLinearMap[#, args]& /@ f

PseudoLinearMap[expr_, {steq_StateSpace, n_Integer} ] := Nest[ Simplify[ PseudoLinearMap[#, steq]]&, expr, n]
PseudoLinearMap[ expr_, {t_Symbol, n_}, TimeDerivative ] := D[ expr, {t, n}] (*If n is pattern (as in DefineOreRing) then Nest doesn't work*)
PseudoLinearMap[ expr_, {t_Symbol, n_}, Shift ] := expr /. t->t+n (*Not going to work with De and n>1.*)

PseudoLinearMap[ expr_, t_, TimeDerivative] := D[expr,t]
PseudoLinearMap[ expr_, t_, Shift] := expr/.t->t+1


PseudoLinearMap[w_, StateSpace[f_, Xt_, Ut_,t_, h_, Yt_,type_, opts___] ]:=
Module[{w1, coords},
	w1 = PseudoLinearMap[w,t,type] /. Thread[ PseudoLinearMap[Xt, t, type] -> f];
(*	coords = DetectVariables[w1, t, type];*)
	ExpandDe[w1, t, type]
]/;!FreeQ[{w}, De]


PseudoLinearMap[De[coef_,dxi_List], t_Symbol, TimeDerivative] := Module[{wed},
	wed = Table[ If[i==j, PseudoLinearMap[dxi[[j]],t,TimeDerivative], dxi[[j]] ],
		{i,Length@dxi}, {j,Length@dxi} ];
	Plus @@ (De[coef,#]&/@wed) + De[PseudoLinearMap[coef,t,TimeDerivative],dxi]
]
PseudoLinearMap[ De[coef_, dxi_List], t_Symbol, Shift] := De[PseudoLinearMap[coef,t,Shift],PseudoLinearMap[dxi,t,Shift]]

PseudoLinearMap[ w:De[coef_, dxi_List], {t_Symbol,n_Integer}, TimeDerivative] := Nest[ Simplify[ PseudoLinearMap[#, t, TimeDerivative]]&, w, n]
(* Shift uses the general def in Core.m *)


PseudoLinearMap[expr_, steq:StateSpace[__, TimeDerivative, ___ ]] := PseudoD[expr, steq]
PseudoLinearMap[expr_, steq:StateSpace[__, Shift, ___ ]] := ForwardShift[expr, steq]


(* ::Subsection:: *)
(*Generalized & Extended state*)


(* Used to construct field of meromorphic functions in Ore *)
Clear[FromIOToGeneralizedState]
FromIOToGeneralizedState[IO[eqs:{__Rule}, Ut_, Yt_, t_, type_], stvar_]:=
Module[{ni, ytn, phi, Xt, Xtparts, f, rules, revRules, idx, Yti, h, badinput},
	{ytn, phi} = Transpose[List@@@eqs];
	ni = MaxPLMOrder[ytn,#,type]& /@ Yt;
	Xt = NewVariables[stvar, Plus@@ni, t];
	idx = FoldList[ Plus, 0, ni];
	idx = Transpose[{ Drop[idx+1, -1], Rest[idx] }];
	Xtparts = Take[Xt,#]& /@ idx;
	Yti = MapThread[ Table[PseudoLinearMap[#1,{t,i-1},type],{i,#2}]&,{Yt,ni}];
	rules = Thread[ Flatten[Yti]->Xt ];
	f = MapThread[ Rest@Join[#1,{#2}]&, {Xtparts,phi/.rules} ]//Flatten;
	h = First /@ Xtparts;
	revRules = Reverse/@rules;
	StateSpace[f, Xt, Ut, t, h, Yt, type, StateDefinitions->revRules] /; Xt=!=badinput
] (*End Module*)


ExtendedState[gen:StateSpace[f_,Xt_,Ut_,t_,h_,Yt_,type_, opts___],stvar_,invar_]:=
Module[{n,alpha,m,Zt,Vt,Utij,Ztdef,Vtdef,fe,he,rules, revRules},
	n = Length[Xt];
	alpha = MaxPLMOrder[f,Ut,type];
	m = Length@Ut;
	Zt = NewVariables[stvar,n+m (alpha+1),t];
	Vt = NewVariables[invar,m,t];
	(Utij = Table[ PseudoLinearMap[Ut, {t,i},type], {i,0,alpha}]//Transpose//Flatten;
		Ztdef = Join[Xt,Utij];
		Vtdef = PseudoLinearMap[Ut,{t,alpha+1},type];
		rules = Thread[Join[Ztdef,Vtdef]->Join[Zt,Vt]];
		fe = Table[ If[i==alpha+1, Vt[[j]], Zt[[ n+(j-1)(alpha+1)+i+1 ]] ],
			{j,m}, {i,alpha+1} ]//Flatten;
		fe = Join[f/.rules,fe];
		he = h/.rules;
		revRules = Reverse/@rules;
		StateSpace[fe, Zt, Vt, t, he, Yt, type, StateDefinitions-> revRules ]
	)/;(Zt=!=badinput && Vt=!=badinput)
] (*End Module*)


ExtendedState[ioEq_IO, stvar_, invar_]:=
Module[{genEq, extEq, extRules, genRules,z},
	genEq = FromIOToGeneralizedState[ioEq, z];
	extEq = ExtendedState[genEq, stvar, invar];
	genRules = StateDefinitions /. Cases[genEq, _Rule];
	extRules = StateDefinitions /. Cases[extEq, _Rule];
	extEq /.(StateDefinitions->{__}) -> (StateDefinitions->(extRules/.genRules))
]


(* ::Subsection:: *)
(*Inversive Closure*)


(* ::Subsubsection:: *)
(*Sumbersivity*)


Submersivity[ioeq_IO ] := Submersivity @ ExtendedState[ ioeq, z, v ]
Submersivity[geneq_StateSpace] := Submersivity @ ExtendedState[ geneq, z, v]/;!ClassicStateQ[geneq]

Submersivity[ StateSpace[f_, Xt_, Ut_, t_, __, TimeDerivative, ___] ] := True

Submersivity[ StateSpace[f_, Xt_, Ut_, t_, __, Shift, ___] ] :=
	MatrixRank[ Outer[ D, f, Join[Xt,Ut]] ] == Length@f /; FreeQ[f, t+_]



(* ::Subsubsection:: *)
(*Closure (variables which can have negative time shifts)*)


NegativeTimeShifts::nonsub = "The system is not submersive.";


NegativeTimeShifts[ steq:StateSpace[f_, Xt_, Ut_, t_, _, _, Shift, ___ ]] := 
Module[{coords, Xtplus, n, m, df, idx, solidx},
    coords = Join[Xt, Ut];
	n = Length@Xt;
	m = Length@Ut;
	df = Outer[ D, f, coords ];
	Which[
	MatrixRank[df] < n,
		Message[ NegativeTimeShifts::nonsub ];
		{},
	1 === 1,(* else *)
		idx = Subsets[ Range[m+n], {n}];
		solidx = Map[ If[ Det[ df[[All, #]]]=!=0, #]&, idx];
		solidx = DeleteCases[ solidx, Null];
		coords[[ Complement[ Range[m+n], #] ]]& /@ solidx
	] (* End Which *)
] /; FreeQ[f, t+i_]


(* IO equations and Generealzed state equations *)
NegativeTimeShifts[ sys:(StateSpace|IO)[___, Shift,___]] := 
Module[{ ext, rules, z, v},
	ext = ExtendedState[sys, z, v ];
	rules = StateDefinitions /. Cases[ext, _Rule];
	NegativeTimeShifts[ext] /. rules
] /; !ClassicStateQ[sys]


NegativeTimeShifts[ StateSpace[__, TimeDerivative,___] ] := {{}}
NegativeTimeShifts[ IO[__, TimeDerivative] ] := {{}} (*Excessive definition, Extended does the same*)


(* ::Subsubsection:: *)
(*Backward shift operator*)


BackwardShiftOperator::time ="Time spent on a transformation exceeded `n` seconds, and the transformation 
was aborted. Increasing the value of TimeConstraint option may improve the result of simplification.";

BackwardShiftOperator[ StateSpace[ __, TimeDerivative,___ ], negvars___List, opts___Rule ] := {}
BackwardShiftOperator[ IO[ __, TimeDerivative, ___ ], negvars___List, opts___Rule ] := {}

BackwardShiftOperator[syst_, {}, ___Rule] := {}
BackwardShiftOperator[syst_, bwsop:{__Rule}, ___Rule ] := bwsop
(*BackwardShiftOperator[syst_, bwsop_?MatrixQ, opts___Rule ] := BackwardShiftOperator[syst, #, opts]& /@ bwsop*)


(*Systems with distourbances*)
BackwardShiftOperator[ StateSpace[f_, Xt_, Ut:{_List, _List}, t_, h_, Yt_, Shift, opts___] ]:=
	BackwardShiftOperator[ StateSpace[f, Xt, Flatten[Ut], t, h, Yt, Shift, opts] ]


BackwardShiftOperator[syst_, opts___Rule] := Module[{timelimit, negvars, bwsop},
	timelimit = TimeConstraint /. {opts} /. Options[ BackwardShiftOperator ];
	negvars = NegativeTimeShifts[syst];
	bwsop = BackwardShiftOperator[syst, #, opts]& /@ negvars;
	bwsop = DeleteCases[bwsop, {}|$Aborted];
	If[bwsop === {},
		Message[BackwardShiftOperator::time, timelimit],
		First[ Sort[bwsop, ByteCount[#1]<ByteCount[#2]&]]
	] (* End If *)
]


BackwardShiftOperator[sys:(IO[eqs0_, Ut_, XYt_, t_, Shift]|StateSpace[f_, XYt_, Ut_, t_, _, _, Shift, ___]), 
negvars:{_[t_]..}, opts___Rule] :=
	Module[{timelimit, eqs, coords, inv},
	timelimit = TimeConstraint /. {opts} /. Options[ BackwardShiftOperator ];
	eqs = Take[NLCModelToEquations[sys],Length@XYt]; (*In case of StateSpace keeps only state equations*)
	coords = Join[XYt,Ut];
	TimeConstrained[ 
		inv = Solve[ eqs, Complement[ coords, negvars ] ];
		If[ inv === {}||Head[inv] === Solve,
			inv,
			Simplify[ First@inv ]/. t->t-1
		],
	timelimit ]
]


Options[BackwardShiftOperator] = {TimeConstraint->1}


(* ::Subsection:: *)
(*One-forms: SpanK, De*)


De[ 0, x:{___}] := 0
De[ a_, x:{__}] := De[ Signature[x] a, Sort[x]] /; !OrderedQ[x]
De[ a_, x:{__}] := 0 /; Signature[x] == 0
De /: De[a_, {x__}] + De[b_, {x__}] := De[a+b, {x}]
De /:c_*De[a_, {x__}] := De[a*c, {x}] /; FreeQ[{c}, De]
De[a_,{___, _?NumberQ, ___}] := 0


(* ::Subsection:: *)
(*BookForm*)


(* ::Subsubsection:: *)
(*BookForm: ReplaceTimeArgumentBoxes*)


Clear[ReplaceTimeArgumentBoxes];
ReplaceTimeArgumentBoxes[ boxes_, {}, form_, opts___Rule ] := boxes
ReplaceTimeArgumentBoxes[ boxes_, t_Symbol, form_, opts___Rule ] := 
Module[{const, ts, timearg, r0, r1minus, r1plus, r2minus, r2plus, r3minus, r3plus, rules, rules1, rules2},
	const = 3; (* y^+++ ja y^[4] *)
	timearg = TimeArgument /. {opts} /. Options[ BookForm ];
	ts = ToString[t];
	If[form  === TraditionalForm,
		r0      = RowBox[{x__, "(", ts, ")"}]; 
		r1minus = RowBox[{SubscriptBox[x_, n_], "(", RowBox[{ts, "-", k_/;ToExpression[k] <= const}], ")"}];
		r1plus  = RowBox[{SubscriptBox[x_, n_], "(", RowBox[{ts, "+", k_/;ToExpression[k] <= const}], ")"}];

		r2minus = RowBox[{x__, "(", RowBox[{ts, "-", k_/;ToExpression[k] <= const}], ")"}];
		r2plus  = RowBox[{x__, "(", RowBox[{ts, "+", k_/;ToExpression[k] <= const}], ")"}];

		r3minus = RowBox[{x__, "(", RowBox[{ts, "-", k_}], ")" }];
		r3plus  = RowBox[{x__, "(", RowBox[{ts, "+", k_}], ")"}],
	(*Else: form === StandardForm *)
		r0      = RowBox[{x__, "[", ts, "]"}]; 
		r1minus = RowBox[{SubscriptBox[x_, n_], "[", RowBox[{ RowBox[{"-", k_/;ToExpression[k]<=const}], "+", ts }], "]" }];
		r1plus  = RowBox[{SubscriptBox[x_, n_], "[", RowBox[{k_/;ToExpression[k] <= const, "+", ts }], "]"}];

		r2minus = RowBox[{x__, "[", RowBox[{ RowBox[{"-", k_/;ToExpression[k] <= const}], "+", ts }], "]" }];
		r2plus  = RowBox[{x__, "[", RowBox[{k_/;ToExpression[k] <= const, "+", ts }], "]"}];

		r3minus = RowBox[{x__, "[", RowBox[{ RowBox[{"-", k_}], "+", ts }], "]" }];
		r3plus  = RowBox[{x__, "[", RowBox[{k_, "+", ts }], "]"}]
	];
	rules = Switch[ timearg, 
		False, {
			r0      :> x, 
			r1minus :> SubsuperscriptBox[x, n, StringJoin[Array["-"&, ToExpression[k$] ]]],
			r1plus  :> SubsuperscriptBox[x, n, StringJoin[Array["+"&, ToExpression[k$] ]]],	
			r2minus :> SuperscriptBox[x, StringJoin[Array["-"&, ToExpression[k$] ]]],	
			r2plus  :> SuperscriptBox[x, StringJoin[Array["+"&, ToExpression[k$] ]]],
			r3minus -> SuperscriptBox[x, RowBox[{"[-", k, "]"}]],
			r3plus  -> SuperscriptBox[x, RowBox[{"[", k,"]"}]]
			},
		Subscripted, { 
			r0      -> SubscriptBox[x, ts],
			r3minus -> SubscriptBox[x, RowBox[{ts, "-", k}]],
			r3plus  -> SubscriptBox[x, RowBox[{ts, "+", k}]]
			},
		Superscripted, {
			r0      -> x,
			r3minus -> SuperscriptBox[x, RowBox[{"[-", k, "]"}]],
			r3plus  -> SuperscriptBox[x, RowBox[{"[", k,"]"}]]
		},
		_, {
			RowBox[{ RowBox[{"-", k_}], "+", ts }] -> RowBox[{ts, "-", k }],
			RowBox[{k_, "+", ts }] -> RowBox[{ts, "+", k }] }
	];
	rules1 = {
		SuperscriptBox[SubscriptBox[x_,idx_],"\[Prime]\[Prime]\[Prime]",MultilineFunction->None]->SubscriptBox[OverscriptBox[x,"\[TripleDot]"],idx],
		SuperscriptBox[SubscriptBox[x_,idx_],"\[Prime]\[Prime]",MultilineFunction->None]->SubscriptBox[OverscriptBox[x,"\[DoubleDot]"],idx],
		SuperscriptBox[SubscriptBox[x_,idx_],"\[Prime]",MultilineFunction->None]->SubscriptBox[OverscriptBox[x,"."],idx]};
	rules2 = {
		SuperscriptBox[y_,"\[Prime]\[Prime]\[Prime]",MultilineFunction->None]->OverscriptBox[y,"\[TripleDot]"],
		SuperscriptBox[y_,"\[Prime]\[Prime]",MultilineFunction->None]->OverscriptBox[y,"\[DoubleDot]"],
		SuperscriptBox[y_,"\[Prime]",MultilineFunction->None]->OverscriptBox[y,"."]};
	boxes /. rules /. rules1 /. rules2
	]


(* ::Subsubsection:: *)
(*BookForm: Polynomial expressions*)


(* ---------------------- Formatting coefficients for polynomial  ---------------------------------------- *)

(* lets have a polynomial a1 x1 + ... + an xn *)
(* this generates list of strings {"a1", ... , " + an"},*)

CoefficientBoxes[ vec_?VectorQ, form_ ] := Module[{ coefbox },
	coefbox = Switch[#,
		1, {"+"},
		-1, {"-"},
		_Plus, {"+", RowBox[{"(", MakeBoxes[#1, form], ")"}]},
		_?Negative*__ | _?Negative, {"-", MakeBoxes[#, form]&[-#1]},
		_, {"+", MakeBoxes[#1, form ]}
	]& /@ vec
] (* End Module *)


(* --------- Calls CoefficientBoxes and CoordinateBoxes ant puts the results together ---------*)

PolynomialBoxes[coefs_List, coords_List] := Module[{bxs},
	bxs = MapThread[ Join, {coefs, coords} ];
	bxs = DeleteCases[bxs, {"+","0", ___}]; (* Removes 0-coefficients *)
	If[bxs === {}, bxs= {{RowBox[{"0"}]}} ];
	bxs = Replace[ bxs, {a___, {"+"}, b___} -> {a, {"+","1"}, b }]; (* constant term 1 *)
	bxs = Replace[ bxs, {a___, {"-"}, b___} -> {a, {"-","1"}, b }]; (* constant term -1 *)
	bxs = Replace[ bxs, {{"+", a__}, b___} -> {{a}, b} ]; (* Removes first plus *)
	RowBox[ Flatten[ bxs ] ]
]



(* ::Subsubsection:: *)
(*BookForm: StateSpace & IO equations*)


AddSeparatorBoxes[bxs_, StateSpace[f_, _, _,t_,h_, ___]]:= Insert[bxs, { "","",""}, Length[f]+Length[h]+1] /; Length[bxs] > Length[f]+Length[h]
AddSeparatorBoxes[bxs_, _] := bxs

BookForm /: MakeBoxes[ BookForm[sys:( StateSpace[f_,_,_,t_,___]|IO[_,_,_,t_,_]), opts___ ], form_ ] := 
Module[{ bxs, realnum, eqs, lhs, rhs},
	eqs = NLCModelToEquations[sys];	
	realnum = AllowRealNumbers /. {opts} /. Options[ BookForm ];
	If[realnum, eqs = eqs /. Rational[x_,y_] /; Abs[x]>10||y>10 :> N[x/y] ];
	{lhs, rhs} = Transpose[ List@@@eqs ];
	bxs = MapThread[{ MakeBoxes[ #1, form], "=", MakeBoxes[#2, form] }& , {lhs, rhs}];
	bxs = AddSeparatorBoxes[bxs, sys];
	bxs = ReplaceTimeArgumentBoxes[ bxs, t, form, opts ];
	GridBox[ bxs, ColumnAlignments -> {Right, Center, Left} ]
]


(* ::Subsubsection:: *)
(*BookForm: SpanK*)


AddSpanKBox[bxs_, StandardForm] := RowBox[{ "SpanK", "[", RowBox[bxs], "]"} ]
AddSpanKBox[bxs_, TraditionalForm] := RowBox[{ SubscriptBox["span","\[ScriptCapitalK]"], "{", RowBox[bxs], "}"} ]

CoordinateBoxes[ SpanK[sp_, coords_], form_ ] := {RowBox[{"\[DifferentialD]", MakeBoxes[#, form]}]}& /@ coords
CoordinateBoxes[ SpanKVectorFields[sp_, coords_], form_ ] := {FractionBox["\[PartialD]", RowBox[{"\[PartialD]", MakeBoxes[#, form] }] ]}& /@ coords

(* --------------------------------------------------------------------------- *)

BookForm /: MakeBoxes[ BookForm[ (SpanK|SpanKVectorFields|ExactSpanK)[ {}, _], ___ ], form_ ] := 
	AddSpanKBox[{RowBox[{"0"}]}, form]

BookForm /: MakeBoxes[ BookForm[ sp:(SpanK|SpanKVectorFields)[ mat_?MatrixQ, coords_], opts___Rule ], form_ ] := 
Module[{ bxs, dxi},
	dxi = CoordinateBoxes[sp, form];
	bxs = PolynomialBoxes[ CoefficientBoxes[#1, form], dxi]& /@ mat;
	bxs = Rest @ Flatten[ {",", #}& /@ bxs ]; 
	AddSpanKBox[bxs, form]
]

BookForm /: MakeBoxes[ BookForm[ sp:ExactSpanK[ fi_?VectorQ, coords_], opts___Rule ], form_ ] := 
Module[{ bxs },
	bxs = If[ MemberQ[coords, #],
		RowBox[{"d", MakeBoxes[#,form] }],
		RowBox[{"d", "(", MakeBoxes[#,form], ")"}]
	]& /@ fi;
	bxs = Rest @ Flatten[{",", #}& /@ bxs];
	AddSpanKBox[bxs, form]
]

BookForm /: MakeBoxes[ BookForm[ sp_SeparatedSpanK, opts___Rule ], form_ ] := 
Module[{bxs1, bxs2, bxs},
	{bxs1, bxs2} = List @@ (MakeBoxes[ BookForm[#, opts ], form]& /@ sp);
	RowBox[{bxs1, RowBox[{"+"}], bxs2}]
]


(* ::Subsubsection:: *)
(*BookForm: De*)


BookForm[w_De, opts___] := BookForm[ DeList[w], opts]
BookForm[w:HoldPattern[ Plus[__De]], opts___] := BookForm[ DeList @@ w, opts]

CoordinateBoxes[ De[_, coords_ ], form_ ] := { RowBox[ Rest[ 
	Flatten[{"\[Wedge]","\[DifferentialD]", MakeBoxes[#, form] }& /@ coords] 
]] }
CoordinateBoxes[ w_DeList, form_ ] := CoordinateBoxes[#, form ]& /@ (List@@w)


BookForm /: MakeBoxes[ BookForm[ w_DeList, opts___Rule], form_ ] := 
Module[{coefs, coords},
	coefs = CoefficientBoxes[First /@ (List@@w), form];
	coords = CoordinateBoxes[w, form];
	PolynomialBoxes[ coefs, coords]
]

BookForm /: MakeBoxes[ BookForm[wi_, t_Symbol, op_, opts___Rule], form_]:= Module[{bxs},
	bxs = MakeBoxes[ BookForm[wi, opts], form];
	ReplaceTimeArgumentBoxes[ bxs, {t, op}, form, opts]
]/;!FreeQ[{wi}, De]


(* ::Subsubsection:: *)
(*BookForm over lists*)


AddRowLabelBoxes[gb:GridBox[bxMat_,gbOpts___], form_, opts___]:= Module[{labels, labelNames, startIdx, labelBxs, finBxs, oldColAlign},
	labels = RowLabels /. {opts} /. Options[ BookForm];
	If[labels === None,
		gb, (*Nothing is changed*)
	(*Else*)
		startIdx = RowLabelsStartIndex /. {opts} /. Options[ BookForm];
		labelNames = NewVariables[labels, Length@bxMat, startIdx];
		labelBxs = MakeBoxes[ #, form]& /@ labelNames;
		oldColAlign = ColumnAlignments /. {gbOpts} /. Options[ BookForm]; (*Assume ColumnAlginements is determined by opts*)
		finBxs = MapThread[ Join[{#1, "="}, #2]&, {labelBxs, bxMat}];
		GridBox[finBxs, ColumnAlignments -> Join[{Right,Center}, oldColAlign] ]
	]
]


BookForm[{},___]:={}


(*BookForm over lists*)
BookForm /: MakeBoxes[ BookForm[ sps:{__SeparatedSpanK}, t___Symbol, opts___Rule ], form_ ] := Module[{bxs, gbx},
	bxs = MakeBoxes[ BookForm[#, t, opts], form]& /@ sps;
	bxs = First /@ bxs; (*Drop outer RowBox of each row to get 3-element list (matrix row)*)
	gbx = GridBox[bxs, ColumnAlignments->{Left, Center, Left}];
	AddRowLabelBoxes[gbx, form, opts]
]
(*Applied to Span-s and De-s, but not to OreP/OreR/OreRing *)
BookForm /: MakeBoxes[ BookForm[ expr_List, t___Symbol, opts___Rule ], form_ ] := Module[{bxs, gbx, bf},
	bf = BookForm[#, t, opts]& /@ expr;
	bxs = MakeBoxes[#, form]& /@ bf;
	bxs = List/@ bxs; (*From vector to 1-column Matrix*)
	gbx = GridBox[bxs, ColumnAlignments->{Left}];
	AddRowLabelBoxes[gbx, form, opts]
]



(* ::Subsubsection:: *)
(*BookForm : TraditionalForm,  2 argument-syntax*)


(*Neccessary for BookForm\[Rule]LaTeX*)
BookForm /: TraditionalForm[ BookForm[args___]] := DisplayForm[ MakeBoxes[ BookForm[args], TraditionalForm]]

BookForm[str_String, ___] := str
BookForm[ BookFormHolder[expr_, t_Symbol], opts___] := BookForm[expr, t, opts] 
(*Holds expr and t (or R) together as 1 argument, for GUI*)


BookForm /: MakeBoxes[ BookForm[expr_, t_Symbol, opts___Rule ], form_ ] := 
Module[{bxs},
	bxs = MakeBoxes[ BookForm[expr], form ];
	ReplaceTimeArgumentBoxes[ bxs, t, form, opts ]
]

BookForm /: MakeBoxes[ BookForm[expr_, opts___Rule ], form_ ] := MakeBoxes[expr, form]


(* ::Subsubsection:: *)
(*BookForm : Options*)


Options[ BookForm ] = { 
	TimeArgument -> Superscripted, (* Possible values: True, False, Subscripted, Superscripted *)
	ColumnWidths -> Automatic,
	RowSpacings -> 1.0,
	AllowScriptLevelChange -> False,
	AllowRealNumbers ->  True,
	RowLabels -> None,
	RowLabelsStartIndex -> 1,
	ColumnAlignments -> Automatic
};


(* ::Subsection:: *)
(*T EX*)


Clear[NLCToTeX, TeXToNLC]

NLCToTeX[str_String]:=str

NLCToTeX[obj_] := Module[ {texStr, obj1},
	obj1 = obj /. Derivative[n_?(#>2&)][y_][t_] :> Derivative[n][y[t]];
	texStr = ExportString[ TraditionalForm[obj1], "TeXFragment", 
		"BoxRules"->{
			SuperscriptBox[ SubscriptBox[x_,n_], "\[Prime]", _] :> "\\dot "<> x <> "_" <> n,
			SuperscriptBox[ SubscriptBox[x_,n_], "\[Prime]\[Prime]", _] :> "\\ddot "<> x <> "_" <> n,
			SuperscriptBox[ x_, "\[Prime]", _] :> "\\dot "<> x,
			SuperscriptBox[ x_, "\[Prime]\[Prime]", _] :> "\\ddot "<> x
		}
	];
	texStr = StringReplace[ToString[texStr], {"ccc"->"rcl", (*"\\text"\[Rule]"", *)"\r"->""}];
	StringDrop[texStr, -1] (*removes extra "\n" from the end*)
 ]
 
 TeXToNLC[tex_] := ToExpression[tex,TeXForm] /. {
	SphericalBesselY[n_Integer, t_] -> Subscript[ToExpression["y"],n][t],
	Subscript[OverDot[x_,n_],m_][t_] :> Derivative[n][Subscript[x,m]][t],
	OverDot[x_,n_][t_] :> Derivative[n][x][t]
}


(* ::Subsection:: *)
(*GUI*)


ConvertToSubscripted[name_Symbol] := Module[{str, newName, idx},
	str = ToString[name];
	newName = ToExpression[ StringRiffle[StringSplit[str, Except[LetterCharacter]],""]];
	idx = ToExpression[ StringRiffle[StringSplit[str, LetterCharacter],""]];
	If[newName===Null || idx=== Null, 
		name, 
		Subscript[newName, idx]
	]
]
ConvertToSubscripted[ioeq:IO[eqs_, Ut_, Yt_, ___]] := ConvertToSubscripted[ioeq, Join[Ut, Yt]]
ConvertToSubscripted[sseq:StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, ___]] := ConvertToSubscripted[sseq, Join[Xt, Ut, Yt]]

ConvertToSubscripted[obj_, vars_] := Module[{varHeads, newVarHeads},
	varHeads = Head /@ vars;
	newVarHeads = ConvertToSubscripted /@ varHeads;
	obj /. Thread[varHeads -> newVarHeads]
]

ConvertToSubscripted[name_]:=name


GUIFormat[data_List, opts___] := { data,
    StringRiffle[ NLCToTeX[ BookForm[#, TimeArgument -> Superscripted, opts]]& /@ data],
	StringRiffle[ NLCToTeX[ BookForm[#, TimeArgument -> True, opts]]& /@ data]
}


SubmersivityGUI[sys:(_IO|_StateSpace)] := Module[{comment},
	comment = If[Submersivity[sys], "System is submersive.", "System is not submersive."];
	GUIFormat[ {"State space equations:", sys, comment} ]
]


(* ::Subsection:: *)
(*End of package*)


On[General::spell1]
On[General::spell]

End[]

Protect[{}]

EndPackage[]
