(* ::Package:: *)

(* : Title : NLControl`Observability` *)
(* : Context : NLControl`Observability` *)
(* : Author : Vadim Kaparin *)
(* : Summary : *)
(* : Package Version : 2013, 2021 *)
(* : Mathematica Version : 6.0, 12 *)


(* ::Section:: *)
(*Preamble*)


BeginPackage["NLControl`Observability`",
  {"NLControl`Core`", "NLControl`OneForms`", "NLControl`Modelling`"}]


(* ::Subsection:: *)
(*Usage Messages*)


Observability::usage = "uses the observability rank condition to check
whether the state equations are observable or not."
ObservabilityFiltration::usage = "computes the observability filtration of
the system."
ObservableSpace::usage = "computes the observable space of the system."
UnObservableSpace::usage = "computes the unobservable space of the system."
ObservabilityIndices::usage = "computes the observability indices of the 
system."
ObservabilityDecomposition::usage = "decomposes the system into the
observable and unobservable subsystems."
Result::usage = "an option of the function ObservabilityDecomposition."


iOrderAndIOFunction::usage = "an auxiliary function. Finds the order and the 
input-output function of the system."
iFindOmegas::usage = "an auxiliary function. Finds the one-forms \[Omega] in terms
of which several conditions are formulated."


ObserverFormTransformability::usage = "checks whether the system is
transformable into the observer form or not.
Options for continuous-time system:
- ChangeOfOutput -> True - use output transformation;
- ChangeOfOutput -> False - do not use output transformation.
Options for discrete-time system:
- Method -> PartialDerivatives - checks the conditions in terms of 
Partial Derivatives.
- Method -> OneForms - checks the conditions in terms of one-forms"
ChangeOfOutput::usage = "an option of the functions ObserverFormTransformability
and ObserverForm."
PartialDerivatives::usage = "an option of the function
ObserverFormTransformability."
OneForms::usage = "an option of the function ObserverFormTransformability."
iTransfToOFnoOT::usage = "an auxiliary function of ObserverFormTransformability.
Checks whether the continuous-time system is transformable into the observer
form using state transformation only."
iTransfToOFwithOT::usage = "an auxiliary function of ObserverFormTransformability.
Checks whether the continuous-time system is transformable into the observer form
using the state and the output transformations."
iTransfToEOFOneForms::usage = " an auxiliary function of ObserverFormTransformability.
Uses the conditions in terms of one-forms to check whether the discrete-time system
is transformable into the extended observer form."
iTransfToEOFPartialD::usage = "an auxiliary function of ObserverFormTransformability.
Uses the conditions in terms of partial derivatives to check whether the discrete-time
system is transformable into the extended observer form."
MinBuffer::usage = "finds the minimal buffer which allows to transform the system into
the extended observer form."
iTransfToAOF::usage = "an auxiliary function of ObserverFormTransformability.
Checks whether the discrete-time system is transformable into the associative
observer form."


ObserverForm::usage = "transforms the system into the observer form if possible.
Options for continuous-time system:
- ChangeOfOutput -> True - use output transformation;
- ChangeOfOutput -> False - do not use output transformation.
In the discrete-time case, if the buffer is entered, transforms the system into
the extended observer form. If the buffer is not entered, the function transforms
the system into the associative observer form."
iObserverFormNoOT::usage = "an auxiliary function of ObserverForm. Transforms the
continuous-time system into the observer form using the state transformation only."
iObserverFormWithOT::usage = "an auxiliary function of ObserverForm. Transforms the
continuous-time system into the observer form using the state and output transformations."
iExtendedObserverForm::usage = "an auxiliary function of ObserverForm. Transforms the
discrete-time system into the extended observer form."
iOutputTransformation::usage = "an auxiliary function of iExtendedObserverForm. Finds
the output transformation allowing to transform the system into the extended observer
form."
iVarphiFunctions::usage = "an auxiliary function of iExtendedObserverForm. Finds the
input-output injections \[CurlyPhi], necessary for transformation into the extended observer form."
iAssociativeObserverForm::usage = "an auxiliary function of iExtendedObserverForm.
Transforms the discrete-time system into the associative observer form."


iVariableWithHat::usage = "an auxiliary function of Observer and AssociativeObserver.
Sets a hat on a variable."
Observer::usage = "constructs the observer for the continuous-time system using the
observer form obtained by means of state transformation."
AssociativeObserver::usage = "constructs the associative observer for the
discrete-time system."


ObservabilityGUI::usage = "Performs several Observability-related functions, necessary for graphic user interface."


ObserverFormGUI::usage = "Finds Observer form and format output for graphic user interface."


(* ::Subsection::Closed:: *)
(*Begin Functions*)


Begin["`Private`"]

Off[General::spell1]
Off[General::spell]
epsilon = 10.^-15;


(* ::Section:: *)
(*Observability*)


(* ::Subsection::Closed:: *)
(*Observability Condition*)


Observability[sys: StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_]] :=
  Module[{n, Hi, matrix},
    
    n = Length[Xt]; (* dimension of the system *)
 
    (* generates funktions h(x(k, x0, uk-1)) *)
    Hi = Flatten[PseudoLinearMap[h, {sys, #}]& /@(Range[n] - 1)];

    (* computes the matrix of partial derivatives *)
    matrix = Outer[D, Hi, Xt];
    
    (* checs observability rank condition *)
    MatrixRank[matrix] == n
    ]


Observability::cond = "The system is not observable";


(* ::Subsection:: *)
(*Observability Filtrations*)


ObservabilityFiltration[sys: StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_], opts___] := 
  Module[{n, m, p, coordinates, dy, Hi, Ok, l, test1, info, timearg},
    
    info = PrintInfo /. {opts} /. Options[ObservabilityFiltration];
    timearg = TimeArgument /. {opts} /. Options[ObservabilityFiltration];
    
    n = Length[Xt];
    m = Length[Ut];
    p = Length[Yt];
    coordinates = Join[Xt, Ut];
    
    (* initial values of the variables of the "While" cycle *)
    l = 2;
    test1 = False;
    
    While[test1 != True,
      
      (* generates funktions of outputs and its derivatives (shifts) *)
      Hi = Flatten[PseudoLinearMap[h, {sys, #}]& /@(Range[l] - 1)];
      
      (* the matrix of the observability filtration *)
      dy = Join[Outer[D, Hi, Xt], Table[Table[0, {i, m}], {j, p*l}], 2];
      
      (* generates observability filtration *)
      Ok = Table[SpanK[SimplifyBasis[Take[dy, p*i]], coordinates], {i, l}];
      
      l = l + 1;
      
      (* tests whether the last two elements of filtration are equal or not *)
      test1 = SameSubspace[Ok[[-1, 1]], Ok[[-2, 1]]]
      
      ];(* end of While *)
    
    If[info == True,
      Table[Print[Subscript[O, i - 1], " = ", BookForm[Ok[[i]], TimeArgument -> timearg]], {i, Length[Ok] - 1}];
      Print["\[VerticalEllipsis]"];
      Print[Subscript[O, Infinity], " = ", BookForm[Ok[[-1]], TimeArgument -> timearg]]
      (* tests whether system is observable or not using theorem: if O[\[Infinity]] = X the system is observable *)
      (* If[SameSubspace[Take[IdentityMatrix[n + m], n], Ok[[-1, 1]]], Print["Info: The system is observable."],
        Print["Info: The system is not observable."]]; *)
      ];(* end of IF *)
    
    Ok
    ]

Options[ObservabilityFiltration] = {PrintInfo -> False, TimeArgument -> True};


(* ::Subsection::Closed:: *)
(*Observable Space*)


ObservableSpace[sys: _StateSpace] := ObservabilityFiltration[sys][[-1]]


(* ::Subsection:: *)
(*Unobservable Space*)


UnObservableSpace[sys: StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_]] := 
  Module[{n, m, X, ObsSpace, UnObsSpace},
    
    n = Length[Xt];
    m = Length[Ut];
    
    X = SpanK[Take[IdentityMatrix[n + m], n], Join[Xt, Ut]];
    ObsSpace = ObservabilityFiltration[sys][[-1]];
    
    If[ObsSpace[[1]] == {}, UnObsSpace = X,
      UnObsSpace = ComplementSpace[X, ObsSpace]]
    ]


(* ::Subsection::Closed:: *)
(*Observability Indices*)


ObservabilityIndices[sys:StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_], opts___]:=
 Module[{info, OF, sigma, s},

  info = PrintInfo /. {opts} /. Options[ObservabilityIndices];

  OF = ObservabilityFiltration[sys];
  sigma = Join[{MatrixRank[OF[[1,1]]]}, Table[MatrixRank[First@ComplementSpace[OF[[i]], OF[[i-1]]]], {i, 2, Length[OF] -1 }]];
  s = Table[Length@Select[sigma, # >= i&], {i, 1, Max[sigma]}];

  If[info == True,
      Table[Print[Subscript["\[Sigma]", i], " = ", sigma[[i]]], {i, Length[sigma]}];
      Table[Print[Subscript["s", i], " = ", s[[i]]], {i, Length[s]}]
      ];
  {sigma, s}
]

Options[ObservabilityIndices] = {PrintInfo -> False};


(* ::Subsection:: *)
(*Observabilty Decomposition*)


(*
ObservabilityDecomposition[sys: StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_], nvar_, opts___Rule]:=
   Module[{n, m, d, z, X, integr, info, res, OS, IntObsS, PotentsCoords, NewCoords, newvar, index,
Eq},
    
    info = PrintInfo /. {opts} /. Options[ObservabilityDecomposition];
    res = Result /. {opts} /. Options[ObservabilityDecomposition];
    
    OS = ObservableSpace[sys];
    
    n = Length[Xt];
    m = Length[Ut];
    d = Length[OS[[1]]];
    X = SpanK[Take[IdentityMatrix[n + m], n], Join[Xt, Ut]];
    integr = Switch[type,
      TimeDerivative,
        True,
      Shift,
        Integrability[OS]];
    
    If[integr =!= False,
      
      (* if observable space is integrable *)
      
      index = Subsets[Range[n], {n - d}];
      If[index =!= {{}}, index = Select[index, MatrixRank[Join[X[[1, #]], OS[[1]]]] > d &]];
      IntObsS = IntegrateOneForms[OS];
      PotentsCoords = Xt[[#]]& /@ index;
      NewCoords = Join[IntObsS, #]& /@ PotentsCoords;
      newvar = NewVariables[nvar, Length@Xt, t];
      Eq = ChangeOfVariables[sys, nvar, #]& /@ NewCoords;
      Eq = Sort[Eq, ByteCount[#1[[1]]] < ByteCount[#2[[1]]]&];
      
      (* printing of the Info *)
      If[info,
        z = Eq[[1, 1, 2]];
        
        Switch[d, (* number of observable variables *)
          0, Print["There are no observable variables."],
          1, Print[Take[z, d], " is observable variable."],
          _, Print[Take[z, d], " are observable variables."]];
        
        Switch[n - d, (* number of unobservable variables *)
          0, Print["There are no unobservable variables."],
          1, Print[Take[z, -(n - d)], " is unobservable variable."],
          _, Print[Take[z, -(n - d)], " are unobservable variables."]];
        ],
      
      (* if observable space is not integrable *)
      
      Eq = {{}};
      
      (* printing of the Info *)
      If[info, Print["The system cannot be decomposed into the observable and unobservable subsistems. The observable space ",
        Subscript[O, \[Infinity]], " is not completaly integrable."],
        Message[ObservabilityDecomposition::notint, Subscript[O, \[Infinity]]]
        ]
      
      ];(* end of main IF *)
    
    Switch[res,
      All, Eq, (* gives all posible variants of the system *)
      Automatic, Eq[[1]]] (* gives the compact variant of the system *)
    
    ]
Options[ObservabilityDecomposition] = {PrintInfo -> False, Result -> Automatic};
*)

ObservabilityDecomposition[steq:StateSpace[f_, Xt_, Ut_, ___], nvar_, opts___Rule] := 
	Module[{Oinf, zi, ziCompCoords},
	Oinf = ObservableSpace[steq];
	zi = IntegrateOneForms[Oinf];
	If[zi=!={},
		ziCompCoords = ComplementSpace[Xt, zi, Join[Xt,Ut]];
		ChangeOfVariables[steq, nvar, Join[zi,ziCompCoords] ],
	(*Else *)
		Message[ObservabilityDecomposition::notint, Subscript[O, \[Infinity]]];
		{}
	]
]


ObservabilityDecomposition::notint = "The system cannot be decomposed into the observable and unobservable subsystems. The observable space `1` is not integrable.";


(* ::Section:: *)
(*Observer Forms*)


(* ::Subsection:: *)
(*Auxiliary Functions*)


iOrderAndIOFunction[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_,type_]|IO[eq_, Ut_, Yt_, t_, type_])] :=
  Module[{n, ioeq,F},
   Switch[Head[sys],
          StateSpace,
            n = Length[Xt]; (* order of the system *)
            ioeq = PowerExpand[StateSpaceToIO[sys]]; (* i/o equation *)
            If[Observability[sys]===False, Message[Observability::cond]],
          IO,
            n = MaxPLMOrder[eq, Yt,type]; (* order of the system *)
            ioeq = sys  (* i/o equation *)
        ];
       F = Switch[type,
                Shift, (Yt[[1]] /. t -> t + n),
                TimeDerivative, D[Yt[[1]], {t, n}]
               ] /. ioeq[[1]]; (* i/o function*)
      {n, F}
   ]


iFindOmegas[n_Integer, F_, Ut_List, Yt_List, t_] :=
 FullSimplify[Table[De[D[F, (Yt[[1]] /. t -> t + i)], {Yt[[1]] /. t -> t + i}] +
                    De[D[F,(Ut[[1]] /. t -> t + i)], {Ut[[1]] /. t -> t + i}], {i, 0, n - 1}]]

iFindOmegas[sys: (StateSpace[_, _, Ut_, t_, _, Yt_, Shift]|IO[_, Ut_, Yt_, t_, Shift])]:= 
  Module[{n, F},
   {n, F} = iOrderAndIOFunction[sys];
   iFindOmegas[n, F, Ut, Yt, t]
]


(* ::Subsection:: *)
(*Transformability into Observer Form*)


(* ::Subsubsection::Closed:: *)
(*main function*)


Options[ObserverFormTransformability] = {ChangeOfOutput -> True, Method -> PartialDerivatives};


ObserverFormTransformability[sys: (StateSpace[_, _, _, _, _, _, TimeDerivative]|IO[_, _, _, _, TimeDerivative]), opts___Rule] :=
 Module[{option},
  option = ChangeOfOutput /. {opts} /. Options[ObserverFormTransformability];
  If[And[Head[sys] === StateSpace, Observability[sys] === False],
   Message[Observability::cond];
   False,
   Switch[option,
    False, iTransfToOFnoOT[sys],
    True, iTransfToOFwithOT[sys]
   ]
  ]
 ]


ObserverFormTransformability[sys: (StateSpace[_, _, _, _, _, _, Shift]|IO[_, _, _, _, Shift]), BN_, opts___Rule] :=
 Module[{method},
  method = Method /. {opts} /. Options[ObserverFormTransformability];
  If[And[Head[sys] === StateSpace, Observability[sys] === False],
   Message[Observability::cond];
   False,
    Switch[method,
     PartialDerivatives, iTransfToEOFPartialD[sys, BN],
     OneForms, iTransfToEOFOneForms[sys, BN]
    ]
   ]
  ]


ObserverFormTransformability[sys: (StateSpace[_, _, _, _, _, _, Shift]|IO[_, _, _, _, Shift])] := 
 If[And[Head[sys] === StateSpace, Observability[sys] === False],
     Message[Observability::cond];
     False,
     iTransfToAOF[sys]
  ]


ObserverFormTransformability::cond = "The system is not transformable into the observer form. The necessary and sufficient conditions are not satisfied.";
ObserverFormTransformability::extcond = "The system is not transformable into the extended observer form with buffer `1`. The necessary and sufficient conditions are not satisfied.";
ObserverFormTransformability::buffer = "The buffer N should be an integer satisfying 0 \[LessEqual] N \[GreaterEqual] `1`.";
ObserverFormTransformability::assoccond = "The system is not transformable into the associative observer form. The necessary and sufficient conditions are not satisfied.";


(* ::Subsubsection:: *)
(*continuous-time / no output transformation*)


iTransfToOFnoOT[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, TimeDerivative]|IO[eq_, Ut_, Yt_, t_, TimeDerivative])]:= 
  Module[{n, YU, F, fun, omega, difomega, phi, uv, yv, coords, derivatives, matrix, deomega, control, difomeganew},
    
    {n, F} = iOrderAndIOFunction[sys];
    YU = Join[Yt, Ut];
    coords = Flatten[Table[D[Join[Yt, Ut], {t, i}], {i, 0, n - 1}]];

    (* initial values of omega and phi *)
    difomega = {};
    omega = {};
    phi = {};
    F = {F};

    (* search of omega and phi *)
    Table[
      AppendTo[omega, D[F[[k]], D[Yt[[1]], {t, n - k}]] + Plus@@(D[F[[k]], D[#, {t, n - k}]]& /@Ut)];
      AppendTo[difomega, De[D[F[[k]], D[Yt[[1]], {t, n - k}]], Yt] + Plus@@(De[D[F[[k]], D[#, {t, n - k}]], {#}]& /@Ut)];
      AppendTo[phi, (fun@@YU /. DSolve[omega[[k]] == D[fun@@YU, Yt[[1]]] + Plus@@(D[fun@@YU, #]& /@Ut), fun@@YU, YU][[1]]) /. {C[1][_]:>0}];
      AppendTo[F, F[[k]] - D[phi[[k]], {t, n - k}]], {k, n}];

    difomeganew = Cases[FullSimplify[difomega], Except[0]];

    (* necessary and sufficient condition *)
    Table[0, {Length[difomeganew]}] === (ExteriorD[#, coords]& /@ difomeganew)
    ]


(* ::Subsubsection:: *)
(*continuous-time / with ouput transformation*)


iTransfToOFwithOT[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, TimeDerivative]|IO[eq_, Ut_, Yt_, t_, TimeDerivative])]:= 
  Module[{n, F, coords, omegasDY, omegasDU, lambda, dePhiDY, dePhiDU, Phi, fnew, Hi, eqs, coordinateTransform},
   
   {n, F} = iOrderAndIOFunction[sys];

   coords = Flatten[Table[D[Join[Yt,Ut], {t, i}], {i, 0, n-1}]];
   omegasDY = FullSimplify[Table[Sum[(-1)^j Binomial[n - i + j, j]D[D[F, D[Yt[[1]], {t, n - i + j}]], {t, j}], {j, 0, i - 1}], {i, 1, n}]];
   omegasDU = FullSimplify[Table[Sum[(-1)^j Binomial[n - i + j, j]D[D[F, D[Ut[[1]], {t, n - i + j}]], {t, j}], {j, 0, i - 1}], {i, 1, n}]];
   lambda = (Fun[Yt[[1]]]/.DSolve[ExteriorD[De[Binomial[n, 1]D[Fun[Yt[[1]]], t], {Yt[[1]]}] + De[Fun[Yt[[1]]] * omegasDY[[1]], {Yt[[1]]}] + De[Fun[Yt[[1]]] * omegasDU[[1]], {Ut[[1]]}], coords][[All, 1]] == 0, Fun[Yt[[1]]], Yt[[1]]]/. {C[_]:>1})[[1]];
   dePhiDY = FullSimplify[Table[(-1)^(i-1) Binomial[n, i]D[lambda, {t, i}] + Sum[(-1)^(i-j) Binomial[n - j, i - j]D[lambda, {t, i - j}]omegasDY[[j]], {j, 1, i}], {i, 1, n}]/.D[Yt[[1]], {t, n}] -> F];
   dePhiDU = FullSimplify[Table[Sum[(-1)^(i-j) Binomial[n - j, i - j]D[lambda, {t, i - j}]omegasDU[[j]], {j, 1, i}], {i, 1, n}]/.D[Yt[[1]], {t, n}] -> F];
   (D[#, First@Ut]&/@dePhiDY) === (D[#, First@Yt]&/@dePhiDU)
]


(* ::Subsubsection:: *)
(*discrete-time / extended form / conditions in terms of one-forms*)


iTransfToEOFOneForms[n_Integer, F_, Ut_List, Yt_List, t_, BN_] := 
 Module[{coords, omega, deomega, indices, bigomega},
  Which[BN === n - 1, True,
        And[0 <= BN < n - 1, Head[BN] === Integer],
         omega = iFindOmegas[n, F, Ut, Yt, t];
         coords = Flatten[Table[Join[Yt, Ut] /. t -> t + i, {i, 0, n - 1}]];
         bigomega = Table[Join[Flatten[Table[(Join[Yt, Ut] /. t -> t + k), {k, Max[i - BN, 0], i - 1}]], Flatten[Table[(Join[Yt, Ut] /. t -> t + k), {k, i + 1, Min[i + BN, n-1]}]]], {i, 0, n-1}];
         deomega = ExteriorD[#, coords]& /@omega;
         indices = Join[Array[{#, #}&, n], Subsets[Range[n], {2}]];
         Table[0, {Binomial[n, 2] + n}] === (FullSimplify[Wedge[Wedge[deomega[[#[[1]]]], omega[[#[[2]]]]] + Wedge[deomega[[#[[2]]]], omega[[#[[1]]]]], De[1, Complement[Union[bigomega[[#[[1]]]], bigomega[[#[[2]]]]], Yt/. (t -> t + #[[1]] - 1), Ut/. t -> (t + #[[2]] - 1)]]]]& /@ indices),
        Or[BN < 0, BN > n - 1, Head[BN] =!= Integer], Message[ObserverFormTransformability::buffer, n - 1]
   ]
  ]

iTransfToEOFOneForms[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, Shift]|IO[eq_, Ut_, Yt_, t_, Shift]), BN_] := 
 Module[{n, F},
  {n, F} = iOrderAndIOFunction[sys];
  iTransfToEOFOneForms[n, F, Ut, Yt, t, BN]
  ]


(* ::Subsubsection:: *)
(*discrete-time / extended form / conditions in term of partial derivatives*)


iTransfToEOFPartialD[n_Integer, F_, Ut_List, Yt_List, t_, BN_] :=
 Module[{Ayy, Ayu, Auu, Auy, A, test1, yind, uind, ind, optind, test2y, Byy, Byu, test2u, Buu, Buy},
  Which[BN === n - 1, True,
        And[ 0<= BN < n - 1, Head[BN] === Integer],
         (* main conditions *)
         Ayy = FullSimplify[Table[If[MemberQ[Range[i - BN, i + BN], j], 0, D[Log[D[F, (Yt[[1]] /. t -> t + i)]], (Yt[[1]] /. t -> t + j)]], {i, 0, n - 1}, {j, 0, n - 1}]];
         Ayu = FullSimplify[Table[If[MemberQ[Range[i - BN, i + BN], j], 0, D[Log[D[F, (Ut[[1]] /. t -> t + i)]], (Yt[[1]] /. t -> t + j)]], {i, 0, n - 1}, {j, 0, n - 1}]];
         Auu = FullSimplify[Table[If[MemberQ[Range[i - BN, i + BN], j], 0, D[Log[D[F, (Ut[[1]] /. t -> t + i)]], (Ut[[1]] /. t -> t + j)]], {i, 0, n - 1}, {j, 0, n - 1}]];
         Auy = FullSimplify[Table[If[MemberQ[Range[i - BN, i + BN], j], 0, D[Log[D[F, (Yt[[1]] /. t -> t + i)]], (Ut[[1]] /. t -> t + j)]], {i, 0, n - 1}, {j, 0, n - 1}]];
         A = Join[Transpose[Join[Ayy, Ayu]], Transpose[Join[Auu, Auy]]];
         test1 = And@@Table[SameQ@@Cases[A[[i]], Except[0]], {i, 1, 2n}];
         (* additional conditions *)
         If[test1 == True,
          yind = Select[Range[0, n - 1], MemberQ[F, (Yt[[1]] /. t -> t + #), Depth[F]]&];
          uind = Select[Range[0, n - 1], MemberQ[F, (Ut[[1]] /. t -> t + #), Depth[F]]&];
          If[Or[Length@yind == 0, Length@uind == 0, And[2BN < Max[yind] - Min[yind], 2BN < Max[uind] - Min[uind]]],
           True,
           ind = Intersection[yind, uind];
           If[Length@ind == 0, True,
            optind = If[Min[ind] <= n - 1 - Max[ind], Min[ind], Max[ind]];
            test2y = If[2BN < Max[yind] - Min[yind], True,
                    Byy = FullSimplify[Table[First@Cases[A[[optind + 1]], Except[0]] D[F, (Yt[[1]] /. t -> t + r)](D[F, (Yt[[1]] /. t -> t + optind)])^-1, {r, Max[yind] - BN, Min[yind] + BN}]];
                    Byu = FullSimplify[Table[First@Cases[A[[n + optind + 1]], Except[0]] D[F, (Yt[[1]] /. t -> t + r)](D[F, (Ut[[1]] /. t -> t + optind)])^-1, {r, Max[yind] - BN, Min[yind] + BN}]];
                    Byy == Byu];
            test2u = If[2BN < Max[uind] - Min[uind], True,
                    Buu = FullSimplify[Table[First@Cases[A[[n + optind + 1]], Except[0]] D[F, (Ut[[1]] /. t -> t + r)](D[F, (Ut[[1]] /. t -> t + optind)])^-1, {r, Max[yind] - BN, Min[yind] + BN}]];
                    Buy = FullSimplify[Table[First@Cases[A[[optind + 1]], Except[0]] D[F, (Ut[[1]] /. t -> t + r)](D[F, (Yt[[1]] /. t -> t + optind)])^-1, {r, Max[yind] - BN, Min[yind] + BN}]];
                    Buu == Buy];
            And[test2y, test2u]]
           ]
          , test1],
        Or[BN < 0, BN > n-1, Head[BN] =!= Integer], Message[ObserverFormTransformability::buffer, n - 1]
       ]
 ]

iTransfToEOFPartialD[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, Shift]|IO[eq_, Ut_, Yt_, t_, Shift]), BN_] :=
 Module[{n, F},
  {n, F} = iOrderAndIOFunction[sys];
  iTransfToEOFPartialD[n, F, Ut, Yt, t, BN]
]


(* ::Subsubsection:: *)
(*discrete-time / extended form / minimal buffer*)


MinBuffer[sys: (StateSpace[_, _, _, _, _, _, Shift]|IO[_, _, _, _, Shift])] := Module[{BN},
 If[And[Head[sys] === StateSpace, Observability[sys] === False],
    Message[Observability::cond];
    {},
    BN=0;
    While[ObserverFormTransformability[sys, BN] === False, BN++];
    BN
   ]
 ]


(* ::Subsubsection:: *)
(*discrete-time / associative form*)


iTransfToAOF[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, Shift]|IO[eq_, Ut_, Yt_, t_, Shift])]:= 
  Module[{n, F, coords, omega, deomega, control},

    {n, F} = iOrderAndIOFunction[sys];
    coords = Flatten[Table[Join[Yt, Ut] /. t -> t + i, {i, 0, n - 1}]];
    (* search of omega *)
    omega = iFindOmegas[n, F, Ut, Yt, t];
    (* full differential of omega *)
    deomega = ExteriorD[#, coords]& /@omega;

    (* necessary and sufficient condition *)
    Table[Table[0, {n}], {n}] === Plus[Outer[Wedge, deomega, omega], Transpose[Outer[Wedge, deomega, omega]]]
  ]


(* ::Subsection:: *)
(*Observer Form*)


(* ::Subsubsection:: *)
(*main function*)


Options[ObserverForm] = {ChangeOfOutput -> True};


ObserverForm[sys: (StateSpace[_, _, _, _, _, _, TimeDerivative]|IO[_, _, _, _, TimeDerivative]), var_] :=
 If[And[Head[sys] === StateSpace, Observability[sys] === False],
     Message[Observability::cond];
     {},
     iObserverFormNoOT[sys, var]
 ]
ObserverForm[sys: (StateSpace[_, _, _, _, _, _, TimeDerivative]|IO[_, _, _, _, TimeDerivative]), var_, newYt_] :=
 If[And[Head[sys] === StateSpace, Observability[sys] === False],
     Message[Observability::cond];
     {},
     iObserverFormWithOT[sys, var, newYt]
 ]


ObserverForm[sys: (StateSpace[_, _, _, _, _, _, Shift]|IO[_, _, _, _, Shift]), BN_, var_, newYt_] :=
 If[And[Head[sys] === StateSpace, Observability[sys] === False],
     Message[Observability::cond];
     {},
     iExtendedObserverForm[sys, BN, var, newYt]
 ]


ObserverForm[sys: (StateSpace[_, _, _, _, _, _, Shift]|IO[_, _, _, _, Shift]), var_, Phi_] :=
 If[And[Head[sys] === StateSpace, Observability[sys] === False],
     Message[Observability::cond];
     {},
     iAssociativeObserverForm[sys, var, Phi]
 ]


(* ::Subsubsection::Closed:: *)
(*continuous-time / no output transformation*)


iObserverFormNoOT[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, TimeDerivative]|IO[eq_, Ut_, Yt_, t_, TimeDerivative]), var_]:= 
  Module[{n, F, fun, omega, YU, phi, newvar, Hi, eqs, coordinateTransform, fnew},

     If[ObserverFormTransformability[sys, ChangeOfOutput -> False],

       (* if the system is transformable into the observer form *)
       {n, F} = iOrderAndIOFunction[sys];
       YU = Join[Yt, Ut];

       (* initial values of omega and phi *)
       omega = {};
       phi = {};
       F = {F};

       (* new variables *)
       newvar = NewVariables[var, n, t];

       (* search of omega and phi *)
       Table[
         AppendTo[omega, D[F[[k]], D[Yt[[1]], {t, n - k}]] + Plus@@(D[F[[k]], D[#, {t, n - k}]]& /@Ut)];
         AppendTo[phi, (fun@@YU /. DSolve[omega[[k]] == D[fun@@YU, Yt[[1]]] + Plus@@(D[fun@@YU, #]& /@Ut), fun@@YU, YU][[1]]) /. {C[1][_]:>0}];
         AppendTo[F, F[[k]] - D[phi[[k]], {t, n - k}]], {k, n}];

       fnew = Simplify[PowerExpand[Chop[Expand[Join[Take[newvar, -(n - 1)], {0}] + (phi /. Yt[[1]] -> First@newvar)]]]];

       Switch[Head[sys],
         StateSpace,
           Hi = PseudoLinearMap[h[[1]], {sys, #}]& /@(Range[n] - 1);
           eqs = Thread[NestList[D[#, t]&, First@Yt, n - 1] -> Hi];
           coordinateTransform = Expand[FullSimplify[FoldList[D[#1, t] - #2&, Yt[[1]], Take[phi, n - 1]] /. eqs]];

           (* new system *)
           StateSpace[fnew, newvar, Ut, t, First@newvar, Yt, TimeDerivative, StateDefinitions -> Thread[newvar -> coordinateTransform]],
         IO,
           StateSpace[fnew, newvar, Ut, t, First@newvar, Yt, TimeDerivative]
       ],

       (* if the system is not transformable into the observer form *)
       Message[ObserverFormTransformability::cond];
       {}
     ] (* end of If *)
  ]


(* ::Subsubsection:: *)
(*continuous-time / with output transfarmation*)


iObserverFormWithOT[sys_, var_, newYt:Except[_List]] := iObserverFormWithOT[sys, var, {newYt}]

iObserverFormWithOT[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, TimeDerivative]|IO[eq_, Ut_, Yt_, t_, TimeDerivative]), var_, newYt_List]:= 
 Module[{n, ioeq, F, newvar, coords, omegasDY, omegasDU, lambda, bigF, dePhiDY, dePhiDU, Phi, fnew, Hi, eqs, coordinateTransform},
 
     {n, F} = iOrderAndIOFunction[sys];
     (* new variables *)
     newvar = NewVariables[var, n, t];

     coords = Flatten[Table[D[Join[Yt, Ut], {t, i}], {i, 0, n - 1}]];
     omegasDY = FullSimplify[Table[Sum[(-1)^j Binomial[n - i + j, j]D[D[F, D[Yt[[1]], {t, n - i + j}]], {t, j}], {j, 0, i - 1}], {i, 1, n}]];
     omegasDU = FullSimplify[Table[Sum[(-1)^j Binomial[n - i + j, j]D[D[F, D[Ut[[1]], {t, n - i + j}]], {t, j}], {j, 0, i - 1}], {i, 1, n}]];
     lambda = (Fun[Yt[[1]]]/.DSolve[ExteriorD[De[Binomial[n, 1]D[Fun[Yt[[1]]], t], {Yt[[1]]}] + De[Fun[Yt[[1]]] * omegasDY[[1]], {Yt[[1]]}] + De[Fun[Yt[[1]]] * omegasDU[[1]], {Ut[[1]]}], coords][[All,1]] == 0, Fun[Yt[[1]]], Yt[[1]]]/. {C[_]:>1})[[1]];
     dePhiDY = FullSimplify[Table[(-1)^(i-1) Binomial[n, i]D[lambda, {t, i}] + Sum[(-1)^(i-j) Binomial[n - j, i - j]D[lambda, {t, i - j}]omegasDY[[j]], {j, 1, i}], {i, 1, n}]/.D[Yt[[1]], {t, n}] -> F];
     dePhiDU = FullSimplify[Table[Sum[(-1)^(i-j) Binomial[n - j, i - j]D[lambda,{t, i - j}]omegasDU[[j]],{j, 1, i}], {i, 1, n}]/.D[Yt[[1]], {t, n}] -> F];
        If[(D[#, First@Ut]&/@dePhiDY) === (D[#, First@Yt]&/@dePhiDU),
          (* if the system is transformable into the observer form *)
          bigF = Integrate[lambda, Yt[[1]]];
          Phi = Table[Integrate[dePhiDY[[i]], Yt[[1]]] + Integrate[D[Fun[Ut[[1]]], Ut[[1]]]/.First@Solve[D[Integrate[dePhiDY[[i]], Yt[[1]]] + Fun[Ut[[1]]], Ut[[1]]] == dePhiDU[[i]], D[Fun[Ut[[1]]], Ut[[1]]]], Ut[[1]]], {i, 1, n}];
          fnew = Simplify[PowerExpand[Chop[Expand[Join[Take[newvar, -(n - 1)], {0}] + (Phi /. Solve[bigF == First@newvar, Yt][[1, 1]])]]]];

          Switch[Head[sys],
              StateSpace, 
               coordinateTransform = FullSimplify[FoldList[D[#1, t] - #2/.Join[{Yt[[1]] -> h[[1]]}, Thread[D[#, t]&/@Xt -> f]]&,(bigF/.Yt[[1]] -> h[[1]]), Take[Phi, n - 1]]];
               (* new system *)
               {StateSpace[fnew, newvar, Ut, t, First@newvar, newYt, TimeDerivative], Append[Thread[newvar -> coordinateTransform], newYt[[1]] -> bigF]},
              IO,
               {StateSpace[fnew, newvar, Ut, t, First@newvar, newYt, TimeDerivative], {newYt[[1]] -> bigF}}
              ],
       (* if the system is not transformable into the observer form *)
         Message[ObserverFormTransformability::cond];
         {}]

]


(* ::Subsubsection:: *)
(*discrete-time / extended form*)


iExtendedObserverForm[sys_, BN_, var_, newYt:Except[_List]] := iExtendedObserverForm[sys, BN, var, {newYt}]

iExtendedObserverForm[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, Shift]|IO[eq_, Ut_, Yt_, t_, Shift]), BN_, var_, newYt_List] :=
 Module[{n, F, Y, PsiF, varphi, newvar, repl, newvarphi, newf, coordinateTransform},

     {n, F} = iOrderAndIOFunction[sys];
     If[iTransfToEOFPartialD[n, F, Ut, Yt, t, BN],
        (* if conditions are satisfied *)
         newvar = NewVariables[var, n, t]; (* new variables *)
         Y = iOutputTransformation[n, F, Ut, Yt, t, BN];
         PsiF = FullSimplify[Y /. Yt[[1]] -> F];
         varphi = Table[(iVarphiFunctions[n, PsiF, Ut, Yt, t, BN][[-i]] /. t -> t - n + i), {i, 1, n - BN}];
         repl = Table[Solve[(First@newvar == Y /. t -> t - i), Yt[[1]] /. t -> t - i][[1, 1]],{i, 0, BN}];
         newvarphi = Expand[FullSimplify[PowerExpand[varphi /. repl]]];
         newf = Join[Take[newvar, -(n - 1)], {0}] + Join[newvarphi, Table[0, {i, n - BN + 1, n}]];
         Switch[Head[sys],
                StateSpace,
                 coordinateTransform = Expand[FullSimplify[PowerExpand[Table[(Y /. t -> t + i - 1) - Sum[varphi[[l]] /. t -> t + i - 1 - l, {l, 1, Min[i - 1, n - BN]}], {i, 1, n}] /. Join[Table[(Yt[[1]] /. t -> t + i) -> PseudoLinearMap[h[[1]], {sys, i}], {i, 0, n - 1}], Table[(Yt[[1]] -> h[[1]]) /. t -> t - i, {i, 1, 2}]]]]];
                 (* new system *)
                 {StateSpace[newf, newvar, Ut, t, First@newvar, newYt, Shift], Append[Thread[newvar -> coordinateTransform], newYt[[1]] -> Y]},
                IO,
                 {StateSpace[newf, newvar, Ut, t, First@newvar, newYt, Shift], {newYt[[1]] -> Y}}
                ],
        (* if conditions are not satisfied *)
        Message[ObserverFormTransformability::extcond, BN];
        {},
        Null]
  ]


iOutputTransformation[n_Integer, F_, Ut_List, Yt_List, t_, BN_] := 
 Module[{yind, uind, yy, yu, uy, uu, vars, change, PsiPhi, Psiy},
  yind = Select[Range[0, n - 1], MemberQ[F, (Yt[[1]] /. t -> t + #), Depth[F]]&];
  uind = Select[Range[0, n - 1], MemberQ[F, (Ut[[1]] /. t -> t + #), Depth[F]]&];
  vars = Which[
              (yy = Select[{#, Intersection[Complement[Range[0, n - 1], Range[# - BN, # + BN]], yind]}& /@ yind, #[[2]] =!= {}&, 1]) =!= {},
                {Yt[[1]] /. t -> t + yy[[1, 1]], Yt[[1]] /. t -> t + yy[[1, 2, 1]]},
              (yu = Select[{#, Intersection[Complement[Range[0, n - 1], Range[# - BN, # + BN]], uind]}& /@ yind, #[[2]] =!= {}&, 1]) =!= {},
                {Yt[[1]] /. t -> t + yu[[1, 1]], Ut[[1]] /. t -> t + yu[[1, 2, 1]]},
              (uy = Select[{#, Intersection[Complement[Range[0, n - 1], Range[# - BN, # + BN]], yind]}& /@ uind, #[[2]] =!= {}&, 1]) =!= {},
                {Ut[[1]] /. t -> t + uy[[1, 1]], Yt[[1]] /. t -> t + uy[[1, 2, 1]]},
              (uu = Select[{#, Intersection[Complement[Range[0, n - 1], Range[# - BN, # + BN]], uind]}& /@ uind, #[[2]] =!= {}&, 1]) =!= {},
                {Ut[[1]] /. t -> t + uu[[1, 1]], Ut[[1]] /. t -> t + uu[[1, 2, 1]]},
              True, {}];
  If[vars =!= {},
     PsiPhi = FullSimplify[-D[F, vars[[1]], vars[[2]]] / (D[F, vars[[1]]] D[F, vars[[2]]])];
     change = FullSimplify[Solve[(Yt[[1]] /. t -> t + n) == F, vars[[1]]]][[1]];
     Psiy = FullSimplify[PsiPhi /. change /. t -> t - n];
     FullSimplify[Integrate[E^Integrate[Psiy, Yt[[1]]], Yt[[1]]]],
     Yt[[1]]]
 ]

iOutputTransformation[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, Shift]|IO[eq_, Ut_, Yt_, t_, Shift]), BN_] :=
 Module[{n, F},
  If[And[Head[sys] === StateSpace, Observability[sys] === False],
     Message[Observability::cond];
     {},
     {n, F} = iOrderAndIOFunction[sys];
     If[iTransfToEOFPartialD[n, F, Ut, Yt, t, BN],
        iOutputTransformation[n, F, Ut, Yt, t, BN],
        Message[ObserverFormTransformability::extcond, BN];
        {}, Null]
 ]
]


iVarphiFunctions[n_Integer, PsiF_, Ut_List, Yt_List, t_, BN_] :=
 Module[{coords, symbols, repl, revrepl, varphi},
  coords = Flatten[Table[{(Yt[[1]] /. t -> t + i), (Ut[[1]] /. t -> t + i)}, {i, 0, n - 1}]];
  symbols = Flatten[Table[{Symbol["y"<>ToString[i]], Symbol["u"<>ToString[i]]}, {i, 0, n - 1}]];
  repl = Thread[coords -> symbols];
  revrepl = Thread[symbols -> coords];
  varphi={};
  Table[AppendTo[varphi, Together[PowerExpand[FullSimplify[DSolve[{D[fun@@Flatten[Table[{Yt[[1]], Ut[[1]]} /. t -> t + i, {i, j, BN + j}]], (Yt[[1]] /. t -> t + j)] == D[PsiF - Plus@@varphi, (Yt[[1]] /. t -> t + j)], D[fun@@Flatten[Table[{Yt[[1]], Ut[[1]]} /. t -> t + i, {i, j, BN + j}]], (Ut[[1]] /. t -> t + j)] == D[PsiF - Plus@@varphi, (Ut[[1]] /. t -> t + j)]} /. repl, fun@@Flatten[Table[{Yt[[1]], Ut[[1]]} /. t -> t + i, {i, j, BN + j}]] /. repl, Flatten[Table[{Yt[[1]], Ut[[1]]} /. t -> t + i, {i, j, BN + j}]] /. repl] /. {(C[_][__]):>0, (C[_]):>0}][[1, 1, 2]] /. revrepl]]], {j, 0, n - BN - 2}];
  AppendTo[varphi, FullSimplify[PowerExpand[PsiF - Plus@@varphi]]]
 ]

iVarphiFunctions[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, Shift]|IO[eq_, Ut_, Yt_, t_, Shift]), BN_] :=
 Module[{n, F, PsiF},
  If[And[Head[sys] === StateSpace, Observability[sys] === False],
     Message[Observability::cond];
     {},
     {n, F} = iOrderAndIOFunction[sys];
     If[iTransfToEOFPartialD[n, F, Ut, Yt, t, BN],
        PsiF = FullSimplify[iOutputTransformation[n, F, Ut, Yt, t, BN] /. Yt[[1]] -> F];
        iVarphiFunctions[n, PsiF, Ut, Yt, t, BN],
        Message[ObserverFormTransformability::extcond, BN];
        {}, Null]
  ]
 ]


(* ::Subsubsection:: *)
(*discrete-time / associative form*)


iAssociativeObserverForm[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, Shift]|IO[eq_, Ut_, Yt_, t_, Shift]), newvar_, Phi_]:= 
  Module[{n, F, newXt, uv, yv, YU, coords, derivatives, matrix, omega, phi, lambda, newh, PhiRelation, CapitalPhiDot, CapitalPhi, InverseCapitalPhi, newfun, CoordTransform},
   
     If[ObserverFormTransformability[sys],
       (* if the system is transformable into the associative observer form *)

       {n, F} = iOrderAndIOFunction[sys];
       (* new variables *)
       newXt = NewVariables[newvar, n, t];

       uv = Length[Ut]; (* number of input variables *)
       yv = Length[Yt]; (* number of output variables *)
       YU = Table[Join[Yt, Ut] /. t -> t + i, {i, 0, n - 1}];
       coords = Flatten[YU];

       (* Search of omega *)
       derivatives = FullSimplify[Outer[D, {F}, YU]][[1]];
       matrix = Table[Join[Table[0, {i * (uv + yv)}], derivatives[[i + 1]], Table[0, {(n - 1)(uv + yv) - i * (uv + yv)}]], {i, 0, n - 1}];
       omega = SpanK[matrix, coords];

       (* Search of phi *)
       phi = IntegrateOneForms[omega];

       (* Search of lamda *)
       lambda = FullSimplify[Thread[Plus@@Thread[Times[##]]& @@{derivatives, D[YU, t]}] / D[phi, t]];

       (* Search of Phi *)
       PhiRelation = FullSimplify[Flatten[Table[Table[lambda[[j]] / lambda[[i + j]], {i, 1, n - j}], {j, 1, n - 1}]][[1]], phi[[1]] == x] /. phi[[1]] -> x;
       CapitalPhiDot = Times@@Apply[Power, Select[FactorList[Numerator[PhiRelation]], FreeQ[#, x] == False&], 1] / Times@@Apply[Power, Select[FactorList[Denominator[PhiRelation]], FreeQ[#, x] == False&], 1];
       CapitalPhi = Integrate[CapitalPhiDot, x];
       InverseCapitalPhi = Solve[CapitalPhi == y, x][[1, 1, 2]];

       newfun = FullSimplify[Join[Table[InverseCapitalPhi /. y -> ((CapitalPhi /. x -> newXt[[i + 1]]) + (CapitalPhi /. x -> Reverse[phi][[i]])), {i, n - 1}], {Reverse[phi][[n]]}]] /. Join[Table[coords[[i * 2 + 1]] -> Yt[[1]], {i, n - 1}], Table[coords[[i * 2 + 2]] -> Ut[[1]], {i, n - 1}]] /. Yt[[1]] -> newXt[[1]];

       Switch[Head[sys],
         StateSpace,
           CoordTransform = {h[[1]]};
           Table[FullSimplify[AppendTo[CoordTransform, InverseCapitalPhi /. y -> ((CapitalPhi /. x -> (CoordTransform[[i]] /. t -> t + 1)) - (CapitalPhi /. x -> (Reverse[phi][[i]])))]], {i, n - 1}];
           CoordTransform = FullSimplify[CoordTransform /. Join[Table[coords[[i * 2 + 1]] -> Yt[[1]], {i, n - 1}], Table[coords[[i * 2 + 2]] -> Ut[[1]], {i, n - 1}]] /. Thread[(Xt /. t -> t + 1) -> f] /. Yt[[1]] -> h[[1]]];
           {StateSpace[newfun, newXt, Ut, t, newXt[[1]], Yt, Shift], Join[Thread[newXt -> CoordTransform], {Phi -> CapitalPhi /. x -> Phi[[1]]}]},
         IO,
           {StateSpace[newfun, newXt, Ut, t, newXt[[1]], Yt, Shift], {Phi -> CapitalPhi /. x -> Phi[[1]]}}
       ],
       (* if the system is not transformable into the associative observer form *)
       Message[ObserverFormTransformability::assoccond];
       {}
     ] (* end of If *)
   ]


(* ::Subsection:: *)
(*Observer*)


(* ::Subsubsection::Closed:: *)
(*auxiliary function*)


iVariableWithHat[Subscript[var_, index_][t_]]:= Subscript[OverHat[var], index][t]
iVariableWithHat[var_[t_]]:= OverHat[var][t]
iVariableWithHat[List_]:= iVariableWithHat[#]& /@ List


(* ::Subsubsection::Closed:: *)
(*continuous-time / no output transformation*)


Observer[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, TimeDerivative]|IO[eq_, Ut_, Yt_, t_, TimeDerivative]), var___, K_]:= 
  Module[{n, newvar, OF, transfEq, newf, Cm, newsys},

    Switch[Head[sys],
      StateSpace,
        n = Length[Xt]; (* order of system *)
        newvar = Table[Subscript[nx, i][t], {i, n}], (* new variables *)
      IO,
        n = Head[Head[eq[[1, 1]]]][[1]]; (* order of system *)
        newvar = NewVariables[var, n, t] (* new variables *)
    ];

    If[newvar =!= NewVariables[n, t],
      OF = ObserverForm[sys, newvar, ChangeOfOutput -> False];

      If[OF =!= {},

        (* if the system is transformable into the observer form *)

        Cm = Take[IdentityMatrix[n], 1];

        Switch[Head[sys],
          StateSpace,
            newf = (OF[[1, 1]] /. newvar[[1]] -> a) + Map[List[#]&, K].(Cm.newvar - a);
            transfEq = Xt /. Solve[Thread[newvar == (newvar /. OF[[2]])], Xt, InverseFunctions -> True][[1]];
            newsys = ChangeOfVariables[StateSpace[newf, newvar, Ut, t, First@newvar, Yt, TimeDerivative], Xt, transfEq];
            (newsys /. Thread[Join[Xt, Yt] -> iVariableWithHat[Join[Xt, Yt]]] /. a -> h[[1]])[[1]],
          IO,
            newf = (OF[[1]] /. newvar[[1]] -> a) + Map[List[#]&, K].(Cm.newvar - a);
            newsys = StateSpace[newf, newvar, Ut, t, First@newvar, Yt, TimeDerivative];
            newsys /. Thread[Join[newvar, Yt] -> iVariableWithHat[Join[newvar, Yt]]]/.a -> First@newvar
        ],
        (* if the system is not transformable into the observer form *)
        {}
      ] (* end of second If *)
    ] (* end of first If *)
  ]


(* ::Subsubsection:: *)
(*discrete-time / associative observer*)


AssociativeObserver[sys: (StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, Shift]|IO[eq_, Ut_, Yt_, t_, Shift]), var___, K_]:= 
  Module[{n, newvar, AOF, transfEq, newf,  newsys, Phi, InversePhi},

    Switch[Head[sys],
      StateSpace,
        n = Length[Xt]; (* order of system *)
        newvar = Table[Subscript[nx, i][t], {i, n}], (* new variables *)
      IO,
        n = eq[[1, 1, 1, 1]]; (* order of system *)
        newvar = NewVariables[var, n, t] (* new variables *)
    ];

    If[newvar =!= NewVariables[n, t],
      AOF = iAssociativeObserverForm[sys, newvar, Fun[x]];

      If[AOF =!= {},
        (* if the system is transformable into the associative observer form *)

        Phi = Fun[x] /. AOF[[2, -1]];
        InversePhi = Solve[Phi == y, x][[1, 1, 2]];

        newf = FullSimplify[Table[InversePhi /. y -> ((Phi /. x -> (AOF[[1, 1, i]] /. newvar[[1]] -> a)) + K[[i]]((Phi /. x -> newvar[[1]]) - (Phi /. x -> a))), {i, n}]];

        Switch[Head[sys],
          StateSpace,
            transfEq = Xt /. Solve[Thread[newvar == (newvar /. AOF[[2]])], Xt, InverseFunctions -> True][[1]];
            newsys = ChangeOfVariables[StateSpace[newf, newvar, Ut, t, First@newvar, Yt, Shift], Xt, transfEq];
            (newsys /. Thread[Join[Xt, Yt] -> iVariableWithHat[Join[Xt, Yt]]] /. a -> h[[1]])[[1]],
          IO,
            newsys = StateSpace[newf, newvar, Ut, t, First@newvar, Yt, Shift];
            newsys /. Thread[Join[newvar, Yt] -> iVariableWithHat[Join[newvar, Yt]]] /. a -> First@newvar
        ],
        (* if the system is not transformable into the associative observer form *)
        {}
      ] (* end of second If *)
    ] (* end of first If *)
  ]


(* ::Section:: *)
(*GUI*)


ObservabilityGUI[steq0_StateSpace,{obs_, oFiltr_, oSpace_, unoSpace_, oIdx_}] := 
	Module[{steq, result, comment, ob, of, os, unos, idx},
	steq = ConvertToSubscripted[steq0];
	result = {"System equations: ", steq};
	If[obs, 
		ob = Observability[steq];
		comment = If[ob, "The system is observable.","The system is not observable."];
		result = Join[result, {comment}];
	];
	If[oFiltr, 
		of = ObservabilityFiltration[steq];
		comment = "Observability Filtration:";
		result = Join[result, {comment, of}];
	];
	If[oSpace, 
		os = ObservableSpace[steq];
		comment = "Observable Space: ";
		result = Join[result, {comment, os}];
	];
	If[unoSpace, 
		unos = UnObservableSpace[steq];
		comment = "Unobservable Space:";
		result = Join[result, {comment, unos}];
	];
	If[oIdx, 
		idx = ObservabilityIndices[steq];
		comment = "Observability Indices:";
		result = Join[result, {comment, NLCExpression[idx]}];
	];
	GUIFormat[result, RowLabels->"\[ScriptCapitalO]", RowLabelsStartIndex->0]
]


(* ::Section::Closed:: *)
(*Epilogue*)


(* ::Subsection::Closed:: *)
(*End Functions*)


On[General::spell1]
On[General::spell]


End[]

Protect[{}]

EndPackage[]




