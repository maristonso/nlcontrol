(* ::Package:: *)

DeclarePackage[ "NLControl`Core`", {
"StateSpace", "StateEquations", "StateDefinitions", "IO", "Shift","TimeDerivative",
"ChangeOfVariables", "SpanK", "SpanKVectorFields", "ExactSpanK", "SeparatedSpanK", "De", 
"PseudoLinearMap", "ForwardShift", "BackwardShift", "PseudoD", "MaxPLMOrder", "MinPLMOrder",
"IdentityShiftQ", "ZeroDerivativeQ", "ClassicStateQ", "PopovFormQ",
"ModelToRules", "DetectVariables", "NewVariables", "RemoveAppliedOperator",
"FromIOToGeneralizedState", "ExtendedState",
"Submersivity", "NegativeTimeShifts", "BackwardShiftOperator",
"BookForm", "RowLabels", "RowLabelsStartIndex", "TimeArgument", "Superscripted", (*Subscripted is excluded to avoid the context conflict*)
"TimeArgumentSymbol", "FormalT", "PolynomialVariableSymbol", "FormalZ", "RowHeadings", 
"ReplaceTimeArgumentBoxes", "CoefficientBoxes", "CoordinateBoxes", "PolynomialBoxes", 
"AllowRealNumbers", 
"BookFormHolder", "ToNLCExpression", 
"NLCToTeX",
"SubmersivityGUI", "GUIFormat", "ConvertToSubscripted"}
]


DeclarePackage[ "NLControl`OneForms`", {
"ExteriorD", "ToDe",(*Wedge is excluded to avoid the context conflict*)
"Integrability", "IntegrateOneForms", "Frobenius", "IntegrateExactOneForms", "IntegratingFactors",
"BottomDerivedSystem", "IntegrateOneFormsIfPossible",
"ComplementSpace", "SameSubspace", "SimplifyBasis", "IntersectionSpace", "UnionSpace",
"Basis", "SequenceH", "SequenceI", "SequenceS", "SequenceD",
"VerifySequence", "SequenceLengthTest",
"LieBrackets", "LieD", "InvolutiveClosure", 
"UseExtendedState",
"SequenceHGUI"
}]


(* Declarations for file NLControl`Ore`*)
DeclarePackage[ "NLControl`Ore`", {
"\[Eth]",
"ShiftAndDerivativeRules", "ShiftAndDerivativeAdjointRules", "Adjoint",
"$OreRing", "OreRing", "DefineOreRing", "DefineAdjointOreRing", 
"OreP", "OreR", 
"OreSimplify", "OreExponent", "GenerateOreP", "LeadingElement",
"OreCancel", "RemoveDoubleFractions",
"OreAdd", (*for OreR *)
"OreMultiply", (* for OreP and OreR*)
(*"OreDot",(* for matrices containing OreP as elements *)*)
"LeftQuotientRemainder", "RightQuotientRemainder",
"LeftQuotient", "RightQuotient",
"LeftRemainder", "RightRemainder",
"CutAndShift",
"LeftEuclideanAlgorithm", "RightEuclideanAlgorithm",
"LeftLCM", "RightLCM",
"LeftGCD", "RightGCD",
(*"LowestDegreePosition", "LLTMStep", "MakeZeroColumn", *)"LowerLeftTriangularMatrix", "OreInverseMatrix",
"FromIOToOreP", "ToOrePD", "ApplyOre", "FromOrePToSpanK", "MBF", "OrePHolder", 
"FromIOToOrePGUI", "OrePolynomialsGUI", "OreRationalsGUI", "PolynomialIO", 

	(*Author: Juri Belikov*)
"iUpperLeftTriangularMatrix", "DieudonneDet", "DieudonneDetExponent", 
"JacobsonForm", "OreCoefficient", "iOreDenominator", "RightOreCondition", "LeftOreCondition", 
"iRightNormalization", "iLeftNormalization", "iRightUniMatrix", "iLeftUniMatrix",
"iMatrTransf"}];


(* ======================= Control Problems ========================*)

(* Declarations for file NLControl`Modelling`*)
DeclarePackage["NLControl`Modelling`", {
"RelativeDegree",
"ColumnReducedQ", "RowReducedQ", "DoubleReducedQ", "DoublyReducedQ",
"PopovFormQByOreP", "StrongPopovFormQ", "PopovForm", "PopovFormMatrix",
"RowReductionMatrix", "ColumnReductionMatrix",
"RowReduction", "ColumnReduction", "DoubleReduction", "DoublyReduce","RowReductionMatirx",
"ReduceConstantTerms", "CanonicalIO",
"StateElimination", "StateSpaceToIO",
"ReducedDifferentialForm", "Irreducibility", "Reduction", "AutonomousElementsD",
"StateDifferentialsLeftQuotient", "StateDifferentialsAdjoint",
"Realizability", "Realization",
"ClassicTransformability", "ClassicTransform", "Lower", "LoweringOrder",
"TransferFunction",
"ReductionGUI", "RealizationGUI", "StateSpaceToIOGUI", "TransferFunctionGUI"}];


(* Declarations for file NLControl`Analysis` *)
DeclarePackage["NLControl`Analysis`", {
"Accessibility", "AccessibilityDecomposition",
"Identifiability", "TransformToSeparable", "Pearson", "Algebraic", (* Author:Janek Tabun *)
"AccessibilityGUI", "IdentifiabilityGUI"}];


(* Declarations for file NLControl`Observability` *)
(* Author: Vadim Kaparin *)
DeclarePackage["NLControl`Observability`", {
"Observability", "ObservabilityFiltration", "ObservableSpace", "UnObservableSpace", 
"ObservabilityIndices", "ObservabilityDecomposition", "Result", 
"iOrderAndIOFunction", "ObserverFormTransformability", "iTransfToOFnoOT", 
"iFindOmegas", "iTransfToEOFOneForms", "iVarphiFunctions", "MinBuffer", "PartialDerivatives", 
"OneForms", "iTransfToOFwithOT", "iTransfToEOFPartialD", "iOutputTransformation", 
"iExtendedObserverForm", "ChangeOfOutput", "ObserverForm", "iObserverFormNoOT", 
"iObserverFormWithOT", "Observer", "iVariableWithHat", "iTransfToAOF", 
"iAssociativeObserverForm", "AssociativeObserver",
"ObservabilityGUI", "ObserverFormGUI"
}];


(* Declarations for file NLControl`Synthesis`*)
DeclarePackage["NLControl`Synthesis`", {
"TransformabilityToNormalForm", "NormalForm",
"WkBasisDifferential", "WkBasis", "WkBarBasis",
"Linearizability", "Linearization", "PartialLinearization",
"EquivalenceToPrimeSystem", "PrimeSystem",
"FeedforwardCompensator", "FeedbackCompensator",(* Author: Juri Belikov *)  
"LinearizationGUI", "ModelMatchingGUI"
}];


(* Declarations for file NLControl`TimeScale` *)
DeclarePackage["NLControl`TimeScale`", {
"TimeScale", "DeltaDerivative", "DeltaD", "Jump", "\[Mu]", "\[Sigma]", "\[CapitalDelta]"}];


CellPrint[Cell["NLControl package loaded","Text", 
CellFrame->True,
CellFrameColor->RGBColor[0.69, 0, 0],
FontColor->RGBColor[0.69, 0, 0]]];
