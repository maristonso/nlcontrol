(* ::Package:: *)

(* :Title:  NLControl`Ore` *)
(* :Context: NLControl`Ore` *)
(* :Authors: Maris Tonso, Juri Belikov*)
(* :Package Version: 2021 *)
(* :Mathematica Version: 11.3, 12 *)
(* :Institution: Department of Software Science at Tallinn University of Technology, Estonia *)

BeginPackage["NLControl`Ore`", {"NLControl`Core`", "NLControl`OneForms`"}]



(* ::Subsection:: *)
(*Usage messages*)


(* pre-definations for Ore polynomials *)
ShiftAndDerivativeRules::usage = "Composes a list of operators neccessary to work with Ore polynomials."
ShiftAndDerivativeAdjointRules::usage = "Composes a list of operators neccessary to work with Adjoint Ore polynomials."
Adjoint::usage = ""


OreRing::usage = ""

\[Iota]::usage="Symbol in replacement patterns in OreRing"
DefineOreRing::usage = "Defines Ore polynomial ring."
DefineAdjointOreRing::usage = "Defines Ore polynomial ring."

OreP::usage = "Represents Ore polynomial an*x^n + ... + a1*x + a0 in form OreP[an, ..., a1, a0]."
OreR::usage ="Fraction of Ore polynomials p^-1 * q = OreR[p, q]. Note the reversed order, if compared with normal rational expression."

OrePHolder::usage = "Holds together Ore polynomial and description of the Ore ring"


OrePseudoLinearMap::usage = "OrePseudoLinearMap[OreP[], {t, n}, OreRing[]] - applies pseudLinear Map to Ore polynomial n times."
OreSimplify::usage = "Simplifies Ore polynomial regarding i/o equations(??)"
OreAdd::usage = "Temporarily represents OrePlus."
OreMultiply::usage = "noncommutative multiplication of Ore polynomials. You can use ** for ore multiplication also."
OreDot::usage = "OreDot[A, B] multiplies matrices A and B containing Ore polynomials as elements."

iOreMultiplyList::usage = ""
iOrePseudoLinearMapList::usage = ""


LeftQuotientRemainder::usage = "LeftQuotientRemainder[p, q, K ] finds the left quotient and remainder of Ore polynomials or matrices p and q."
RightQuotientRemainder::usage = "RightQuotientRemainder[p, q, K ] finds the right quotient and remainder of Ore polynomials or matrices p and q."
LeftQuotient::usage = "LeftQuotient[p, q, K ] finds the left quotient of Ore polynomials p and q." 
RightQuotient::usage = "RightQuotient[p, q, K ] finds the right quotient of Ore polynomials p and q." 
LeftRemainder::usage = "LeftQRemainder[p, q, K ] finds the left remainder of Ore polynomials p and q."
RightRemainder::usage = "RightRemainder[p, q, K ] finds the left remainder of Ore polynomials p and q." 
CutAndShift::usage = "Applies (generlized) Cut-and-shift operator to the Ore polynomial."

LeftEuclideanAlgorithm::usage = "computes extended left Euclidean algorithm of two Ore polynomials p and q."
RightEuclideanAlgorithm::usage = "computes extended right Euclidean algorithm of two Ore polynomials p and q."
LeftLCM::usage = "finds the least common left multiple of Ore polynomials p and q."
RightLCM::usage = "finds the least common right multiple of Ore polynomials p and q."
LeftGCD::usage = "finds the left greatest common divisor of Ore polynomials p and q."
RightGCD::usage = "finds the left greatest common divisor of Ore polynomials p and q."

LLTMStep::usage = "LLTMStep[mat, {i,j}] makes a transformation with 
mat, using element {i,j} as a leading elemnt. Makes transformations with columns."
MakeZeroColumn::usage = "MakeZeroColumn[mat, {i, j}, K] transforms all elements of the j-th column to zero except element (i,j). 
Makes transformations with rows."
LowestDegreePosition::usage = "Returns the position of the lowest degree nonzero element."

LowerLeftTriangularMatrix::usage = "finds the lower left triangular form of the matrix."
OreInverseMatrix::usage = "Finds inverse of the matrix of Ore polynomials."

OreCancel::usage = "Cancels fraction of ore polynomials"
RemoveDoubleFractions::usage = "Removes double fractions fo Ore polynomial"


FromIOToOreP::usage = "transfroms i/o equations to corresponding polynomials matrix"
FromOrePToSpanK::usage = "transforms matrix of polynomials to Span"


GenerateOreP::usage = "GenerateOreP[n]->\[Eth]^n"
LeadingElement::usage = ""


OreExponent::usage = "Gives the maximum power of the noncommutative polynomial."
iUpperLeftTriangularMatrix::usage = "Transforms the matrix to an upper triangular form of the matrix."
DieudonneDet::usage = "Calculates Dieudonne determinant of the square matrix."
DieudonneDetExponent::usage = "Calculates the maximum power of noncommutative polynomial obtained after finding Dieudonne determinant."


JacobsonForm::usage = "Transforms step-by-step matrix to the Jacobson form"
iOreCoefficient::usage = "iOreCoefficient[poly,n] Returns the n-th coefficient of the polynomial poly"
iOreDenominator::usage = "iOreDenominator[elem] Returns the denominator of elem"
RightOreCondition::usage = "RightOreCondition[p,q,R] returns {p1,q1} such that p*p1 - q*q1 = 0."
LeftOreCondition::usage = "LeftOreCondition[p,q,R] returns {p1,q1} such that p1*p - q1*q = 0."
iRightNormalization::usage = "Returns the matrix with monic polynomials in the rownum-th row"
iLeftNormalization::usage = "Returns the matrix with monic polynomials in the colnum-th column"
iRightUniMatrix::usage = "Returns the right unimodular matrix"
iLeftUniMatrix::usage = "Returns the left unimodular matrix "
iMatrTransf::usage = "Transforms the matrix to the new form. Finds the element (p_{ij} for i=currow,...,p and j=currow,...,m) 
with the lowest degree and puts in into position (currow, currow)."




(* Converting between OreP and OrePolynomial *)

ToOreP::usage =  ""
ToOrePD::usage = "ToOrePD[sys, vars] finds the Ore polynomial matrix P such that P applied to vars gives the system sys."
ApplyOre::usage = "Universal operator allowing to applying Ore Polynomials to all kind of expressions."

FromOrePToNormal::usage =  ""

FromOrePToNormal::usage = ""
FromOrePolynomialToOreP::usage =  ""
FromOrePToOrePolynomial::usage =  ""


PolynomialIO::usage =""
FromIOToOrePGUI::usage = ""
OrePolynomialsGUI::usage = ""
OreRationalsGUI::usage = ""



(* ::Subsection:: *)
(*Begin programs*)


Begin["`Private`"]

Off[General::spell1]
Off[General::spell]
epsilon = 10^-15;


(* ::Subsection:: *)
(*Defining Ore ring, Adjoint ring*)


       ShiftAndDerivativeRules[t_, TimeDerivative] := {TimeDerivative, t->t, t->t, D[#, t]&}
ShiftAndDerivativeAdjointRules[t_, TimeDerivative] := {Adjoint[TimeDerivative], t->t, t->t, -D[#, t]&}


       ShiftAndDerivativeRules[t_, Shift] := {Shift, t->t+1, t->t-1, 0&} 
ShiftAndDerivativeAdjointRules[t_, Shift] := {Adjoint[Shift], t->t-1, t->t+1, 0&}


OreRing /: MakeBoxes[ OreRing[x_, t_, opRules_, info_, rules_List], form_ ] := 
	MakeBoxes[ OreRing[x, t, opRules, info, Shallow[rules, 2]], form]


ConstructOreInfo[eqs_List, Zt_List, Ut_, bwop_List, t_, op_] := Module[
{coords, n, \[CapitalOmega]t, Wt, ni, lowerBound, upperBound, lowerSign, upperSign, chrs, inequalities},
	coords = Join[ Zt, Ut ];
	n = Length@coords;
	\[CapitalOmega]t = ForwardShift[ First/@bwop, t, op]; (*Dependant variables, which will be replaced from system eqs.*)
	Wt = If[ \[CapitalOmega]t==={}, (*Wt are independant vars, which are allowed to appear at negative timem shifts, Wt as denoted in Global... paper.*) 
		{}, (*continuous-time case *) 
		Complement[coords, \[CapitalOmega]t] (* discrete-time case *)
	]; 
	ni = MaxPLMOrder[eqs, #, op]& /@ Zt;
	lowerBound = If[ MemberQ[ Wt, #], -\[Infinity], 0]& /@ coords;
	upperBound = PadRight[ ni-1, n, {\[Infinity]} ];
	lowerSign = If[#===-\[Infinity], Less, LessEqual]& /@ lowerBound;
	upperSign = If[#===\[Infinity], Less, LessEqual]& /@ upperBound;
	chrs = FromCharacterCode /@ (104+Range[n]);
	inequalities = {lowerBound, lowerSign, chrs, upperSign, upperBound};
	inequalities = Inequality @@@ Transpose[inequalities];
	MapThread[{ PseudoLinearMap[#1, {t,#2}, op], #3}&, {coords, chrs, inequalities}]
]

ConstructOreUpperRules[eqs_List, Zt_List, t_, op_] := Module[{phi, ni, lhs, rhs},
	phi = Last/@ eqs;
	ni = MaxPLMOrder[eqs, #, op]& /@ Zt;
	lhs = MapThread[ PseudoLinearMap[#1, {t, Pattern[\[Iota],_]/;\[Iota] >= #2}, op]&, {Zt, ni}];
	rhs = MapThread[ PseudoLinearMap[#1, {t, \[Iota]-#2}, op]&, {phi, ni}];
	Thread[ lhs -> rhs]
]

ConstructOreLowerRules[bwop_List, t_, op_] := Module[
{\[CapitalOmega]t, psi, lhs, rhs},
	\[CapitalOmega]t = ForwardShift[ First/@bwop, t, op];
	psi = Last /@ bwop;
	lhs = \[CapitalOmega]t/.t-> (t+\[Iota]_/;\[Iota]<0);(* Check if works for timescales *)
	rhs = BackwardShift[ psi, {t, -\[Iota]-1}, op];
	Thread[ lhs -> rhs ]
]


DefineOreRing[x_, t_Symbol, op_] := OreRing[x, t, ShiftAndDerivativeRules[t, op]] 

(*eqs0 can be given by "\[Equal]" or by "\[Rule]" *)
DefineOreRing[x_, sys:(IO[eqs0_, Ut_, Zt_, t_, op_]|StateSpace[f_, Zt_, Ut_, t_, _, _, op_, ___]),
bwspec___List] := Module[
	{ eqs, bwop, opRules, info, lowerRules, upperRules},
	eqs = NLCModelToRules[sys]; 
	eqs = Take[eqs, Length@Zt]; (*Remove output equations, possibly included by NLCModelToRules*)
	bwop = BackwardShiftOperator[sys, bwspec];
	opRules = ShiftAndDerivativeRules[t, op];
	info = ConstructOreInfo[eqs, Zt, Ut, bwop, t, op];
	upperRules = ConstructOreUpperRules[eqs, Zt, t, op];
	lowerRules = ConstructOreLowerRules[bwop, t, op];
	OreRing[x, t, opRules,  info, Join[lowerRules, upperRules] ]	
]


DefineAdjointOreRing[Z_, t_Symbol, op_] := OreRing[SuperStar[Z], t, ShiftAndDerivativeAdjointRules[t, op]]

DefineAdjointOreRing[Z_, sys:(IO[eqs0_, Ut_, Zt_, t_, op_]|StateSpace[f_, Zt_, Ut_, t_, _, _, op_, ___]), 
bwspec___] := Module[{R, adRules},
	R = DefineOreRing[Z, sys, bwspec ];
	adRules = ShiftAndDerivativeAdjointRules[t, op];
	OreRing[ SuperStar[Z], t, adRules, R[[4]], R[[5]] ]
]


(* ::Subsection:: *)
(*OreP*)


(* OreP[an,..., a1, a0] === an*s^n + ... + a1*s + a0 *)

OreP[0, ai___] := OreP[ai]
OreP[] := 0
OreP[c_?NumericQ] := c

c_ + OreP[ ai___, a0_] ^:= OreP[ai, a0+c] /; Head[c] =!= OreP
c_*OreP[ coef__] ^:= OreP@@(c*{coef}) /; Head[c] =!= OreP

OreP[ coef1__] + OreP[ coef2__] ^:= With[
	{maxi = Max[ Length[{coef1}], Length[{coef2}] ]},
	OreP @@ Simplify[ PadLeft[ {coef1}, maxi] + PadLeft[ {coef2}, maxi]]
]

OreP/:Power[OreP[a0_],-1] := OreP[1/a0] (*Apr 2012*)


OreSimplify[expr_, OreRing[x_, t_, opRules_]] := Simplify[expr/. (OreP|OreR)[c_/;FreeQ[c, t]]->c];
OreSimplify[expr_, OreRing[x_, t_, opRules_, info_, replrules_ ]] := OreSimplify[ expr//.replrules,  OreRing[x, t, opRules]]


GenerateOreP[deg_Integer]:=OreP @@ PadRight[ {1}, deg+1];
LeadingElement[OreP[a_,___]]:=a
LeadingElement[a_]:=a


OreExponent[pList_List] := OreExponent[#]& /@ pList
OreExponent[p_OreP] := Length[p] - 1
OreExponent[OreR[p1_, p2_]] := OreExponent[p2]- OreExponent[p1]
OreExponent[0] := -Infinity
OreExponent[const_] := 0


(* ::Subsection::Closed:: *)
(*\[Theta]*a, Ore multiplication, Adjoint*)


iOrePseudoLinearMapList[P_List, {t_, n_}, OreRing[x_, t_, {type_, sigma_, sigmainv_, delta_}, ___]] := 
	Nest[ Function[thetaP, 
		Join[thetaP/.sigma,{0}] + Join[{0}, delta /@ thetaP]
	], List@@P, n] // Simplify(* End Nest *)

OrePseudoLinearMap[P_OreP, {t_, n_}, K_OreRing] := OreSimplify[OreP @@ iOrePseudoLinearMapList[ List@@P, {t, n}, K], K]
OrePseudoLinearMap[P_, t_Symbol, K_OreRing] := OrePseudoLinearMap[P, {t, 1}, K]

Adjoint[p_OreP, AdK:OreRing[x_, t_, __]] := With[{m = Length@p},
	OreSimplify[Plus@@Table[ OreP@@ iOrePseudoLinearMapList[ {p[[i]]}, {t, m-i}, AdK], {i, 1, m}], AdK]
]

Adjoint[P_List, adK_OreRing] := Adjoint[#, adK]& /@ P

Adjoint[a_?NumberQ,_]:=a;

iOreMultiplyList[P_List, Q_List, K:OreRing[x_, t_, __]] := 
With[{m = Length[P], n = Length[Q]},
	OreSimplify[ Plus @@ 
		Table[ PadLeft[ P[[i]]*iOrePseudoLinearMapList[ Q, {t, m-i}, K], m+n-1], {i,1,m}],
	K ]
]

SetAttributes[OreMultiply, Listable ]
OreMultiply[p1_OreP, p2_OreP, K_OreRing] := OreP @@ iOreMultiplyList[List@@p1, List@@p2, K]


OreDot[P_List, Q_List, K_OreRing ] := Inner[ OreMultiply[#1, #2, K]&, P, Q, OreAdd[##, K]& ]
OreDot[P_List, Q_List, R__List, K_OreRing ] := OreDot[ OreDot[P, Q, K], R, K]
OreDot[P_List, R_OreRing] := P  (* OneIdentity *)


(* ::Subsection::Closed:: *)
(*Quotients and Reminders*)


LeftQuotientRemainder[ P_OreP, Q_OreP, K:OreRing[x_, t_, {name_, sigma_, sigmainv_, delta_}, ___]] := 
Module[ {repl, R, Qlist,gammai, gamma, m, n, tol = 10^-10},
	m = Length[P]; 
	n = Length[Q];
	repl = t->Nest[#/.sigmainv&, t, n-1];
	R = List@@P;
    Qlist = List@@Q;
	gamma = Table[
		gammai =(First[R] / First[Q]) /. repl; (*Important for Difference-case!*)
		R = R - iOreMultiplyList[ Qlist, PadRight[{gammai}, i], K];
		R = Simplify[ Chop[ Rest[R], tol]];
		gammai,
	{i, m-n+1, 1, -1}];
	OreSimplify[{ OreP@@gamma, OreP@@R},K]
]


RightQuotientRemainder[ P_OreP, Q_OreP, K:OreRing[x_, t_, {name_, sigma_, _, _}, ___]] := 
Module[ {repl, R, Qlist, gammai, gamma, m, n, tol = 10^-10},
	m = Length[P]; 
	n = Length[Q];
	R = List@@P;
    Qlist = List@@Q;
	gamma = Table[
		(*repl = t->Nest[#/.sigma&, t, i-1];*)
		(*repl = ForwardShift[t, {t,i-1}, name]; Bad point- uses _name _, not actual rule! *)
		repl = t; Do[ repl = repl/.sigma, {i-1}]; repl = t->repl; (* Cumbersome! *)
		gammai = First[R] / (First[Q] /. repl);
		R = R - iOreMultiplyList[ PadRight[{gammai}, i], Qlist, K];
		R = Simplify[ Chop[ Rest[R], tol]];
		gammai,
	{i, m-n+1, 1, -1}];
	OreSimplify[{ OreP@@gamma, OreP@@R}, K]
]



LeftQuotientRemainder::matdim = "Matrix dimensions does not match or the second matrix does 
not have lower left triangular form.";

LeftQuotientRemainder[ P_?MatrixQ, Q_?MatrixQ, K_OreRing ] := 
(* Matrix Q should have a triangular form *)
Module[ {m, abovediag, result, expr, q, r, quot = {}, rem = {}, tol = 10^-10, err },
	m = Length[ Q ];
	abovediag = Flatten[ Table[ Take[Q, {i}, {i+1,-1}], {i, m-1}]];
	result = If[ Length[P] == Length[Q[[1]]] == m && (* checks matrix dimensions *)
		(And @@ (#===0&/@abovediag)), (* checks if all elements above main diagonal are zeros *)
	(* Then *)
		Do[ expr = Chop[ P[[i]] - OreDot[ Take[Q[[i]],i-1], quot, K], tol];
			{q, r} = Transpose[ LeftQuotientRemainder[#, Q[[i,i]], K]& /@ expr]; 
			AppendTo[quot, q];
			AppendTo[rem, r],
		{i, m } ]; 
		{quot, rem}, (* Returns *)
	(* Else *) 
		Message[LeftQuotientRemainder::matdim]; err
	]; (* End If *)
	result /; result =!= err
]

 LeftQuotientRemainder[ a:Except[_OreP], P_OreP, K_OreRing] :=  {0, a}
RightQuotientRemainder[ a:Except[_OreP], P_OreP, K_OreRing] :=  {0, a}
 LeftQuotientRemainder[ a_, b_, K_OreRing] := { a/b, 0}
RightQuotientRemainder[ a_, b_, K_OreRing] := { a/b, 0}

 LeftQuotient[ P_, Q_, K_OreRing ] :=  LeftQuotientRemainder[P, Q, K][[1]]
RightQuotient[ P_, Q_, K_OreRing ] := RightQuotientRemainder[P, Q, K][[1]]
 LeftRemainder[ P_, Q_, K_OreRing ] :=  LeftQuotientRemainder[P, Q, K][[2]]
RightRemainder[ P_, Q_, K_OreRing ] := RightQuotientRemainder[P, Q, K][[2]]



CutAndShift[p_OreP, K:OreRing[x_, t_, {Shift, __}, ___]] := OreSimplify[ Drop[p, -1] /. t->t-1, K]
CutAndShift[p_OreP, K_OreRing] := LeftQuotient[p, OreP[1,0], K]
CutAndShift[P_List, K_OreRing] := CutAndShift[#,K]& /@ P
CutAndShift[c_, K_OreRing] := 0


(* ::Subsection::Closed:: *)
(*OreGCD & OreLCM*)


LeftEuclideanAlgorithm[ poly1_, poly2_, K_OreRing] := NonCommutativeEuclideanAlgorithm[ poly1, poly2, K, "Left"]
RightEuclideanAlgorithm[ poly1_, poly2_, K_OreRing] := NonCommutativeEuclideanAlgorithm[ poly1, poly2, K, "Right"]

NonCommutativeEuclideanAlgorithm[ poly1_, poly2_, K_OreRing, side_] :=
Module[{A, B, R, Q, i, nonComQuotientRemainder},
	nonComQuotientRemainder = ToExpression[side <> "QuotientRemainder"];
	R[0] = poly1;  R[1] = poly2;
    A[0] = 1;      A[1] = 0;
    B[0] = 0;      B[1] = 1;
    i = 1;
    While[ R[i]=!=0,
        i++;
        {Q[i-1], R[i]} = nonComQuotientRemainder[ R[i-2], R[i-1], K ];
        A[i] = OreSimplify[ A[i-2] - OreMultiply[ Q[i-1], A[i-1], K ], K];
        B[i] = OreSimplify[ B[i-2] - OreMultiply[ Q[i-1], B[i-1], K ], K]
    ];
    Table[{R[j], Q[j], A[j], B[j]}, {j, 0, i} ] /. Q[_] -> Null
]



SetAttributes[ {LeftGCD, RightGCD, LeftLCM, RightLCM}, Listable]

(* Matrices *)
(*LeftGCD[P_?MatrixQ, Q_?MatrixQ, K_OreRing] := LowerLeftTriangularMatrix[ MapThread[ Join, {P, Q}], K]*)
(* Nii ei saa defineerida, sest Listable sunnib kohe yle maatriksite Jaotama. *)
				
 LeftGCD[P_, K_OreRing] := P
RightGCD[P_, K_OreRing] := P
 LeftLCM[P_, K_OreRing] := P
RightLCM[P_, K_OreRing] := P

(* Multiple arguments *)
 LeftGCD[P_, Q_, R__, K_OreRing] :=  LeftGCD[  LeftGCD[P, Q, K], R, K]
RightGCD[P_, Q_, R__, K_OreRing] := RightGCD[ RightGCD[P, Q, K], R, K]
 LeftLCM[P_, Q_, R__, K_OreRing] :=  LeftLCM[  LeftLCM[P, Q, K], R, K]
RightLCM[P_, Q_, R__, K_OreRing] := RightLCM[ RightLCM[P, Q, K], R, K]

(* actual computation *)
 LeftGCD[P_, Q_, K_OreRing] := LeftEuclideanAlgorithm[ P, Q, K][[-2, 1]]
RightGCD[P_, Q_, K_OreRing] := RightEuclideanAlgorithm[ P, Q, K][[-2, 1]]
 LeftLCM[P_, Q_, K_OreRing] := OreMultiply[ RightEuclideanAlgorithm[ P, Q, K][[-1, 3]], P, K]
(*RightLCM[P_, Q_, K_OreRing] := OreMultiply[ P, LeftEuclideanAlgorithm[ P, Q, K][[-1, 3]], K]*)
RightLCM[p_, q_, R:OreRing[Z_, t_, {type_, rules__}, repl___]] := Module[{adR, adp, adq},
	adR = DefineAdjointOreRing[Z, t, type, repl];
	adp = Adjoint[p,adR];
	adq = Adjoint[q,adR];
	Adjoint[ LeftLCM[adp,adq,adR], R]
]


(* ::Subsection:: *)
(*Lower left triangular matrix*)


(*Returns the position of the lowest degree nonzero element*)
LowestDegreePosition[A_?ArrayQ] := Module[{d, deg, mindeg, lc, minlc },
    d = ArrayDepth[A];
	deg = OreExponent[A];
	mindeg = Min[ DeleteCases[ Flatten@deg,-Infinity]];
	lc = MapThread[ If[ #2=!=mindeg, Infinity, LeafCount@#1 ]&, {A, deg},d];
	minlc = Min[lc];
	Position[lc, minlc, {d}, Heads->False ][[1]]
]
LowestDegreePosition[A_, pos_]:= LowestDegreePosition[
	Take[A, Sequence @@Transpose @{pos,Dimensions[A]}]
]+pos-1

tol = 10^-10;

(* i - row index, j - column index*)

(*Transforms elements at positions ((i,j+1), (i,j+2) to zeros?*)
LLTMStep[ mat_?MatrixQ, {i_, j_}, K_OreRing] := Module[{A, leading, gamma, A1},
	A = mat;
	leading = A[[i,j]];
	A[[All, {i, j}]] = A[[All, {j, i}]];
	gamma = LeftQuotient[#, leading, K]& /@ A[[i]];
	gamma[[i]] = 0;
	A1 = Outer[ PowerExpand[ OreMultiply[#1, #2, K]]&, A[[All,i]], gamma];
	A = Simplify[ A-A1 ]/.(0.) -> 0;
	Chop[ Simplify@Chop[A, tol], tol]
]

SimplestOrePPosition[Ai_?VectorQ] := Module[{deg, mindeg, lc, minlc },
	deg = Switch[ #, 0, Infinity, _OreP, Length[#], _, 1]& /@ Ai;
	mindeg = Min[deg];
	lc = MapThread[ If[ #2=!=mindeg, Infinity, LeafCount@#1 ]&, {Ai, deg}];
	minlc = Min[lc];
	Position[lc, minlc, {1}, Heads->False ][[1, 1]]
]

LowerLeftTriangularMatrix[ polymat_?MatrixQ, K_OreRing ] := 
Module[ {colnum, rownum, A, Ai, deg, j, leading, gamma },
{rownum, colnum} = Dimensions[polymat];
A = polymat;
Do[
	While[ Ai = Drop[ A[[i]], i-1]; 
	Or @@ (#=!=0& /@ Rest[Ai] ),
		j = SimplestOrePPosition[Ai]+i-1;
		A = LLTMStep[A, {i,j}, K];
	], (* End While *)
{i, rownum} ];
A = Take[A, All, rownum ]; (* Removes zero elements *)
Do[
	leading = A[[i,i]];
	While[ 
		deg = Switch[#, 0, 0, _OreP, Length[#], _, 1]& /@ Take[A[[i]], i];
		j = 0; While[j++; deg[[j]] < deg[[-1]] ];
		j < i,
			gamma = LeftQuotient[ A[[i, j]], leading, K];
			A[[All, j]] = A[[All, j]] - OreMultiply[ A[[All, i]], gamma, K]
	], 
{i, 2, rownum}]; 
A (* Returns *)
] (* End Module *)


(* ::Subsection::Closed:: *)
(*Fractions of Ore polynomials*)


OreR[p2_/;p2=!=0, 0] := 0


(*OreR[a:Except[_OreP], b_] := b/a  2013 - causes error messages. Why? *)
OreR[a:Except[_OreP], OreP[b__]] := OreP[b]/a (* 2013 *)
OreR[ OreP[a0_], const:Except[_OreP]] := OreP[const/a0] (*Apr. 2012*)
OreR[q:Except[_OreP|_Pattern], p:Except[_OreP|_Pattern]]:=p/q (* Jan. 2015 *)

OreP /: Power[p_OreP, -1] := OreR[p, 1] ; (* Nov 2008 *)
OreR /: Power[OreR[p_, q_], -1] := OreR[q, p] ; (* Nov 2008 *)


OreAdd[ OreR[b1_, a1_ ], OreR[b2_, a2_], K_OreRing] := Module[ {M, b1new, b2new},
	M = LeftLCM[b1, b2, K]; (*  b2new * b1 = b1new * b2  *)
	b1new = RightQuotient[M, b2, K];
	b2new = RightQuotient[M, b1, K];
	OreR[ M, OreMultiply[ b2new, a1, K ] + OreMultiply[ b1new, a2, K ] ]
]
OreAdd[args__, K_OreRing]:= Plus[args]/;FreeQ[{args}, OreR]
OreAdd[r_, K_OreRing] := r (*Identity*)
OreAdd[K_OreRing] := 0
OreAdd[r1_, r2_, r3__, K_OreRing] := OreAdd[ OreAdd[r1, r2, K], r3, K] (* Flat *)

OreAdd[ OreR[A0_, B0_], D0_, K_OreRing] := OreR[A0, B0 + OreMultiply[A0, D0, K]]
OreAdd[ D0_, OreR[A0_, B0_], K_OreRing] := OreR[A0, B0 + OreMultiply[A0, D0, K]]

OreR /: c_*OreR[P2_, P1_] := OreR[P2, c*P1] /; Head[c]=!=OreR && Head[c]=!= OreP

(* main program *)
OreMultiply[ OreR[b1_, a1_ ], OreR[b2_, a2_], K_OreRing ] := Module[{M, b2new, a1new},
	M = LeftLCM[a1, b2, K]; (* b2new * a1 = a1new * b2 *)
	b2new = RightQuotient[M, b2, K];
	a1new = RightQuotient[M, a1, K];
	OreR[ OreMultiply[ a1new, b1, K], OreMultiply[ b2new , a2, K] ]
]

OreMultiply[ OreR[A0_, B0_], D0_, K_OreRing ] := OreR[A0, OreMultiply[B0, D0, K]]

OreMultiply[ B0_OreP, OreR[C0_, D0_], K_OreRing ] := Module[{M, B1, C1},
	M = LeftLCM[B0, C0, K];
	B1 = RightQuotient[M, C0, K];
	C1 = RightQuotient[M, B0, K];
	OreR[ C1, OreMultiply[ B1, D0, K] ]
]
OreMultiply[b0_, OreR[C0_, D0_], K_OreRing ] := OreR[C0, b0*D0]


OreMultiply[P_, K_OreRing] := P (* Stands for OneIdentity *)
OreMultiply[P_, Q_, R__, K_OreRing] := OreMultiply[ OreMultiply[P, Q, K], R, K] (*Stands for flat*)
OreMultiply[a_, b_, K_OreRing] := OreSimplify[a b, K]


OreCancel[ OreR[p2_, p1_], K_OreRing] := Module[{alpha, q1, q2, beta},
	alpha = LeftGCD[p2, p1, K];
	q1 = LeftQuotient[p1, alpha, K];
	q2 = LeftQuotient[p2, alpha, K];
	beta = PolynomialGCD @@ ({q1, q2} /. OreP->List // Flatten);
	(*beta = 1;*)
	Simplify[ OreR[q2/beta, q1/beta] ]
]
OreCancel[vec_List, K_]:= OreCancel[#, K]& /@ vec
OreCancel[p_, K_OreRing] := p


RemoveDoubleFractions[OreR[p2_, p1_], K_OreRing] := Module[{polylcm},
	polylcm = OreP[ PolynomialLCM @@ (Denominator/@(List@@p2))]; 
	Simplify[ OreR[ OreMultiply[ polylcm, p2, K], OreMultiply[polylcm, p1, K ] ]]
]
RemoveDoubleFractions[vec_List, K_OreRing] := RemoveDoubleFractions[#, K]& /@ vec
RemoveDoubleFractions[p_, K_OreRing] := p



(* ::Subsection:: *)
(*Inverse Matrix*)


MakeZeroColumn[A_, {row_, col_}, K_OreRing] := Module[{lcm, beta, gamma},
Table[
	If[ i===row||A[[i, col]]===0, A[[i]],
(* Else *) (*to replace by LeftOreCondition*)
	lcm = LeftLCM[ A[[row,col]], A[[i,col]], K];
	beta = RightQuotient[lcm, A[[row,col]], K];
	gamma = RightQuotient[lcm, A[[i,col]], K];
	(OreMultiply[gamma, #, K]& /@A[[i]]) - (OreMultiply[beta, #, K]& /@ A[[row]])
], (* End If *)
{i, Length[A]}]
] (* End Module *)

OreInverseMatrix[mat_?MatrixQ, K_] :=
Module[{m, A, Ainv, pos},
	m = Length[mat];
	A = MapThread[ Join, {mat, IdentityMatrix[m]} ];
	Do[ pos = LowestDegreePosition[ A[[All,i]], {i}][[1]];
		A[[{i,pos}]] = A[[{pos,i}]];
		A = MakeZeroColumn[A, {i,i}, K], 
	{i,m}];
	Ainv = Table[
		OreCancel[OreR[ A[[i,i]], #], K]& /@ Take[A[[i]], -m ],
	{i, m}]
]


(* ::Subsection:: *)
(*Determinant*)


(* Juri Belikov *)


iUpperLeftTriangularMatrix[polyMat_?MatrixQ, K_OreRing]:=
	Block[{A = polyMat, k, l, n},

(* Initialization *)
n = Length[A];

(* Transformation of the matrix to an upper triangular form *)
For[k = 1, k < n, k++,
	If[A[[k, k]] === 0,
		For[l = k, l <= n, l++,
			If[A[[l, k]] =!= 0, Break[]];
		];
		
		If[l <= n,
			A[[{k, l}]] = A[[{l, k}]];
			A[[k + 1 ;; All, k + 1 ;; All]] = Table[OreAdd[A[[i, j]],
				-OreMultiply[A[[i, k]], Power[A[[k, k]], -1], A[[k, j]], K], K], {i, k + 1, n}, {j, k + 1, n}];
			A[[k + 1 ;; All, k]] = 0,
			Break[]
		],
		A[[k + 1 ;; All, k + 1 ;; All]] = Table[OreAdd[A[[i, j]],
			-OreMultiply[A[[i, k]], Power[A[[k, k]], -1], A[[k, j]], K], K], {i, k + 1, n}, {j, k + 1, n}];
		A[[k + 1 ;; All, k]] = 0
	];
];

A

]


DieudonneDet[polyMat_?MatrixQ, K_OreRing]:=
	Block[{diagPoly, finalPoly, uperLeftTriangularMatrix},

uperLeftTriangularMatrix = iUpperLeftTriangularMatrix[polyMat, K];

diagPoly = Diagonal[uperLeftTriangularMatrix];
finalPoly = OreMultiply[Sequence @@ diagPoly, K]
]


DieudonneDetExponent[polyMat_?MatrixQ, K_OreRing] :=
	Block[{degSet, detDeg = {}, diagPoly, uperLeftTriangularMatrix},

uperLeftTriangularMatrix = iUpperLeftTriangularMatrix[polyMat, K];

diagPoly = Diagonal[uperLeftTriangularMatrix];
degSet = OreExponent[diagPoly];
detDeg = Plus@@degSet;

detDeg
]


(* ::Subsection:: *)
(*Jacobson form*)


(* Juri Belikov *)


(* Returns the num-th coefficient of the polynomial poly *)
OreCoefficient[poly_, num_] := Block[{pow},
	pow = OreExponent[poly];

	If[Head[poly] === OreP,
		If[num > pow || num < 0, 0, poly[[pow-num+1]]],
		poly
	]
];

(* Returns the denominator of elem *)
iOreDenominator[elem_] := Block[{oreDenominator},
	If[Head[elem] === OreR, oreDenominator = elem[[1]], oreDenominator = Denominator[elem]];
	oreDenominator
];

(* Solves the "right-hand" Ore condition *)
RightOreCondition[A_, B_, K_] := Module[{lowComMult, alpha12, alpha22},
	lowComMult = RightLCM[A, B, K];
	alpha12 = LeftQuotient[lowComMult, B, K];
	alpha22 = LeftQuotient[lowComMult, A, K];

	{alpha22, alpha12}
];
RightOreCondition[A_, 0, K_] := {0, A};
RightOreCondition[0, B_, K_] := {B, 0};
RightOreCondition[0, 0, K_] := {0, 0};

(* Solves the "left-hand" Ore condition *)
LeftOreCondition[A_, B_, K_] := Block[{lowComMult, alpha12, alpha22},
	lowComMult = LeftLCM[A, B, K];
	alpha12 = RightQuotient[lowComMult, B, K];
	alpha22 = RightQuotient[lowComMult, A, K];

	{alpha22,alpha12}
];
LeftOreCondition[A_, 0, K_] := {0, A};
LeftOreCondition[0, B_, K_] := {B, 0};
LeftOreCondition[0, 0, K_] := {0, 0};

(* Returns the right unimodular matrix *)
iRightUniMatrix[poly_, rownum_, colnum_, K_] := Block[{a, b, c, d, m, n, p, p1, p2, rqac, uniMatrix},
	{p, m} = Dimensions[poly];
	p1 = poly[[rownum, rownum]];
	p2 = poly[[rownum, colnum]];
	rqac = LeftEuclideanAlgorithm[p1, p2, K];
	a = rqac[[-2, 3]];
	c = rqac[[-2, 4]];
	{b, d} = RightOreCondition[p1, p2, K];

	uniMatrix = IdentityMatrix[m];
	uniMatrix[[rownum, rownum]] = a;
	uniMatrix[[rownum, colnum]] = b;
	uniMatrix[[colnum, rownum]] = c;
	uniMatrix[[colnum, colnum]] = -d;

	uniMatrix
];

(* Returns the left unimodular matrix *)
iLeftUniMatrix[poly_, rownum_, colnum_, K_] := Block[{a, b, c, d, m, n, p, p1, p2, rqab, uniMatrix},
	p1 = poly[[colnum, colnum]];
	p2 = poly[[rownum, colnum]];
	rqab = RightEuclideanAlgorithm[p1, p2, K];
	a = rqab[[-2, 3]];
	b = rqab[[-2, 4]];
	{c,d} = LeftOreCondition[p1, p2, K];

	uniMatrix = IdentityMatrix[Length[poly]];
	uniMatrix[[colnum, colnum]] = a;
	uniMatrix[[colnum, rownum]] = b;
	uniMatrix[[rownum, colnum]] = c;
	uniMatrix[[rownum, rownum]] = -d;

	uniMatrix
];

(* Returns the matrix with monic polynomials in the colnum-th column *)
iLeftNormalization[poly_?MatrixQ, colnum_] := Block[{curCol, helpList, n},
	curCol = poly[[All, colnum]];
	n = Length[curCol];
	helpList = Table[OreP[OreCoefficient[curCol[[i]], OreExponent[curCol[[i]]]]], {i, 1, n}];

	DiagonalMatrix[
		Table[If[helpList[[i]] =!= 0, Power[helpList[[i]], -1], 1], {i, 1, n}]
	]
];

(* Returns the left and right elementary matrices. These matrices allows to transform the original matrix to the new form in which on the position (currow, currow) is the element of the lowest degree among remained elements. *)
iMatrTransf[poly_, currow_] := Block[{colnum, lim, m, minExp, p, rim, rownum},
	{p, m} = Dimensions[poly];
	{rownum, colnum} = LowestDegreePosition[poly, {currow, currow}];
	lim = IdentityMatrix[p];
	lim[[{currow, rownum}]] = lim[[{rownum, currow}]];
	rim = IdentityMatrix[m];
	rim[[All, {currow, colnum}]] = rim[[All, {colnum, currow}]];

	{lim, rim}
];


(* Transforms step-by-step matrix to the Jacobson form *)
JacobsonForm[poly_?MatrixQ, K_OreRing] := Block[{GSM, k, lim, lnm, leftUniMatrix, lum, m, n, q, p, P = poly, rim, rightUniMatrix, rum, tlum, trum},
	{p, m} = Dimensions[poly];
	n = Min[p, m];
	lum = IdentityMatrix[p];
	rum = IdentityMatrix[m];

(* Finding rational element q and polynomial matrix P *)
	q = LeftLCM[Sequence@@Flatten[Table[iOreDenominator[poly[[i, j]]], {i, p}, {j, m}]], K];
	Table[P[[i, j]] = OreMultiply[q, P[[i, j]], K], {i, p}, {j, m}];

(* The basic part of the function *)
	For[k = 1, k <= n, k++,
		{tlum, trum} = iMatrTransf[P, k];
		P = OreDot[tlum, P, trum, K];
		lum = OreDot[tlum, lum, K];
		rum = OreDot[rum, trum, K];

		While[Or@@(# =!= 0 &/@P[[k, k+1;;m]]) || Or@@(# =!= 0 &/@P[[k+1;;p, k]]),
			If[Or@@(# =!= 0 &/@P[[k, k+1;;m]]),
				Table[
					If[P[[k, j]] =!= 0,
						rightUniMatrix = iRightUniMatrix[P, k, j, K];
						P = OreDot[P, rightUniMatrix, K];
						rum = OreDot[rum,rightUniMatrix,K];
					];
				, {j, k+1, m}];
			];

			If[Or@@(# =!= 0 &/@P[[k+1;;p, k]]),
				Table[
					If[P[[i, k]] =!= 0,
						leftUniMatrix = iLeftUniMatrix[P, i, k, K];
						P = OreDot[leftUniMatrix, P, K];
						lum = OreDot[leftUniMatrix, lum, K];
					];
				, {i, k+1, p}];
			];
		];
	];

(* Normalization of the elements on the main diagonal *)
	Table[
		lnm = iLeftNormalization[P, i];
		P = OreDot[lnm, P, K];
		lum =  OreDot[lnm, lum, K];
	, {i, 1, n}];

	{lum, P, rum}
];


(* ::Subsection::Closed:: *)
(*Converting an applying polynomials*)


(* ::Subsubsection:: *)
(*Transforming: equations -> matrix of polys ->  span (ToOrePD)*)


Clear[ToOrePD];
ToOrePD[ expr_List, vars_?VectorQ, R_OreRing] := ToOrePD[#, vars, R]& /@ expr
ToOrePD[ expr:(_Rule|_Equal), vars_?VectorQ, R_OreRing] := ToOrePD[#1-#2& @@ expr, vars, R]

ToOrePD[ IO[eqs_, Ut_, Yt_, t_, type_], vars_?VectorQ, R___] := ToOrePD[eqs, vars, DefineOreRing[x, t, type]]
ToOrePD[ steq:StateSpace[f_, Xt_, Ut_, t_, _,_,type_, ___Rule], vars_?VectorQ, R___] := 
	ToOrePD[First@ModelToRules[steq], vars, DefineOreRing[x, t, type]]

ToOrePD[ expr_, vars_?VectorQ, R_OreRing] := ToOrePD[expr, #, R]& /@ vars

(* Single expression, singel variable *)
ToOrePD[ expr_, x_[t_], OreRing[z_, t_, {type_,__}]]:=
Module[{n, deriv},
	n = Max[0, MaxPLMOrder[expr, x[t], type]];
	deriv = Reverse @ NestList[ PseudoLinearMap[#, t, type]&, x[t], n];
	Simplify[ OreP @@ D[expr, {deriv}] ]
]

ToOrePD[ expr_, t_, OreRing[z_, t_, {type_,__}]]:=
Module[{allvars, repl},
	allvars = DetectVariables[expr, t, type];
	repl = #->Unique[z]& /@ allvars;
	Simplify[ OreP[ D[expr/.repl, t] /. Reverse/@repl] ]
]


FromIOToOreP[ ioeq:IO[eqs_, Ut_, Yt_, t_, type_]] := { ToOrePD[ioeq, Yt], ToOrePD[ioeq, Ut] }


FromOrePToSpanK[ P_?MatrixQ, QR__?MatrixQ, vars__ ] := FromOrePToSpanK[ MapThread[Join, {P, QR}], vars]
FromOrePToSpanK[ PQ_?MatrixQ, IO[eqs_, Ut_, Yt_, t_, type_]] := FromOrePToSpanK[PQ, Yt, Ut, t, type]

FromOrePToSpanK[ PQ0_?MatrixQ, vars__?VectorQ, t_, type_] := Module[ {PQ, deg, mat, coords, sp, allvars},
	PQ = Map[ If[ Head[#] === OreP, Reverse[List @@ #], {#}]&, PQ0, {2}];
	deg = Max /@ Transpose[ Map[ Length, PQ, {2} ]];
	mat = MapThread[ PadRight, {#, deg}]& /@ PQ;
	coords = MapThread[ Table[ PseudoLinearMap[#1, {t, i-1}, type], {i, #2}]&, 
		{Join[vars], deg}];
	sp = SpanK[ Flatten /@ mat, Flatten[coords]];
	allvars = DetectVariables[mat, t, type];
	AddCoordinatesIfRequired[sp, allvars]
]


(* ::Subsubsection:: *)
(*OreP \[DoubleLeftRightArrow] OrePolynomial*)


ClearAll[FromOrePToNormal];
FromOrePToNormal[vec_List, op_] := FromOrePToNormal[#,op]& /@ vec
FromOrePToNormal[p_OreP, op_] := Dot[List@@p, Table[op^i, {i, Length[p]-1, 0, -1} ] ]
FromOrePToNormal[b_,op_] := b


(*From Normal Expression to OreP*)
Clear[ToOreP]
ToOreP[p_, Z_ ]:= OreP @@ Reverse @ CoefficientList[p,Z]/;PolynomialQ[p, Z]
ToOreP[plist_List, Z_] := ToOreP[#,Z]& /@ plist


Clear[FromOrePolynomialToOreP, FromOrePToOrePolynomial]
SetAttributes[FromOrePolynomialToOreP, Listable]
FromOrePolynomialToOreP[p:OrePolynomial[_,OreAlgebraObject[{op_},__],__]]:=
	ToOreP[ iOrePolynomialToNormal[p], op]

FromOrePToOrePolynomial[p_,op_,A_] := ToOrePolynomial[FromOrePToNormal[p,op],A]


(* ::Subsubsection:: *)
(*ToOrePolynomialD (For IO & StateSpace)*)


(*general def is in OreAlgebraicAnalysis*)


ToOrePolynomialD[steq_StateSpace, vars_List, A_OreAlgebraObject] := 
	ToOrePolynomialD[ ModelToRules[steq][[1]], vars, A]

ToOrePolynomialD[ioeq_IO, vars_List, A_OreAlgebraObject] := 
	ToOrePolynomialD[ ModelToRules[ioeq][[1]], vars, A]


(* ::Subsubsection::Closed:: *)
(*ApplyOre, ApplyMatrix*)


Clear[ApplyOre]
(*ApplyOre[p_OreP, fi_, R:OreRing[z_,t_,{TimeScale,__},___]]      := OreSimplify[ Dot[ Reverse[ NestList[DeltaD[#,t]&, fi, Length[p]-1]],List@@p], R]
ApplyOre[p_OreP, fi_, R:OreRing[z_,t_,{Shift,__},___]]          := OreSimplify[ Dot[ Reverse[ NestList[#/.t->t+1&, fi, Length[p]-1]],List@@p], R]
ApplyOre[p_OreP,fi_, R:OreRing[z_,t_,{TimeDerivative,__},___]] := OreSimplify[ Dot[ Reverse[ NestList[D[#,t]&, fi, Length[p]-1]],List@@p], R]
2015-09-28 *) 
ApplyOre[p_OreP, fi_, R:OreRing[z_, t_, {type_,__}, repl___]] := Dot[ Reverse@NestList[ OreSimplify[ PseudoLinearMap[#,t,type],R]&, fi, Length[p]-1], 
	List@@p ] /; FreeQ[{fi}, De]
ApplyOre[p_OreP, fi_, R:OreRing[z_, t_, {type_,__}, sys___]] := 
Module[{plm,res}, res = Dot[ Reverse@NestList[ 
	ExpandDe[ plm = OreSimplify[ PseudoLinearMap[#,t,type],R], Coordinates->DetectVariables[plm,t,type]]&, 
	fi, Length[p]-1], List@@p ];
	Expand[res]
]
ApplyOre[c_, fi_, _OreRing]:= c*fi


(* For OreModules: 
For one-forms only, operators: Der[t] ja S[t].*)
ApplyOreOperator1[p_OrePolynomial, w0_] := Module[
{dvars, dvars1, w1, w2, repl2},
	dvars = Union[ Cases[{w0}, De[_,{x_[t_]}]->x, -1] ];
	dvars1 = Array[dx[#]&,Length@dvars];
	w1 = w0/.De[a1_,x_]:>De[a1,x/.Thread[dvars->dvars1]]/.De[a1_,{x_}]->a1 x;
	w2 = ApplyOreOperator[p, w1];
	repl2 = {a1_.*Derivative[n_][x:Alternatives@@dvars1][arg_]->
		De[a1,{Derivative[n][x][arg]}],
	a1_.*(x:Alternatives@@dvars1)[arg_] -> De[a1,{x[arg]}]};
	w2/.repl2/.Thread[dvars1->dvars]
] /;!FreeQ[{w0}, De]

ApplyOre[p_OrePolynomial,expr_] := ApplyOreOperator1[p, expr]/;!FreeQ[expr,De]
ApplyOre[p_OrePolynomial,expr_] := ApplyOreOperator[p, expr]


ApplyMatrix[ops_List, vec_List, R_OreRing] := OreSimplify[ Inner[ ApplyOre[#1,#2,R]&, ops, vec]] (*Analogous to OAA*)
ApplyMatrix[ops_List, vec_List] := OreSimplify[Inner[ApplyOre, ops, vec]]
ApplyMatrixD[mat_List, vars_List] := OreSimplify[ Inner[ ApplyOreOperator1, mat, De[#, Coordinates->vars]& /@ vars]]


(*Caution!*)
ApplyMatrix[Umat_, phi_, K_OreRing]:= Simplify[ Plus @@@ OreDot[Umat, OreP /@ phi, K]]


(* ::Subsubsection:: *)
(*FromOrePolynomialToSpanK*)


Clear[FromOrePolynomialToSpanK]
FromOrePolynomialToSpanK[R_?MatrixQ, vars_?VectorQ, t_Symbol,type_]:=
	FromOrePToSpanK[ FromOrePolynomialToOreP[R], vars, t, type]


(* ::Subsection:: *)
(*BookForm: OreP, OreR, TransferFunction*)


(* --------- OreP: Formatting coordinates ----------------------------------------*)
CoordinateBoxes[ P_OreP, K:OreRing[x_,__], form_] := Module[ {xbox, n}, 
	n = Length[P]; 
	xbox = MakeBoxes[x, form];
	Switch[ n,
		1, {{}},
		2, {{xbox}, {} },		
		_, Join[ Table[ {SuperscriptBox[ xbox, ToString[i]]}, {i, n-1, 2, -1}], {{xbox},{}} ]
	]
]


BookForm /: MakeBoxes[ BookForm[ P_OreP, R:OreRing[x_, t_, type_, ___], opts___ ], form_ ] := 
Module[{ coefs, coords, polybxs },
	coefs = CoefficientBoxes[List@@P, form];
	coords = CoordinateBoxes[P, R, form];
	polybxs = PolynomialBoxes[ coefs, coords];
	ReplaceTimeArgumentBoxes[ polybxs, t, form, opts]
]

BookForm /: MakeBoxes[ BookForm[ OreR[p2_, p1_], R_OreRing, opts___Rule], form_ ] := 
	FractionBox[ MakeBoxes[ BookForm[p1, R, opts], form], 
				 MakeBoxes[ BookForm[p2, R, opts], form]
    ]



PolynomialIO[ioeq:IO[eqs_, Ut_, Yt_, t_, op_]] := Module[{P, Q},
	{P,Q} = FromIOToOreP[ioeq];
	PolynomialIO[P, Q, Ut, Yt]
]

BookForm/: MakeBoxes[ BookForm[PolynomialIO[P_,Q_,Ut_,Yt_], R:OreRing[s_, t_, op_], opts___], form_] := 
Module[{bxsP, bxsQ, bxsdY, bxsdU},
	{bxsP, bxsQ} = MakeBoxes[ MatrixForm[#], form]& /@ BookForm[{P,Q}, R, opts];
	bxsdY = GridBox[{RowBox[{"d", MakeBoxes[ BookForm[ #, t, opts], form]}]}& /@ Yt];
	bxsdU = GridBox[{RowBox[{"d", MakeBoxes[ BookForm[ #, t, opts], form]}]}& /@ Ut];
	If[ Length@Yt>1, bxsdY = RowBox[{"(", bxsdY, ")"}]];
	If[ Length@Ut>1, bxsdU = RowBox[{"(", bxsdU, ")"}]];
	RowBox[ {bxsP, "\[CenterDot]", bxsdY, "+", bxsQ, "\[CenterDot]", bxsdU, "=", "0"}]
]





BookForm[polys_List, R_OreRing, opts___] := BookForm[#, R, opts]& /@ polys
BookForm[ BookFormHolder[p_, R_OreRing], opts___] := BookForm[p, R, opts] (*Holds polys and R together as 1 argument, for GUI*)

BookForm /: MakeBoxes[ BookForm[ expr_, OreRing[x_, t_, type_, ___], opts___ ], form_ ] := MakeBoxes[BookForm[expr,t],form]
(*BookForm[ expr_, OreRing[x_, t_, type_, ___], opts___ ] := BookForm[expr,t] Puts into beginning, ruins the other defs.*)



(* ::Subsection:: *)
(*GUI*)


FromIOToOrePGUI[ioeq0:IO[eqs_, Ut_, Yt_, t_, type_], s_] := 
Module[{result, R, polyRep, comment, ioeq},
	ioeq = ConvertToSubscripted[ioeq0];
	R = DefineOreRing[s, t, type];
	comment = "Polynomial representation:";
	polyRep = PolynomialIO[ioeq];
	GUIFormat[ {"System equation(s)", ioeq, comment, BookFormHolder[polyRep, R]}]
]


OrePolynomialsGUI[ OreOperation_, p_, q_, s_, ioeq:IO[eqs_, Ut_, Yt_, t_, type_]] := 
Module[{R, result, comment, opResult},
	If[eqs === Null,
		R = DefineOreRing[s, t, type]; result ={};,
		R = DefineOreRing[s, ioeq]; result = {"System equation(s): ", ioeq}
	];
	comment = If[Head[p] === OreR || Head[q] === OreR, "Rational expressions of Ore Polynomials: ", "Ore Polynomials: "];
	result = Join[result, {comment, BookFormHolder[{p,q}, R]}];
	opResult = OreOperation[p, q, R];
	GUIFormat[ Join[result, {"Result: ", BookFormHolder[opResult, R]}] ]
]


(* ::Subsection:: *)
(*End package*)


On[General::spell1]
On[General::spell]


End[]

Protect[{}
]

EndPackage[]


