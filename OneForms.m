(* ::Package:: *)

(* :Title:  NLControl`Core` *)
(* :Context: NLControl`Core` *)
(* :Author: Maris Tonso *)
(* :Package Version: 2021 *)
(* :Mathematica Version: 11.3, 12 *)
(* :Institution: Department of Software Science at Tallinn University of Technology, Estonia *)

BeginPackage["NLControl`OneForms`", {"NLControl`Core`"}]


(* ::Subsection:: *)
(*Usage messages*)


ToDe::usage = "converts SpanK to the list of differential oneforms, reprended by object De[]."

ExteriorD::usage = "Exterior derivative"

ExpandDe::usage = "Expands De objects: ExpandDe[De[x,s {y z}]]-> De[x y, {z}] + De[x z, {y}]. 
This function is only used in ApplyOre function in Ore.m." 

Integrability::usage = "tests integrability of the pfaffian system"

Frobenius::usage = "uses Frobenius'es theorem to check complete integrability" 

IntegrateOneForms::usage = "Integrates the pfaffian system, if possible"

IntegrateExactOneForms::usage = ""

IntegratingFactors::usage = "IntegratingFactors[SpanK[mat,coords], {x1,x2,...}] -> {}"

BottomDerivedSystem::usage = "find bottom derived system"

IntegrateOneFormsIfPossible::usage =""


(* Operations with spans/spaces/sets*)
ComplementSpace::usage = "ComplementSpace[A, B] - gives difference of subspaces A-B or A\\B. 
A and B are supposed to be matrices with the equal row length.\n
ComplementSpace[SpanK[sp1,coords], SpanK[sp2,coords]] - does the same with SpanK-objects."

SameSubspace::usage = "Tests if two basis represent the same subspace."

SimplifyBasis::usage = "simplifies a basis of subspace"

IntersectionSpace::usage = "finds intersection of two subspaces"

UnionSpace::usage = "finds union of two subspaces"


AddCoordinatesIfRequired::usage = ""
DeleteCoordinatesIfNotRequired::usage = "used in ForwardShift and BackwardShift"


(*Sequences of one-forms and distributions*)

Basis::usage = "Finds I1 = H1"

SequenceH::usage = "computes sequence Hk."
(*classicstate, generalizedstate, io*)

SequenceI::usage = "computes sequence Ik for discrete systems"
(*classicstate, generalizedstate, io*)

VerifySequence::usage = "VerifySequence[{Dk, Hk, Ik}, StateSpace[f, Xt, Ut, t, h, Yt, Shift] ]  
tests if certain coditions are fulfilled for sequences Dk, Hk and Ik. For discrete systems only."

SequenceLengthTest::usage = "SequenceLengthTest[Hk, m] Tests if the sequence elements have a proper number basis vectors."

LieBrackets::usage = "LieBrackets[vec1, vec2, coords] computes Lie Brackets of vector v1 and v2."

LieD::usage = "LieD[f,h,Xt] computes the Lie derivative L_f (h)"

Involutivity::usage = "Involutivity[ SpanK[mat, coords, 1, t] ] tests if the distribution is involutive or not."

InvolutiveClousure::usage = "InvolutiveClousure[ SpanK[mat, coords, 1, t] ] finds the involutive clousure for the 
distribution."

SequenceS::usage = "computes sequence Sk for continuous systems"

SequenceD::usage = "computes sequence Dk for discrete systems"

UseExtendedState::usage = "Internal function. UseExtendedState[Fun, sys] founds extended state equations
for sys (io or generelized state eqs), then applies Fun and then replaces variables back in the result."

SequenceHGUI::usage =""


(* ::Subsection:: *)
(*Begin programs*)


Begin["`Private`"]

Off[General::spell1]
Off[General::spell]
epsilon=10.^-15;


(* ::Subsection:: *)
(*Exterior derivative, Wedge, SpanK -> De*)


(* time-varying *)
Clear[ExteriorD]
(*ExteriorD[ w_ ] := ExteriorD[w, Union[ Cases[ {w}, _Symbol, -1]]]*)
ExteriorD[ De[a_, x_List], coords_List] := Wedge[ Plus @@ ( De[ D[a,#], {#} ]& /@ coords), De[1, x] ]
ExteriorD[ df_Plus, args___ ] := ExteriorD[#, args]& /@ df
ExteriorD[ df_List, args___ ] := ExteriorD[#, args]& /@ df
ExteriorD[ c_?NumberQ, args___ ] := 0
ExteriorD[ f_, args___ ] := ExteriorD[ De[f, {}], args]

ExteriorD[ w_, t_, type_] := Module[{coords,repl},
	coords = DetectVariables[w, t, type];
	repl = #->Unique[z]& /@ coords;
	ExteriorD[w, coords] + ExteriorD[w/.repl,{t}] /. Reverse/@repl
]



ExpandDe[ w_ ] := ExpandDe[w, Union[ Cases[ {w}, _Symbol, -1]]]
ExpandDe[ w_, t_, type_] := ExpandDe[w, DetectVariables[w, t, type]]
ExpandDe[ De[a_, dif_List], coords_List] := Expand[a * Wedge@@ExteriorD[dif, coords], De]
ExpandDe[ df_Plus, args___ ] := ExpandDe[#, args]& /@ df
ExpandDe[ df_List, args___ ] := ExpandDe[#, args]& /@ df
ExpandDe[ f_, args___ ] := f

Wedge[ df_ ] := df
Wedge[ De[a_,x_], De[b_, y_], df___] := Simplify[ Wedge[ De[ a b, Join[x,y] ], df]]
Wedge[ df1___, df2_Plus, df3___] := Wedge[df1, #, df3]& /@ df2
Wedge[df1___, df2_, df3___] := df2 Wedge[df1,df3] /; FreeQ[{df2}, De]


SetAttributes[ ToDe, Listable ]
ToDe[ SpanK[mat_, coords_]] := Inner[ De[#1, {#2}]&, mat, coords]
ToDe[ SpanK[{}, ___ ]] := {}


(* ::Subsection:: *)
(*PseudoLinearMap, ForwardShift, BackwardShift applied to 1-forms*)


SpanK /: BackwardShift[ sp:SpanK[ { }, _], __] := sp
SpanK /: PseudoLinearMap[ sp:SpanK[ {}, _], _StateSpace] := sp


SpanK /: ForwardShift[ sp_SpanK, steq:StateSpace[ args1__, Shift, args2___]] := PseudoLinearMap[sp, steq]

(* =========================== PseudoLinearMap - Span ================================================ *)
(* Handles aTimeDerivative, Shift and TimeScale systems *)

SpanK /: PseudoLinearMap[ SpanK[mat_, coords_], steq:StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, op_, opts___] ]:=
Module[{allCoords, addCoords, newCoords, mat1, f1, df1, mat2, sp1},
	allCoords = DetectVariables[ PseudoLinearMap[coords, steq], t, op];
	addCoords = Complement[allCoords, coords];
	newCoords = Join[ coords, addCoords];
	mat1 = PadRight[mat, {Length@mat, Length@newCoords} ];
	f1 = PseudoLinearMap[ newCoords, steq];
	df1 = D[f1, {newCoords} ];
	mat2 = ForwardShift[ mat1, steq].df1 + PseudoD[mat1, steq];
	sp1 = SpanK[mat2, newCoords];
	DeleteCoordinatesIfNotRequired[sp1, addCoords]
]


SpanK /: BackwardShift[ SpanK[mat_, coords_], steq:StateSpace[f_, Xt_, _, t_, _, _, Shift, ___], bwspec___ ] :=
Module[{bwrules, allCoords, addCoords, newCoords, mat1, psi1, dpsi1, matMinus, sp},
	bwrules = BackwardShiftOperator[steq, bwspec];
	allCoords = DetectVariables[ coords/.t->t-1/.bwrules, t, Shift];
	addCoords = Complement[allCoords, coords];
	newCoords = Join[coords, addCoords];
	mat1 = PadRight[mat, {Length@mat, Length@newCoords} ];
	psi1 = newCoords /. t->t-1 /. bwrules;
	dpsi1 = D[ psi1, {newCoords}];
	matMinus = (mat1 /. t->t-1 /. bwrules).dpsi1;
	sp = SpanK[ Simplify[matMinus], newCoords];
	DeleteCoordinatesIfNotRequired[sp, addCoords]
]


SpanKVectorFields /: BackwardShift[SpanKVectorFields[Xi_, coords_], steq:StateSpace[f_,Xt_, Ut_, t_, h_,Yt_,type_], bwSpec_] := 
Module[{nonZt, Xit, coordsNew, k, difs, difsPlus, difsPlusA, coefPlus, coef},
	coordsNew = BackwardShiftedCoordinates[coords, steq, bwSpec];
	k = Length@coordsNew;
	difs = SpanK[ IdentityMatrix[k], coordsNew];
	difsPlus = ForwardShift[difs, steq];
	difsPlusA = UpdateSpanKCoordinates[difsPlus, coords][[1]];
	coefPlus = Xi.Transpose[difsPlusA];
	coef = BackwardShift[coefPlus, steq, bwSpec];
	SpanKVectorFields[coef, coordsNew]
]
SpanKVectorFields /: ForwardShift[SpanKVectorFields[Xi_, coords_], steq:StateSpace[f_,Xt_, Ut_, t_, h_,Yt_,type_], bwSpec___] := 
Module[{coordsNew,k, difs, difsMinus, difsMinusA, coefMinus, coef},
	coordsNew = PseudoLinearMappedCoordinates[coords, steq];(*Add new coordinates what may appear in result(??)*)
	k = Length@coordsNew;
	difs = SpanK[ IdentityMatrix[k],coordsNew];
	difsMinus = BackwardShift[difs, steq, bwSpec];
	difsMinusA = UpdateSpanKCoordinates[difsMinus, coords][[1]];
	coefMinus = Xi.Transpose[difsMinusA];
	coef = ForwardShift[coefMinus, steq];
	SpanKVectorFields[coef, coordsNew]
]


(*Finds which differentials may appear in 1-form PLM(\[Omega]), if coords are differentials appearing in 1form \[Omega]*)
PseudoLinearMappedCoordinates[coords_, StateSpace[f_, Xt_,Ut_, t_, h_, Yt_, type_, ___]]:=
	Union@Join[Xt, Ut, PseudoLinearMap[Complement[coords,Xt], t, type]]

BackwardShiftedCoordinates[coords_, StateSpace[f_, Xt_,Ut_, t_, h_, Yt_, type_, ___], bwSpec_]:= 
Module[{Zt, nonZt},
	If[ Head[bwSpec[[1]]] === Rule,
		nonZt = ForwardShift[First/@ bwSpec, t, type];
		Zt = Complement[Join[Xt,Ut], nonZt],
		Zt = bwSpec;
		nonZt = Complement[Join[Xt,Ut], Zt];
	];
	Union @ Join[BackwardShift[Complement[coords, nonZt ], t, type], Xt, BackwardShift[Zt, t, type]]
]


(* ::Subsection::Closed:: *)
(*Integration*)


(* ::Subsubsection:: *)
(*Integrability*)


(*A variable appears in mat, its differentials are missing, consquently the one-forms are not integrable. Converse does not hold!
If MissingDifferentialsCheck returns True, if missing differntials exist and one-forms are not integrable 
Assumes mat is rowreduced!*)

MissingDifferentialsCheck[SpanK[mat_, coords_]] := Module[{posZero},
	posZero = Position[ Transpose[mat], {__?(#===0&)}, {1}];
	Not[ And @@ (FreeQ[ mat, #, -1]& /@ coords[[Flatten@posZero]]) ]
]


Integrability[ sps_List, opts___Rule] := Integrability[#, opts]& /@ sps
Integrability[ SpanK[ {}, __ ], opts___] := True

Integrability[ sp:SpanK[ mat0_, coords_], opts___] := 
	Module[{met, mat},
	met = Method /. {opts} /. Options[ Integrability ];
	mat = RowReduce[mat0/.x_Real:>Rationalize[x]];
	If[ MissingDifferentialsCheck[ SpanK[mat, coords] ],
		False,
	(* Else *)
		If[ met === Frobenius, 
			FrobeniusCondition[mat0, coords],
			IntegrabilityPD[mat, coords]
		]
	] (*End If*)
]

Options[ Integrability ] = {Method -> Automatic};


FrobeniusCondition[mat_, coords_] := Module[{omega, wed},
	omega = ToDe[SpanK[mat, coords]];
	wed = Wedge @@ omega;
	Apply[ And, 
		Chop[ Simplify[ Wedge[
			ExteriorD[#, coords ],
		wed ] ], epsilon ]
	=== 0& /@ omega ]
]

(* Requires RowReduced matrix!! *)
IntegrabilityPD[mat0_, coords_] :=
	Module[{mat, matT, n, k, m, posI, XiI, XiB , Bt, expr1, expr2, expr},
	mat = RowReduce@mat0;
	matT = Transpose@mat;
	{k, n} = Dimensions[mat]; (*{rows, cols}*)
	m = n-k;
	posI = First@Position[#, 1, {1}, 1]& /@ mat; (* rows, starting with 1*)
	XiI = Part[ coords, Flatten[posI] ];
	XiB = Delete[ coords, posI ];
	Bt = -Delete[ matT, posI]; (*B tranposed*)
	expr = Table[
		expr1 = D[ Bt[[i]], { Join[{XiB[[j]]}, XiI]} ].Join[{1}, Bt[[j]] ];
		expr2 = D[ Bt[[j]], { Join[{XiB[[i]]}, XiI]} ].Join[{1}, Bt[[i]] ];
		Chop[ Simplify[ expr1 - expr2 ]], 
	{i, m-1}, {j, i+1, m}]; (*i\[Equal]j\[Equal]m makes no sense*)
	AllTrue[ expr, #===0&, 3]
]


(* ::Subsubsection:: *)
(*IntegrateOneForms, IntegratingFactors*)


Clear[IntegrateOneForms]
IntegrateOneForms::unable = "IntegrateOneForms is unable to solve partial differential equation `1`.";
IntegrateOneForms::nonint = "The subspace spanned over the set of differential 1-forms is not integrable.";

IntegrateOneForms[ sps_List, opts___Rule] := IntegrateOneForms[#,opts]& /@ sps
IntegrateOneForms[ SpanK[ {}, __ ], opts___] := {}

IntegrateOneForms[ sp:SpanK[ mat0_, coords_], opts___] := 
	Module[{mat, mat1, coords1, w, repl, phi},
	mat = RowReduce[mat0/.x_Real:>Rationalize[x]];
	Which[ 
		MissingDifferentialsCheck[ SpanK[mat, coords] ],
			Message[IntegrateOneForms::nonint]; phi= {},
		(*Else *)
			{mat1, coords1} = DeleteZeroColumns[mat, coords];
			repl = Thread[coords1 -> Array[Subscript[w, #]&, Length@coords1 ]];
			phi = IntegrateOneFormsBasic[ mat1/.repl, coords1/.repl, opts];
		Head[phi] =!= IntegrateOneFormsBasic,
			phi = phi /. (Reverse /@ repl);
			If[ !FreeQ[ mat0, _Real], 
				phi = N[phi] /. Thread[N[coords] -> coords]			];
		];
	Simplify[phi] /; Head[phi]=!=IntegrateOneFormsBasic
]


epsilon=10^(-10);

DeleteZeroColumns[ mat_,coords_] := Module[{posZero},
	posZero = Position[ Transpose[mat], {__?(#===0&)}, {1}];
	{Transpose[ Delete[Transpose[mat], posZero]],
		Delete[coords, posZero]
	}
]

Clear[MakeAndSolvePDE];
MakeAndSolvePDE[coefs0_, vars0_, order_] := Module[{vars, coefs, pde, phi},
	If[ order === 1, (* Old, normal order *)
		vars = vars0;
		coefs = coefs0,
	(* Else: Reversed order - good for some sin, cos examples? *)
		vars = RotateLeft[vars0];
		coefs = RotateLeft[coefs0];
	];
	pde = Dot[ coefs, D[g@@vars, #]& /@ vars ] == 0;
	phi = DSolve[ pde, g@@vars, vars ] /.y[i1_, j1_] :> Rationalize[ y[i1, j1] ];
	If[ Head[phi] === DSolve,
		Message[IntegrateOneForms::unable, pde]; phi,
		(*Else*)
		phi = Simplify[ g@@vars /. phi[[1]] /. 
			C[1][a__][b__] -> {a, b} /. 
			C[1][b__] -> {b} ]
	]
]

TransformMatrixToNewVariables[B_, XiB_, XiI_, phi_, YiI_] := Module[{B1, sol},
	B1 = Chop[ Simplify[ D[ phi, {XiB} ] + D[phi, {XiI} ].B ]];
	sol = First@Solve[ YiI == phi, XiI];
	Chop[Simplify[ B1 /. sol]]
]


Clear[IntegrateOneFormsBasic]

IntegrateOneFormsBasic[ mat_?MatrixQ, Xi_?VectorQ,___]:= 
	With[{n = Length@mat},
		Xi /; mat === IdentityMatrix[n] && Length@Xi===n
]
(* Assumptions:
1) mat0 has no zero columns, 
2) no floating point numbers, 
3) Xi are nice varibles, accepted by DSolve (Symbols)
*)
IntegrateOneFormsBasic[ mat0_?MatrixQ, Xi_?VectorQ, opts___] :=
Module[{ met, prn, B, mat, posI, XiI, XiB, coefs, repl, i, vars, phi, YiI, status },
	met = Method /. {opts} /. Options[ IntegrateOneForms ];
	prn = PrintInfo /. {opts} /. Options[ IntegrateOneForms ];
	mat = Simplify[ RowReduce[ mat0 ]];

	posI = First@Position[#, 1, {1}, 1]& /@ mat; (*pos of columns in the form (0 ... 0 1 0 ... 0) *)
	XiI = Part[ Xi, Flatten[posI] ]; (*coordinates, corresponding to columns in fhe form (0 ... 0 1 0 ... 0) *)
	XiB = Delete[ Xi, posI]; (*coordinates, correseponding to the remaining columns, i.e. columns of mat B*)
	B = -Transpose[ Delete[ Transpose@mat, posI ]]; (*the 'other' columns*)

	repl = {};
	i = 0; 
	status = "Solved"; 
	While[ i++;                                                   If[prn, Print["Step i = ", i]];
		coefs = Join[ {1}, B[[All, 1]] ];
		vars = Join[ {First@XiB}, XiI ];                          If[prn, Print["Eliminating varaible ", First@XiB]];
		phi = MakeAndSolvePDE[coefs, vars, met];                  If[prn, Print["phi = ", phi]];
		If[ Head[phi] === DSolve, 
			status = "Unable"; Break[]  (*DSolve is not able to Solve PDE*)
		];
	Length@XiB > 1, 
	
		YiI = Array[ y[i, #]&, Length@XiI ];
		repl = Thread[ YiI -> phi ] /. repl;                       If[prn, Print[repl]];
		B = TransformMatrixToNewVariables[B, XiB, XiI, phi, YiI];
		If[ !FreeQ[ B, First@XiB, -1 ], 
			Message[IntegrateOneForms::nonint]; status = "Unsolvable"; Break[] (*solution does not exist*)
		];
		B = Rest /@ B;
		XiB = Rest[XiB];
		XiI = YiI
	]; (* End While *)
	If[ status === "Solved", 
		phi /. repl, {} 
	]/; status =!= "Unable"
] (* End Module *)

Options[IntegrateOneForms] = {Method -> 1, PrintInfo -> False}
(*Method \[Rule] 2 rotates the order of independent variables in PDE, yielding sometimes simpler solution.*)


IntegratingFactors[ SpanK[\[Omega]i_, coords_], xi_] := 
	Transpose[ LinearSolve[ Transpose[\[Omega]i], Transpose[ D[xi, {coords}]] ] ]

IntegratingFactors[ sp_SpanK] := IntegratingFactors[ sp, IntegrateOneForms[sp]]


IntegrateExactOneForms[SpanK[mat_, coords_]]:=
Module[{sp,w,fi,mu},
	(Function[{vec},
	sp = SpanK[{vec},coords];
	w = ToDe[sp][[1]];
	If[ ExteriorD[w, coords ]===0,
		fi = IntegrateOneForms[sp];
		mu = IntegratingFactors[sp,fi];
		fi/mu, $Failed]
	] /@ mat) // Flatten
]


(* ::Subsubsection:: *)
(*Bottom derived system*)


Clear[BottomDerivedSystem]; (*21 jan. 2016*)

BottomDerivedSystem[ sp:SpanK[_,coords_], opts___Rule] :=  
	SpanK[ BottomDerivedSystem[ ToDe[sp], coords, opts], coords]
BottomDerivedSystem[ sp:SpanK[_,coords_], coords1_List, opts___?OptionQ] := 
	SpanK[ BottomDerivedSystem[ ToDe[sp], coords1, opts], coords]


BottomDerivedSystem[ forms_, t_Symbol, type_, opts___?OptionQ] := BottomDerivedSystem[ forms, DetectVariables[forms,t,type], opts ]

BottomDerivedSystem[ wiAll_List, coords_List, opts___?OptionQ] := 
Module[ { j, k, prn, wi, wed, dwi, fi, Ai, d, str, frob },
prn = PrintInfo/. {opts} /. Options[ BottomDerivedSystem ];
j = 0;
FixedPoint[
	Function[ wi, 
		If[ wi === {}, {},
			wed = Wedge @@ wi;
			dwi = Wedge[ ExteriorD[#, coords], wed]& /@ wi;
			If[ prn, 
				Print["    j = ", j];
				k = Length@wi;
				Do[ Print["\[Omega]", i, " = ", BookForm[ wi[[i]]] ], {i, k}];
				str = StringJoin @@ Table["\[Wedge]" <> "\[Omega]" <> ToString[i], {i,k} ];
				frob = "d\[Omega]" <> ToString[#] <> str& /@ Range[k];
				Do[ Print[frob[[i]], " =  ", BookForm[ dwi[[i]]] ], {i, k}] 
			];
			j++; 
			fi = Simplify[ dwi /. De[a_,b_List] :> a*d @@ b];
			Ai = SimplifyBasis @ NullSpace[ {fi} ];
			If[ prn, Print["Aij = ",  Ai]];
			Ai = Select[ Ai, FreeQ[#,d]& ];
			If[ Ai=!={}, Expand[Ai.wi], {} ]
	]], (* End Function *)
wiAll, SameTest -> (Length@#1 == Length@#2 &) ]
]/;!FreeQ[wiAll,De]


Options[BottomDerivedSystem] = {PrintInfo->False};


IntegrateOneFormsIfPossible[ sp:SpanK[{}, coords_]] := SeparatedSpanK[
	ExactSpanK[{}, coords], SpanK[{}, coords]
]

IntegrateOneFormsIfPossible[ sp:SpanK[mat_, coords_]] := 
Module[{wi, bdsys, intPos, nonIntPos, spInt, spNonInt, fi},
	wi = ToDe[sp];
	coords = sp[[2]]; (*kontrolli disk juhtumi z[t-1] muutujatega.*)
	bdsys = BottomDerivedSystem[ wi, coords];
	intPos = Flatten[ Position[wi,#]& /@ bdsys];
	nonIntPos = Complement[ Range[ Length@wi ], intPos];
	spInt = mat[[#]]& /@ intPos;
	spNonInt = mat[[#]]& /@ nonIntPos;
	fi = IntegrateOneForms[ SpanK[spInt, coords]];
	SeparatedSpanK[ExactSpanK[fi,coords], SpanK[spNonInt, coords]]
]


(* ::Subsection:: *)
(*Operations with spans /spaces/sets*)


SpanK/:MatrixRank[sp_SpanK]:=MatrixRank[sp[[1]]]


(*Let varList = {a_1,a_2,a_3,...} Checks if a_i appears directly in mat or da_i appears in 1-forms (a column, respective to da_i is nonzero)
If neither a_i nor da_i appears, then ai is removed from coords and respective colmn in mat is deleted.*)
DeleteCoordinatesIfNotRequired[ SpanK[mat_,coords_], varList_] := 
Module[{pos, mat1,coords1},
	mat1 = mat;
	coords1 = coords;
	Function[ var,
		pos = Position[coords1, var][[1]];
		If[ FreeQ[mat1, var] && AllTrue[ Take[mat1, All, pos], #===0&, 2],
			coords1 = Delete[coords1, pos];
			mat1 = Delete[#, pos]& /@ mat1
		]
	] /@ varList;
	SpanK[mat1, coords1]
]

AddCoordinatesIfRequired[ sp:SpanK[{}, _], _] := sp
AddCoordinatesIfRequired[ SpanK[mat_, coords_], addCoords_] := With[{
	newCoords = Join[coords, Complement[addCoords,coords]] },
	SpanK[ PadRight[mat, {Length@mat, Length@newCoords}], newCoords]	
]




UpdateSpanKCoordinates[SpanK[mat_, coords_], newCoords_] := Module[{repl},
	repl = Append[Thread[coords->Transpose@mat], _->Array[0&,Length@mat]];
	SpanK[ Transpose[Replace[newCoords, repl,1]], newCoords]
]

UpdateSpanKCoordinates[SpanKVectorFields[mat_, coords_], newCoords_] := Module[{repl},
	repl = Append[Thread[coords->Transpose@mat], _->Array[0&,Length@mat]];
	SpanKVectorFields[ Transpose[Replace[newCoords, repl,1]], newCoords]
]

EqualizeSpanKDimensions[Asp:(SpanK|SpanKVectorFields)[Amat_,Acoords_], Bsp:(SpanK|SpanKVectorFields)[Bmat_,Bcoords_]]:=
Module[{coordsAll, Arepl, Brepl, AmatNew, BmatNew},
	coordsAll = Union@Join[Acoords,Bcoords];
	{UpdateSpanKCoordinates[Asp, coordsAll], 
	 UpdateSpanKCoordinates[Bsp, coordsAll]
	 }
]


SimplifyBasis[{ }] := {}

SimplifyBasis[ SpanK[ sp_, args__ ]] := SpanK[ SimplifyBasis[sp], args ]
SimplifyBasis[ SpanKVectorFields[ sp_, args__ ]] := SpanK[ SimplifyBasis[sp], args ]

SimplifyBasis[ mat_?MatrixQ ] := 
Module[ {spsimpl, spnotred, spred, fact, vec1, sp = mat /. (0.) -> 0 },
    spsimpl = Simplify[sp];
	spnotred = Chop[ spsimpl, epsilon];
	spred = DeleteCases[ Chop[Simplify[RowReduce[spsimpl]],epsilon], {___?(#===0&)}];
	sp = If[ Length[spred] == Length[spnotred] && LeafCount[spred] >= LeafCount[spnotred], spnotred, spred ];
	Function[ vec,
		vec1 = PowerExpand[ Expand[
			PolynomialLCM@@
				Denominator[ Together@vec ] * vec
		] ];
	 	Simplify[ 
	 		If[ fact = PolynomialGCD@@vec1; fact=!=0, 
	 			vec1/fact, vec1
	 		] (* End If *)
	 	]
	 ] /@ sp
] (* End Module *) /. (0.) -> 0


UnionSpace[ SpanK[ sp1_, coords_], SpanK[ sp2_, coords_]] := SpanK[ Join[sp1,sp2], coords]


IntersectionSpace[ ___, {}, ___] := {}

IntersectionSpace[ sp1_?MatrixQ, sp2_?MatrixQ ] := Module[{int},
	int = Take[ NullSpace[ Transpose[ Join[sp1,sp2]] ], All, Length[sp1] ];
	If[ int==={}, {}, int.sp1 ]
]/;Length[sp1[[1]]] === Length@sp2[[1]]

IntersectionSpace[ SpanK[sp1_, coords_], SpanK[sp2_, coords_] ] := 
	SpanK[ IntersectionSpace[ sp1, sp2 ], coords]
	
IntersectionSpace[ sp1_SpanK, sp2_SpanK] := IntersectionSpace @@ EqualizeSpanKDimensions[sp1,sp2];


(* Removes columns containing only zero entries.*)
iDeleteZeroCoordinates[SpanK[sp_, coords_]] := Module[{pos, sptransp, spnew, coordsnew},
	pos = Position[ sptransp = Transpose[sp], {0..}];
	spnew = Transpose[ Delete[ sptransp, pos]];
	coordsnew = Delete[coords, pos];
	SpanK[spnew, coordsnew]
]

(* Checks if two sets of vectors span the same subspace*)
SameSubspace[ sp1_SpanK, sp2_SpanK] := SameSubspace[ iDeleteZeroCoordinates[sp1], iDeleteZeroCoordinates[sp2]]
SameSubspace[ SpanK[ sp1_, coords_], SpanK[ sp2_, coords_] ] := SameSubspace[ sp1, sp2 ]
SameSubspace[ { }, { }] := True
SameSubspace[ mat1_?MatrixQ, mat2_?MatrixQ ] := Module[{sp1,sp2},
	sp1 = DeleteCases[ RowReduce[mat1], {__?(#===0&)}];
	sp2 = DeleteCases[ RowReduce[mat2], {__?(#===0&)}];
	If[ Length[sp1] != Length[sp2],
		False,
	(* Else *)
		And @@ (#===0& /@ Flatten[ Chop[ Simplify[ sp1-sp2 ] ] ])
	] (* End If *)
]


ComplementSpace[{}, {}] := {}
ComplementSpace[{}, subSp_?MatrixQ] := {}
ComplementSpace[allSp_?MatrixQ, {}] := allSp

ComplementSpace[allSp_?MatrixQ, subSp_?MatrixQ] := Module[{i, testSpace},
	i = MatrixRank[subSp]; 
	Fold[(
		testSpace = Join[ subSp, #1, {#2}]; (* #1 is a complement space we are searching for. *)
		If[ MatrixRank[testSpace] > i, i++; Join[#1, {#2}], #1 ]
		)& , 
		{},   (* Start set *)
		allSp (* list, where #2 is taken.*)
	] (* End Fold*)
]

ComplementSpace[spAll_SpanK, sp_SpanK]:= ComplementSpace @@ EqualizeSpanKDimensions[spAll,sp]

ComplementSpace[ SpanK[allSp_?MatrixQ, coords_], SpanK[subSp_?MatrixQ, coords_]] :=
	SpanK[ ComplementSpace[allSp, subSp], coords]


ComplementSpace[allfun_?VectorQ, startfun_?VectorQ, coords_] := 
	ComplementSpace[allfun, startfun, coords, Length[allfun]] (* dim compfun = dim allfun *)

ComplementSpace[allfun_?VectorQ, startfun_?VectorQ, coords_, p_Integer] :=
(* p shows dimension of startfun+compfun *)
Module[{i, j, compfun, startsp, testsp},
	compfun={};
	i = Length[startfun]; (* functions found *)
	j = 1; (*index of allfun*)
	startsp = Outer[D, startfun, coords];
	While[ i<p && j<=Length[allfun],
		testsp = Outer[D, {allfun[[j]]}, coords];
		If[ MatrixRank[ Join[startsp,testsp]] > i,
			i++;
			startsp = Join[startsp, testsp];
			compfun = Join[compfun, {allfun[[j]]}]
		]; (* End If *)
		j++
	]; (* End While *)
	compfun
]


(* ::Subsection:: *)
(*Sequences*)


(* ::Subsubsection:: *)
(*Basis, SequenceLengthTest, UseExtendedState*)


Basis[ StateSpace[ f_, Xt_, Ut_, t_, h_, Yt_, op_, opts___] ] :=
	SpanK[ Take[ 
		IdentityMatrix[ Length@Xt + Length@Ut ], Length@Xt ], 
		Join[ Xt, Ut ]
	]


SequenceLengthTest::errlen ="Warning. Number of basis vectors is `1`. Some vectors have lost.";
SequenceLengthTest[subsp_List, m_] := Module[{len, testi},
	len = Length[ First[#] ]& /@ subsp;
	testi = #<=m& /@ Drop[ len-RotateLeft[len], -1];
	If[!And @@ testi, Message[ SequenceLengthTest::errlen, len]]
]


UseExtendedState[Fun_, sys:(StateSpace|IO)[_, _, _, t_ ,___], pos_, negvars___?VectorQ, opts___Rule ] :=
Module[{ extEq, rules, z, v },
	extEq = ExtendedState[ sys, z, v ];
	rules = StateDefinitions /. Cases[extEq, _Rule];
	If[{negvars} === {}, 
		Fun[ extEq, pos, opts ],
		Fun[ extEq, pos, negvars /. Reverse /@ rules, opts ]
	]  /. ((#1 /. t->tau_) -> (#2 /. t->tau)& @@@ rules )  
]/; !ClassicStateQ[sys]


(* ::Subsubsection:: *)
(*Sequence Ik (disc)*)


SequenceI::arg = "Forward shift of the system is idnetity and sequence Ik can not be computed.";

SequenceI[ steq:StateSpace[ f_, Xt_, Ut_, t_, _, _, Shift,opts___], pos_ ] := 
Module[ {k, H1, Ik, Ikplus, Iknext},
	If[ pos =!= All && FreeQ[pos, _?Negative], k = Max@pos, k = 30 ];
	H1 = Basis[steq];
	Ik = FixedPointList[ 
		Function[{Ik},
			Ikplus = ForwardShift[Ik, steq];
			Iknext = IntersectionSpace[Ik, Ikplus];
			SimplifyBasis[Iknext]
		], (*End Function*)
		H1, (* Start set *)
		k - 1, (* Max repeatings *)
		SameTest -> (Length[#1[[1]]]  == Length[#2[[1]]] &)
	];
	SequenceLengthTest[Ik, Length[Ut]];
	Take[Ik, pos]
] /; FreeQ[f, t+_, -1 ] 

SequenceI[syst_StateSpace, pos_ ] := UseExtendedState[ SequenceI, syst, pos ]/;!ClassicStateQ[syst]
SequenceI[syst_IO, pos_ ]         := UseExtendedState[ SequenceI, syst, pos ]

SequenceI[ (StateSpace|IO)[__, TimeDerivative], pos_ ] := err /; Message[ SequenceI::arg ]


(* ::Subsubsection:: *)
(*Sequence Hk*)


NextHk[etaMyMixed_, eta_, steq:StateSpace[f_, Xt_, Ut_, ___], bwrules_] := 
Module[{coords, my, etaPlus, newCoords, etaPlusNewBasisT, aiplus, Hknext},
	coords = Join[Xt, Ut];
	my = ComplementSpace[ etaMyMixed, eta ];
	etaPlus = Simplify[ PseudoLinearMap[ SpanK[eta, coords], steq] ][[1]];
	etaPlus = Take[etaPlus, All, Length@coords]; (*new extra coordinates are not necessary here.*)
	etaPlusNewBasisT = LinearSolve[ 
		Transpose[ Join[eta, my]], Transpose[etaPlus] ](* /.
		x_Real :> Rationalize[x]*);
	If[ Head[ etaPlusNewBasisT ] =!= LinearSolve,
		aiplus = NullSpace[ Take[etaPlusNewBasisT, -Length[my]] ];
		If[ aiplus =!= {},
			Hknext = SimplifyBasis[ Dot[ BackwardShift[aiplus, steq, bwrules], eta] ],
			Hknext = {}
		],
		Hknext = {}
	]
]

iSequenceHLinearSolve[ steq:StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, op_, ___Rule], k_Integer, bwspec___List, opts___Rule] := 
Module[{bwrules, prn, m, n, Imat, coords, Hk, i, eta, etaAndMy, Hknext, vars},
	bwrules = BackwardShiftOperator[steq, bwspec];
	prn = PrintInfo /. {opts} /. Options[ SequenceH ];
	n = Length@Xt;
	m = Length@Ut;
	coords = Join[Xt, Ut];
	Imat = IdentityMatrix[n+m];
	Hk = {Imat, Take[Imat, n]};
	i = 1;
	While[ i++;
		{etaAndMy, eta} = Hk[[{-2, -1}]];
		k>1 && i <= k && eta =!= {} && Length[ eta ] < Length[etaAndMy], (* Condition *)
		Hknext = NextHk[etaAndMy, eta, steq, bwrules];
		AppendTo[ Hk, Hknext];
		If[ TrueQ[prn], Print["H", i, " found with ", Length[Hknext], " elements"]];
	];
	Hk = Rest[Hk];
	Function[ mat,
		vars = DetectVariables[mat, t, op]; (*Search for possible variables \[Xi][t-1], [t-2], ... *)
		AddCoordinatesIfRequired[ SpanK[mat, coords], vars]
	] /@ Hk
]



iSequenceHIntersectionSpace[ steq:StateSpace[f_, Xt_, Ut_, t_, _,_, Shift,___Rule ], k_Integer, bwspec___List, opts___Rule] := 
Module[{bwop, prn, i, H1, Hk, Hkplus, int, Hknext},
	bwop = BackwardShiftOperator[steq, bwspec];
	prn = PrintInfo /. {opts} /. Options[ SequenceH ];
	H1 = Basis[steq];
	i = 1;
	Hk = FixedPointList[
		Function[ {Hk}, 
			i++;
			Hkplus = PseudoLinearMap[Hk, steq];
			int = IntersectionSpace[ Hk, Hkplus ];
			Hknext = BackwardShift[int, steq, bwop];
			If[ TrueQ[prn], Print["H", i, " found with ", Length[First[Hknext]], " elements"]];
			SimplifyBasis[Hknext]
		], (* End Function *)
		H1,     (* Start set *)
		k - 1,  (* max repeatings *)
		SameTest -> (Length[#1[[1]]]  == Length[#2[[1]]] &)
	] (*End FixedPointList*)
]
iSequenceHIntersectionSpace[ steq:StateSpace[__, TimeDerivative, ___Rule ], __] := err/;Message[SequenceH::met]


iSequenceHSequenceDk[ steq:StateSpace[f_, Xt_, Ut_, t_, _, _, Shift,___Rule], k_Integer, bwspec___List, opts___Rule] := 
Module[{Dk, Hk, coords },
		coords = Join[Xt,Ut]; 
		Dk = SequenceD[steq, k, bwspec]; 
		If[ k=!= All && Length[Dk]<k, Message[SequenceH::invol] ]; (* Only first nonintegrable subspace can be found! *)
		(* Problem!! If k == All, then the waring doesnt work correctly. *)
		Hk = SimplifyBasis[ NullSpace[First@#] ]& /@ Dk;
		SpanK[#, coords]& /@ Hk
]

iSequenceHSequenceDk[ steq:StateSpace[__, TimeDerivative,___Rule ], __] := err/;Message[SequenceH::met]



SequenceH::met = "This method is not avaliable for this type of system.";
SequenceH::invol = "All subspaces can not be found with this method, because corrresponding 
subspace(s) of vector fields is/are not ivolutive.";

(* Systems with distourbances *)
SequenceH[StateSpace[f_, Xt_, Ut:{_List, _List}, args__], n_, bws___]:=
	SequenceH[StateSpace[f, Xt, Flatten[Ut], args], n, bws]


SequenceH[ steq:StateSpace[f_, Xt_, Ut_, t_, __], pos_, bwspec___List, opts___Rule ] := 
Module[{met, k, m, Hk, len, testi, badinput, names},
	met = Method /. {opts} /. Options[ SequenceH ];
	k = If[ pos =!= All && FreeQ[pos, _?Negative], Max@pos, 30 ];
	m = Length@Ut;
	Hk = Switch[ met,
		1, iSequenceHLinearSolve[steq, k, bwspec, opts],
		2, iSequenceHIntersectionSpace[steq, k, bwspec, opts],
		3, iSequenceHSequenceDk[ steq, k, bwspec, opts],
		_, badinput
	]; (* End Switch *)
	SequenceLengthTest[Hk, m];
	Hk
]/;ClassicStateQ[steq]


(* Systems with distourbances *)
SequenceH[StateSpace[f_, Xt_, Ut:{_List, _List}, args__], n_, bws___]:=
	SequenceH[StateSpace[f, Xt, Flatten[Ut], args], n, bws]


(* Don't Remove!! It is neccessary for WebMath pages! It must be before UseExtendedState.*)
SequenceH[ ioeq:IO[{__Equal}, __], args__] := SequenceH[ ModelToRules[ioeq], args ]


SequenceH[sys_StateSpace, args__ ] := UseExtendedState[ SequenceH, sys, args ]/;!ClassicStateQ[sys]
SequenceH[sys_IO, args__ ]         := UseExtendedState[ SequenceH, sys, args ]


Options[SequenceH] = {Method -> 1, PrintInfo -> False};


(* ::Subsubsection:: *)
(*Vector fields*)


Clear[LieBrackets];

LieBrackets[ U_?VectorQ, V_?VectorQ, coords_ ] := Simplify[D[ V, {coords}].U - D[ U, {coords}].V]
LieBrackets[ f_?VectorQ, h_, coords_?VectorQ] := D[h,{coords}].f
LieBrackets[ U_, V_, W__, coords_] := LieBrackets[ LieBrackets[U, V, coords], W, coords]

LieD[f_, h_, Xt_List] := Simplify@Dot[ D[h,{Xt}], f]
LieD[f_, h_, Xt_List, n_Integer] := Nest[ LieD[f, #, Xt]&, h, n]


(*Ad[f_, g_, coords_, n_] := Nest[ Simplify[ LieBrackets[f, #1, coords]]&, g, n] *)


Involutivity[ SpanKVectorFields[mat_, coords_]] := Involutivity[ mat, coords]  /; FreeQ[mat, t+_?Negative ]
Involutivity[ SpanKVectorFields[mat_, coords_]] := False  /; !FreeQ[mat, t+_?Negative ] (* Warning! Mathematically uncorrect! *)

Involutivity[ mat_, coords_ ] := Module[{k, liebr},
	k = Length[ mat ];
	liebr = Flatten[ Table[ LieBrackets[ mat[[i]], mat[[j]], coords], 
		{i, 1, k}, {j, i+1, k} ], 1];
	MatrixRank[ Join[mat, liebr]] == MatrixRank[ mat ]
]


InvolutiveClousure[ SpanKVectorFields[sp_, coords_]] := SpanKVectorFields[ InvolutiveClousure[sp, coords]]
InvolutiveClousure[ sp_, coords_] := Module[{ mat, l, k, liebr, newmat},
mat = sp; k = Length@sp;
While[ l=k;
	liebr = Flatten[ Table[ LieBrackets[ mat[[i]], mat[[j]], coords], 
		{i,1,k}, {j,i+1,k}], 1];
	liebr = DeleteCases[ liebr, {___?(#===0&)}];
	Map[(newmat = Join[ mat, {#}];
		If[ MatrixRank[newmat]>k, mat=newmat; k++])&,
	liebr ];
	k > l
]; mat
]



(* ::Subsubsection:: *)
(*Sequence Sk (cont)*)


SequenceS[ StateSpace[f_, Xt_, Ut_, t_, ___, TimeDerivative], num_] := 
Module[{coords, n, m, S1, Si, lb, Skicl, g},
	coords = Join[Xt,Ut];
	n = Length@Xt;
	m = Length@Ut;
	g = Join[f, D[#,t]& /@ Ut];
	S1 = Table[ KroneckerDelta[n+i,j], {i,m}, {j,m+n}];
	Si = FixedPointList[ 
		Function[ Sk,
			Skicl = InvolutiveClousure[Sk, coords];
			lb = LieBrackets[g, #, coords]& /@ Skicl;
			SimplifyBasis[ Join[Skicl, lb]]
		], (* End Function *)
	S1, If[ num===All, 30, num-1] ]; (* End FixedPointList *)
	SpanKVectorFields[#, coords]& /@ Si 
] /; FreeQ[f, Derivative[_][_][t], -1]


SequenceS[syst_StateSpace, args__ ] := UseExtendedState[ SequenceS, syst, args ]/;!ClassicStateQ[syst]
SequenceS[syst_IO, args__ ]         := UseExtendedState[ SequenceS, syst, args ]


(* ::Subsubsection:: *)
(*Sequence Dk (disc)*)


SequenceD[ steq:StateSpace[f_, Xt_, Ut_, t_, _,_, Shift], pos_, bwspec___?VectorQ, opts___Rule ] := 
Module[{bwrules, coords, m, n, D0, Tf, Di, Deltanextplus},
	bwrules = BackwardShiftOperator[steq, bwspec];
	coords = Join[Xt,Ut];
	n = Length@Xt;
	m = Length@Ut;
	D0 = Table[ KroneckerDelta[n+i,j], {i,m}, {j,m+n}];
	Tf = Outer[D, f, coords];
	Di = NestWhileList[ 
		Function[ Dk,
			Deltanextplus = Dk.Transpose[Tf] /. t->t-1 /. bwrules;
			Deltanextplus = PadRight[#, m+n]& /@ Deltanextplus;
			Chop[ SimplifyBasis[ Join[ Deltanextplus, D0 ] ] ]
		], (* End Function *)
		D0, (* Start element *)
		(Length[#1] != Length[#2])&, (* test *)
		2, (*supplies 2 recent results as arguments for test *)
		If[ pos===All, 30, pos-1] (* max repeatings *)
	]; (* End NestWhileList *)
	SpanKVectorFields[#, coords]& /@ Di 
] /; FreeQ[f, t+_, -1]


SequenceD[syst_StateSpace, args__ ] := UseExtendedState[ SequenceD, syst, args ]/;!ClassicStateQ[syst]
SequenceD[syst_IO, args__ ]         := UseExtendedState[ SequenceD, syst, args ]

(* Testing: Simplify[ Outer[Dot, Hk[[2,1]], Dk[[2,1]], 1] ]*)


(* ::Subsubsection:: *)
(*Verification of sequences*)


VerifySequence[{Dk0_List, Hk0_List, Ik0_List}, stEq:StateSpace[f_,_,_,t_,_,_,Shift], opts___Rule]:=
Module[{prn, Dk, Hk, Ik, Inew, Hkplus, prod, res1, res2, res3, row1, row2, row3, ii, iim, eqsign},
	prn = PrintInfo /. {opts} /. Options[ VerifySequence ];
	Dk = Dk0;
	Hk = DeleteCases[Hk0, SpanK[{}, __]];
	Ik = DeleteCases[Ik0, SpanK[{}, __]];
(*1. condition<H_k,D_k> =0? ==============================================================*)
	res1 = Table[ prod=Simplify[Hk[[i,1]].Transpose[Dk[[i,1]]] ];
		And @@ (#===0& /@ Flatten[prod]),
		{i, Min[ Length[Dk],Length[Hk]] }
	];
(*2. condition: \Delta^(k-1)(H_k)=I_k?  ===============================================*)
	Inew = Table[ Nest[ SimplifyBasis[ ForwardShift[#,stEq]]&,Hk[[i]],i-1],
		{i,Length@Hk}];
	res2 = Thread[ SameSubspace[Ik,Inew] ];
(*3. condition H_k^+ \subspace H_ {k-1}? ====================================================*)
	Hkplus = SimplifyBasis[ ForwardShift[#,stEq]]& /@ Hk;
	res3 = Table[ SameSubspace[ Hkplus[[i]], IntersectionSpace[Hkplus[[i]],Hk[[i-1]]]],
		{i,2,Length[Hk]}];
(*Printing the results=====================================================================*)
	If[ TrueQ[prn],
	row1 = Table[ 
		ii = ToString[i]; 
		eqsign = If[res1[[i]],"=","\[NotEqual]"]; RowBox[
		{RowBox[{"\[LeftAngleBracket]",SubscriptBox["H", ii],",",SubscriptBox["D",ii],"\[RightAngleBracket]"}], eqsign, "0"}
	], {i, Length[res1]}];
	row1 = PadRight[row1, Length[Hk], "---"];
	row2 = Table[ 
		ii = ToString[i]; 
		iim = ToString[i-1]; 
		eqsign = If[res2[[i]],"=","\[NotEqual]"]; RowBox[ 
		{RowBox[{SuperscriptBox["\[CapitalDelta]",iim], "(", SubscriptBox["H",ii],")"}], eqsign, SubscriptBox["I", ii]}
	], {i, Length[res2]}];

	row3 = Table[ 
		ii = ToString[i]; 
		iim = ToString[i-1]; 
		eqsign = If[res3[[i-1]],"\[Subset]","\[NotSubset]"]; RowBox[
		{SubsuperscriptBox["H", ii, "+" ], eqsign, SubscriptBox["H", iim ]}
	], {i, 2, Length[res3]+1 }];
	row3 = Join[{"---"}, row3];
	Print[ GridBox[ Transpose[{row1,row2,row3}],
		ColumnSpacings->2, RowSpacings->1.7] // DisplayForm]
	]; (* End If prn*)
	{res1, res2, res3}
]

Options[VerifySequence] = {PrintInfo->True}


(* ::Subsection:: *)
(*GUI Functions*)


SequenceHGUI[sys0:(_StateSpace|_IO), n_, integration_?BooleanQ] := Module[ {sys, t, result, Hk, integr, comment},
	sys = ConvertToSubscripted[sys0];
	t = sys[[4]];
	Hk = SequenceH[sys, n];
	result = {"System equation(s)", sys, "Result: ", BookFormHolder[Hk, t]};
	If[integration,
		comment = "\\(\\mathcal{H}_i\\) = Integrable part + nonintegrable part: ";
		integr = IntegrateOneFormsIfPossible /@ Hk;
		result = Join[result,  {comment, BookFormHolder[integr,t]}];
	];
	result = GUIFormat[result, RowLabels->"\[ScriptCapitalH]"];
	result /. x_String :> StringReplace[x, "rclcc" -> "rclcl"]
] 


(* ::Subsection:: *)
(*End of package*)


On[General::spell1]
On[General::spell]

End[]

Protect[{}]

EndPackage[]
