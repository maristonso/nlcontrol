(* ::Package:: *)

(* :Title:  NLControl`Synthesis` *)
(* :Context: NLControl`Synthesis` *)
(* :Author: Maris Tonso, Juri Belikov *)
(* :Package Version: 2021 *)
(* :Mathematica Version: 11.3, 12 *)
(* :Institution: Department of Software Science at Tallinn University of Technology, Estonia *)

BeginPackage["NLControl`Synthesis`",{"NLControl`Core`", "NLControl`OneForms`", "NLControl`Ore`", "NLControl`Modelling`"}]


(* -------------------------------------------------------------------- *)
(* ========================== USAGE MESSAGES ========================== *)
(* -------------------------------------------------------------------- *)


(* ::Subsection:: *)
(*Usage messages*)


TransformabilityToNormalForm::usage = "tests if the system is transformable into 
normal form"

NormalForm::usage = "Transform the system into normal form."

WkBasisDifferential::usage = "Basis Wij in terms of differntials."
WkBasis::usage = "finds basis for the subspace Wk."
WkBarBasis::usage = "Basis WkBar for nonintegrable subspaces."
Linearizability::usage = "gives True, if the system is transformable into 
controller canonical form (linearizable by regular static state feedback and False 
otherwise."

Linearization::usage = "Transformes the system into controller canonical form 
(Linearizes the system by static state feedback)."

PartialLinearization::usage = "partial linearization"

EquivalenceToPrimeSystem::usage = "tests if the system is equivalent with the 
prime system"

PrimeSystem::usage = "finds prime form of the system"

FeedforwardCompensator::usage = "FeedforwardCompensator[F, G] finds a feedforward compensator to 
system F such that the compensated system coincides with the system G. Systems F and G may be given eihter 
by TransferFunction or by IO."

FeedbackCompensator::usage = "FeedbackCompensator[F, G] finds a feedback compensator to 
system F such that the compensated system coincides with the system G. Systems F and G may be given eihter 
by TransferFunction or by IO."


LinearizationGUI::usage = ""


ModelMatchingGUI::usage = ""


(* -------------------------------------------------------------------- *)
(* ============================= PROGRAMS ============================= *)
(* -------------------------------------------------------------------- *)


(* ::Subsection::Closed:: *)
(*Begin package*)


Begin["`Private`"]

Off[General::spell1]
Off[General::spell]
(*epsilon 10^-15;*)


(* ::Subsection::Closed:: *)
(*Additional functions & messages*)


PrintNonIntegrable[name_, n_]:= Module[{nstr, str},
nstr = ToString[n];
str = Switch[ name,
		SubspaceD, "The subspace D"<>nstr<>" is not involutive.", 
		SubspaceI, "The subspace I"<>nstr<>" is not integrable.", 
		SubspaceH, "The subspace H"<>nstr<>" is not integrable."
];
Print[str]
]

PrintNonAccessible[name_] := Print[ "The system is not accessible. The subspace ", 
	Subscript[ Switch[name, SubspaceH, "H", SubspaceI, "I" ], \[Infinity]], " \[NotEqual] \[EmptySet]."]


(* ::Subsection:: *)
(*Normal form*)


TransformabilityToNormalForm[ stEq:StateSpace[ f_, Xt_, Ut_, t_, h_, Yt_, type_, ___], opts___] :=
Module[{ m, n, p, frwXt, hXplus, dhXplus, H2, prn },
m = Length@Ut;
n = Length@Xt;
p = Length@Yt;
prn = PrintInfo /. {opts} /. Options[ TransformabilityToNormalForm ];
hXplus = NestWhileList[ PseudoLinearMap[#, stEq]&, #, FreeQ[#, Alternatives @@ Ut]& ]& /@ h;
Which[
	m!=p, If[ prn, Print["dim U \[NotEqual] dim Yt."]]; False,
	MatrixRank[ Transpose@ Outer[ D, Last /@ hXplus, Ut] ] < m, (* === cond *)
		If[ prn, Print["The system is not invertible."]]; False,
		(*======================================================= *)
		hXplus = Flatten[ Drop[#, -1]& /@ hXplus ];
		dhXplus = Outer[D, hXplus, Xt];
	MatrixRank[ dhXplus ] < Length[dhXplus], (* === cond *)
		If[ prn, Print[ "The output function is linearly dependent." ]]; False,
		(*======================================================= *)		
		H2 = Last @ SequenceH[ stEq, {2} ];
	Length@ First@ H2 =!= n-p,
		If[prn, Print[ "dim ", Subscript["\[ScriptCapitalH]",2], " < dim Y." ]]; False,
	!Integrability[H2],
		If[ prn, PrintNonIntegrable[ SubspaceH, 2]]; False,
	True, True
] (*End Which*)
]

Options[ TransformabilityToNormalForm ] = { PrintInfo -> False };


NormalForm[ stEq:StateSpace[ f_, Xt_, Ut_, t_, h_, Yt_, type_,___], stvar_ ] :=
Module[ {n, m, p, coords, hXplus, dhXplus, sumri, transf, z, sys1, H2, firstcoords, phi, res, repl1 },
	n = Length@ Xt;
	m = Length@ Ut;
	p = Length@ Yt;
	hXplus = NestWhileList[ PseudoLinearMap[#, stEq]&, #, FreeQ[#, Alternatives @@ Ut]& ]& /@ h;
	res = Which[ 
	m != p, badinput,
	MatrixRank[ Transpose@Outer[ D, Last /@ hXplus, Ut] ] < m, (* === cond 1 *) badinput,
		hXplus = Flatten[ Drop[#, -1]& /@ hXplus ];
		sumri = Length @ hXplus;
		dhXplus = Outer[D, hXplus, Xt];
	MatrixRank[ dhXplus ] < Length[dhXplus], (* === cond  2*) badinput,
		transf = Join[ hXplus, ComplementSpace[Xt, hXplus, Join[Xt, Ut] ]];
		{sys1, repl1} = ChangeOfVariables[ stEq, z[#][t]&, transf ];
		H2 = Last @ SequenceH[ sys1, {2} ];
	Length[First@ H2] =!= n - p, (* ==== cond 3*)badinput,
		phi = IntegrateOneForms@ H2;
	phi === {}, {},
	
	Head[phi] === IntegrateOneForms, badinput,
	
	True,
		coords = H2[[2]];
		firstcoords = Take[ coords, sumri];
		transf = Join[ firstcoords, ComplementSpace[phi, firstcoords, coords, n]];
		ChangeOfVariables[ sys1, stvar, transf ] /. repl1
	] (* End Which *); 
res /; res =!= badinput ]



(* ::Subsection:: *)
(*Linearization*)


SpanK[{{}},args___]:=SpanK[{},args];

WkBasisDifferential[steq:StateSpace[ f_, Xt_, Ut_, t_, _,_, op_,___]]:=
Module[{Hk, coords, wwPlus, wnew},
	Hk = SequenceH[steq, All];
	coords = Join[Xt,Ut];
	Rest@ Fold[ Function[ {w, Hm},
			wwPlus = Join[#, {Simplify[ First @ SimplifyBasis @ 
			PseudoLinearMap[ SpanK[Last@#,coords, t, op], steq]]}
		]& /@ w;
		wnew = ComplementSpace[Hm[[1]],  DeleteCases[Flatten[wwPlus,2],{}]];
		Join[wwPlus, {{wnew}}]
	], (* End Function *)
	{{{{}}}}, Reverse[Drop[ Hk,-2] ] 
	](* End Fold *)
](* End Module *)


WkBasis[ Hk_List, stEq:StateSpace[ f_, Xt_, Ut_, t_, __] ] :=
Module[ {coords, res, phi, wwPlus, wnew},
	coords = Join[Xt, Ut];
	res = Which[
	Hk[[-1,1]] =!={}, 
		badinput,
	True,
		Catch@ Rest@ Fold[ Function[ {w, Hm},
			phi = IntegrateOneForms[Hm];
			If[ MatchQ[ phi, {} |_IntegrateOneForms ], Throw[phi] ];
			wwPlus = Join[#, {Simplify[ PseudoLinearMap[ Last[#], stEq] ] }]& /@ w;
			wnew = ComplementSpace[phi, Flatten[wwPlus], coords, Length[phi]];
			Join[wwPlus, {{wnew}}]
			], (* End Function *)
		{{{}}}, Reverse[Drop[ Hk,-1] ] 
		] (* End Fold *)
	]; (* End Which *)
res/;res=!=badinput
] (* End Module *)

WkBasis[stEq_StateSpace] := WkBasis[SequenceH[ stEq, All], stEq]


Linearizability[ stEq_StateSpace, opts___] :=
Module[ {Hk, badsp, prn, met, Dn, res},
	prn = PrintInfo /. {opts} /. Options[ Linearizability ];
	met = Method /. {opts} /. Options[ Linearizability ];
	res = Which[
	met === SubspaceH,
		Hk = SequenceH[ stEq, All ];
		And @@ {
			If[ Hk[[-1, 1]] =!= {}, 
				If[ prn, PrintNonAccessible[met] ];
			False, True ],
				badsp = Flatten@ Position[ Integrability /@ Hk, False];
				If[ prn, Scan[ PrintNonIntegrable[met, # ]&, badsp ]];
			badsp === {}
		},
	met === SubspaceD && Last[stEq] === Shift,
		Dn = SequenceD[ stEq, All ];
		If[ Dn[[1]] === {}, True, If[prn, PrintNonIntegrable[met, Length@ Dn]]; False ], (* Perhaps incorrect? *)
	met === SubspaceD && Last[stEq] === TimeDerivative,
		Print["Sorry, not programmed yet."]; badinput
	]; (* End Which *)
	res /; res =!= badinput
]

Options[ Linearizability ] = {PrintInfo -> False, Method -> SubspaceH};


Linearization::lnzbility = "The system is not linearizable.";
Linearization[ stEq:StateSpace[ f_, Xt_, Ut_, t_, h_, Yt_, type_,___], stvar_, invar_, opts___ ] :=
Module[ { met, Vt, Zt, Hk, \[Delta]k, res, transf, lineq, lineqOpts, defs, pos, g, gNew, newDefs},
met = Method /. {opts} /. Options[ Linearization ];
If[met === Automatic, met = SubspaceH];
res = Which[ 
	Vt = NewVariables[ invar, Length@ Ut, t ];
Vt === badinput, $Failed,
	transf = Which[ (* Select method *)
		met === OperatorM && Length@Ut === 1,
			\[Delta]k = SequenceDelta[stEq]; 
			If[ \[Delta]k[[-1]] === Const && Length[\[Delta]k] == Length[Xt]+1,
				NestList[ First@OperatorCapitalM[#,stEq]&, {\[Delta]k[[-3,1]]}, Length[Xt]-1],
				{} (* lin sys does not exist *)
			], (* End If *)
		True, (* Automatic method subspaceH*)
			Hk = SequenceH[ stEq, All, Method->1];
			If[ Hk[[-1,1]] =!= {}, {}, (* lin sys does not exist *)
				WkBasis[Hk, stEq]
			] (* End If *)
	];
transf === {}, Message[ Linearization::lnzbility ]; {}, (* lin sys does not exist *)
Head[transf] === IntegrateOneForms, $Failed,  (* mtca technical disability *)
True,
	lineq = ChangeOfVariables[ stEq, stvar, Flatten[transf] ];
	lineqOpts = Cases[List@@lineq, _Rule];
	defs = StateDefinitions /. lineqOpts; 
	g = lineq[[1]];
	Zt = lineq[[2]];
	pos = Position[g, _?(FreeQ[Zt,#]&), {1}, Heads->False];
	gNew = ReplacePart[g, Thread[ pos->Vt ]];
	newDefs = Join[ defs, Thread[ Vt->g[[Flatten@pos]]] ];

	StateSpace[gNew, Zt,Vt, t, lineq[[5]], lineq[[6]], type, StateDefinitions->newDefs]
]; res /; res =!= $Failed
] (* End Module *)

Options[Linearization] = {Method -> Automatic}


Clear[WkBarBasis,PartialLinearization]
WkBarBasis[ Hk_List, stEq:StateSpace[ f_, Xt_, Ut_, t_, _,_, op_, ___] ] :=
Module[ {res, coords, HBar, q, WBar, intWBar, WBarPrev, intWBarPrev, WkHat, failed},
res = Which[ Hk[[-1,1]] =!= {}, badinput, 
	coords = Join[Xt, Ut];
	HBar = BottomDerivedSystem /@ Hk;
	(*q = Length[Hk] - 2;*)
	q = Position[HBar, SpanK[{},__]][[1,1]]-1; (*yields better coordinates if HBar[inf-1]={}. *)
	WBar = HBar[[q,1]];(*Integrable part; simply unstructured matrix; contains also wBarPlus, wBarPlusPlus,....*)
	WkHat = Complement[ Hk[[q,1]], WBar ];(* Nonintegrable part; only last WkHat is stored here*)
	intWBar = {{IntegrateOneForms[ SpanK[ WBar, coords, t, op ]] }}; 
		(*structured coordinates, include neccessary empty lists etc.*)
	failed = Do[
		intWBar = Join[#, {PseudoLinearMap[Last[#],stEq]}]& /@ intWBar; (*sama mis wwPlus leidmine*)
		WBar = Join[ WBar, Outer[ D, Flatten[ Last /@ intWBar ], coords ]];
		WBarPrev = ComplementSpace[Join[ HBar[[k,1]], Hk[[k+1,1]] ], Join[WBar, WkHat]];
		WBar = Join[WBarPrev, WBar];
		intWBarPrev = IntegrateOneForms[WBarPrev, coords];
		If[ Head[intWBarPrev] === IntegrateOneForms, Return[unable]];
		intWBar = Join[intWBar, {{intWBarPrev}}];
		WkHat = ComplementSpace[ Hk[[k,1]], WBar],
	{k, q-1, 1, -1} ]; (* End Do*)
	failed === unable, badinput,
	True, intWBar
]; (* End Which *)
res /; res =!= badinput
] (* End Module *)

WkBarBasis[stEq_StateSpace] := WkBarBasis[SequenceH[ stEq, All], stEq]

PartialLinearization[ stEq:StateSpace[ f_, Xt_, Ut_, t_, ___], stvar_ ] :=
Module[ {transf,res},
transf = Flatten[ WkBarBasis[stEq]];
res = Which[
	Head[transf] === WkBarBasis, badinput,
	True,
	transf = Join[transf, ComplementSpace[Xt, transf, Join[Xt,Ut]] ];
	ChangeOfVariables[ stEq, stvar, transf ]
]; (* End Which*)
res /; res =!= badinput
] (*End Module*)


(* ::Subsection:: *)
(*Prime system*)


EquivalenceToPrimeSystem[ stEq:StateSpace[ f_, Xt_, Ut_, t_, h_, Yt_, type_, ___ ], opts___] :=
Module[ {Hk, Hklastremoved, coords, F0, F0intHk, same, Hkplus, badsp, prn },
	Hk = Drop[ SequenceH[ stEq, All ], -1];
	prn = PrintInfo /. {opts} /. Options[ EquivalenceToPrimeSystem ];
And@@ {
	If[ Hk[[-1, 1]] === {}, True, If[prn, PrintNonAccessible[SubspaceH]];False ],
		Hklastremoved = Drop[Hk, -1];
		badsp = Flatten@ Position[ Integrability /@ Hklastremoved, False];
		If[ prn, Scan[ PrintNonIntegrable[ SubspaceH, #]&, badsp ]];
	badsp === {},
		coords = Join[Xt, Ut];
		F0 = SpanK[ Outer[D, h, coords], coords, t, type ];
		F0intHk = IntersectionSpace[F0, #]& /@ Hklastremoved;
		Hkplus = PseudoLinearMap[#, stEq]& /@ Rest[Hk];
		same = MapThread[ SameSubspace[#1, UnionSpace[#2, #3]]&, 
			{Hklastremoved, F0intHk, Hkplus}];
		badsp = Flatten@ Position[ same, False];
		If[ prn, Scan[
			Print[ "Holds inequality  ", Subscript["\[ScriptCapitalH]", #], " \[NotEqual] ", 
			Subscript["\[ScriptCapitalH]", #], " \[Intersection] ", Subscript["\[ScriptCapitalF]", 0], " + ", Subscript["\[CapitalDelta]\[ScriptCapitalH]", #+1] ]&, (*End Print*)
 		badsp ]];
	badsp === {}, 
		badsp = Flatten@ Position[ Integrability /@ F0intHk, False];
		If[ prn, Scan[ 
			Print["The subspce ", Subscript["\[ScriptCapitalH]",#1]," \[Intersection] ",Subscript["\[ScriptCapitalF]",0], " is not integrable"]&,
		badsp ]];
	badsp === {}
} ]

Options[ EquivalenceToPrimeSystem ] = {PrintInfo -> False};


PrimeSystem[ stEq:StateSpace[ f_, Xt_, Ut_, t_, h_, Yt_, type_ , ___], stvar_, invar_, outvar_ ] := 
Module[ { res, Xtnew, Utnew, Ytnew, coords, Hk,
		F0, transfList, Wkplus, failed, fnew, F0intHk, phi, phicomp, 
		prime, len, posInput, posOutput, rules, repl },
	res = Which[
		Xtnew = NewVariables[stvar, Length@ Xt, t ];
	Xtnew === badinput, badinput,
		Utnew = NewVariables[ invar, Length@ Ut, t ];
	Utnew === badinput, badinput,
		Ytnew = NewVariables[ outvar, Length@ Yt, t ];
	Ytnew === badinput, badinput,
		coords = Join[Xt, Ut];
		Hk = Drop[ SequenceH[stEq, All ], -1];
	Hk[[-1, 1]] =!= {}, badinput,
		F0 = SpanK[ Outer[D, h, coords], coords, t, type ];
		transfList = {{{}}};
		Wkplus = {};
		failed = Do[ 
			fnew = Join[#, {Simplify[ PseudoLinearMap[Last[#] ,stEq] ]}]& /@ transfList;
			Wkplus = Join[ Wkplus, Outer[ D, Flatten[ Last /@ fnew ], coords ]];
			F0intHk = SimplifyBasis[ IntersectionSpace[ F0, Hk[[k]] ]];
			If[ !SameSubspace[ Hk[[k]], UnionSpace[ F0intHk, PseudoLinearMap[Hk[[k+1]], stEq ] ] ],
				Return[{}] ]; (*Kas siin peab olema pseudoLinearMap v\[OTilde]i forwardShift? *)
			phi = IntegrateOneForms[ F0intHk ];
			If[ MatchQ[ phi, {} |_IntegrateOneForms ], Return[phi] ];
			phicomp = ComplementSpace[ phi, Flatten[fnew], coords, Length @ Hk[[k, 1]] ];
			Wkplus = Join[ Wkplus, Outer[ D, phicomp, coords] ];
			transfList = Join[ fnew, {{ phicomp }} ],
		{k, Length@Hk-1, 1, -1 } 
	];(* End Do *)
	Head[ failed ] === IntegrateOneForms, badinput,
	failed === {}, {},
	1 === 1, (* All conditions are satisfied *)
		{prime, repl} = FullSimplify[ 
			ChangeOfVariables[ stEq, Xtnew, Flatten@ transfList ]];
		len = Length /@ (transfList //. {a___, {}, b___} -> {a, b} );
		posInput = Rest@ FoldList[Plus, 0, len ];
		posOutput = FoldList[Plus, 1, Drop[len, -1] ];
		rules = Join[
		Thread[ Ytnew -> Xtnew[[ posOutput ]] /. First@
			Solve[ prime[[5]] == Yt, Xtnew[[ posOutput ]] ] ],
		Thread[ Utnew -> prime[[1, posInput ]] ]
		];
		prime[[1, posInput]] = Utnew;
		StateSpace[ prime[[1]], Xtnew, Utnew, t, Xtnew[[posOutput]], Ytnew, type, StateDefinitions->Join[ repl, rules] ]
	]; (* End Which *) 
res /; res =!= badinput ]


(* ::Subsection:: *)
(*Model Macthing*)


(* Juri Belikov *)


FeedforwardCompensator[TransferFunction[z_, TFsys_, Ut_, Yt_, t_, type_], TransferFunction[z_, TFrm_, Vt_, Yt_, t_, type_]] := 
Block[{K, reg, TFreg},
	K = DefineOreRing[z, t, type];
	reg = OreCancel[OreMultiply[Power[TFsys, -1], TFrm, K], K];
	TFreg = TransferFunction[z, reg, Vt, Ut, t, type]
];

FeedforwardCompensator[sysIO:IO[eqs_, Ut_, Yt_, t_, type_], rmIO:IO[eqs2_, Vt_, Yt_, t_, type_]] := 
Block[{dreg, int, regIO, TFsys, TFreg, TFrm, z},
	TFsys = TransferFunction[z, sysIO];
	TFrm = TransferFunction[z, rmIO];
	TFreg = Flatten[FeedforwardCompensator[TFsys, TFrm][[2]]][[1]];

	If[TFreg =!= {},
		dreg = FromOrePToSpanK[{{TFreg[[1]]}}, {{TFreg[[2]]}}, Ut, Vt, t, type];
		int = Integrability[dreg];

		If[int,
			regIO = IntegrateOneForms[dreg][[1]];
			IO[{regIO == 0}, Vt, Ut, t, type],
			Message[FeedforwardCompensator::unable];
			Message[IntegrateOneForms::nonint]; {}
		]
	]
];

FeedforwardCompensator::unable = "FeedforwardCompensator is unable to construct feedforward compensator.";


FeedbackCompensator[TransferFunction[z_, TFsys_, Ut_, Yt_, t_, type_], TransferFunction[z_, TFrm_, Vt_, Yt_, t_, type_]] := 
Block[{gamma, K, pF, pG, pR, regIO, qF, qG, qRy},
	K = DefineOreRing[z, t, type];
	pF = Flatten[TFsys][[1, 1]];
	qF = -Flatten[TFsys][[1, 2]];
	pG = Flatten[TFrm][[1, 1]];
	qG = -Flatten[TFrm][[1, 2]];
	{gamma, qRy} = RightQuotientRemainder[pG, pF, K];
	pR = OreMultiply[gamma, qF, K];

	TransferFunction[z, {{OreR[pR, -qG], OreR[pR, qRy]}}, Join[Vt, Yt], Ut, t, type]
];

FeedbackCompensator[sysIO:IO[eqs_, Ut_, Yt_, t_, type_], rmIO:IO[eqs2_, Vt_, Yt_, t_, type_]] := 
Block[{dreg, regIO, TFregRv, TFregRy, TFsys, TFrm, pR, qRv, qRy, TFreg, z},
	TFsys = TransferFunction[z, sysIO];
	TFrm = TransferFunction[z, rmIO];
	TFreg = FeedbackCompensator[TFsys, TFrm];
	{{TFregRv,TFregRy}} = TFreg[[2]];
	pR = TFregRy[[1]];
	qRy = TFregRy[[2]];
	qRv = TFregRv[[2]];

	dreg = FromOrePToSpanK[{{-qRy}}, {{pR}}, {{qRv}}, Yt, Ut, Vt, t, type];
	regIO = IntegrateOneForms[dreg][[1]];
	IO[{regIO == 0}, Join[Vt, Yt], Ut, t, type]
];


(* ::Subsection:: *)
(*GUI*)


LinearizationGUI[steq0_StateSpace, z_,v_, opts___] := Module[{steq, lin, comment},
	steq = ConvertToSubscripted[steq0];
	lin = Linearization[steq, z, v];
	comment = If[ Head[lin] === StateSpace,
		"Linearized system equations: ",
		"The system is not linearizable by static state feedback. "
	];
	GUIFormat[{"System equations: ", steq, comment, lin}]
]


ModelMatchingGUI[ioeq0_IO, refeq0_IO, ffComp_, fbComp_] := Module[{ioeq, refeq, result, ffC, fbC, res1, res2},
	ioeq = ConvertToSubscripted[ioeq0];
	refeq = ConvertToSubscripted[refeq0];
	result = {"System equation(s)", ioeq, "Reference model:", refeq};
	If[ffComp, 
		ffC = FeedforwardCompensator[ioeq, refeq];
		res1 = If[ Head[ffC] === IO, 
			{"Feedforward compensator is:",ffC},
			{"The feedforward compensator does not exist/cannot be represented in the form of i/o equation(s).\n"}
		];
		result = Join[result, res1];
	];
	If[fbComp,
		fbC = FeedbackCompensator[ioeq, refeq];
		res2 = If[ Head[fbC] === IO, 
			{"Feedback compensator is:", fbC},
			{"The feedforward compensator exists but NLControl has failed to find it.\n"}
		];
		result = Join[result, res2];
	]; 
	GUIFormat[result]
]


(* ::Subsection::Closed:: *)
(*End*)


On[General::spell1]
On[General::spell]
End[]

Protect[{}
]

EndPackage[]
