(* ::Package:: *)

(* : Title : NLControl`Analysis` *)
(* : Context : NLControl`Analysis` *)
(* : Authors : Vadim Kaparin - Observability *)
(* :           Janek Tabun - Identifiability *)
(* :           Maris Tonso, - Accessibility *)
(* :Package Version: 2021 *)
(* :Mathematica Version: 11.3, 12 *)
(* :Institution: Department of Software Science at Tallinn University of Technology, Estonia *)


BeginPackage["NLControl`Analysis`",{"NLControl`Core`", "NLControl`OneForms`", "NLControl`Ore`"}]


(* ::Subsection:: *)
(*Usage Messages*)


Accessibility::usage = "tests if the system is accessible"

AccessibilityDecomposition::usage = "decomposes the ystem into accessible and 
nonaccessible part."



Identifiability::usage = "Gives True or False wether the I/O system is identifiable or
       not."

TransformToSeparable::usage = "When it's possible converts the I/O model to an
       separable mode"

IdentifiabilityLSq::usage = "Gives TRUE or FALSE wether the I/O system is identifiable or
       not. Uses Least-squre method created by A.E.Pearson."

KnownInitialConditions::usage = "Determines whether the system identifiability is found
       with known initial conditions or not. Options: True / False (Default)"

PartialDerivativesMatrix::usage = "Prints partial derivatives matrices to the screen or exports
       to the fail called PartialDerivativesMatrix.nb. Options: None/Print/Export"

AccessibilityGUI::usage =""

IdentifiabilityGUI::usage =""


(* ::Subsection::Closed:: *)
(*Begin package*)


(* -------------------------------------------------------------------- *)
(* ============================= PROGRAMS ============================= *)
(* -------------------------------------------------------------------- *)

Begin["`Private`"]

Off[General::spell1]
Off[General::spell]
epsilon = 10.^-15;


(* ::Subsection::Closed:: *)
(*Observability*)


(* See Observability.m *)


(* ::Subsection::Closed:: *)
(*Accessibility*)


iAccsTest[steq:StateSpace[f_, Xt_, Ut_, t_, ___, Shift]] := 
Module[ {n,bwop, Amat,Bmat,C1, bwA, CList, CJoin},
	n = Length[Xt];
	bwop = BackwardShiftOperator[steq];
	Amat = Outer[ D[#1,#2]&, f, Xt];
	Bmat = Outer[ D[#1,#2]&, f, Ut];
	C1 = Simplify[ BackwardShift[ Bmat, steq, bwop]];
	bwA = Simplify[ BackwardShift[Amat, steq, bwop]];
	CList = NestList[ Simplify[ bwA.BackwardShift[#,steq,bwop]]&, C1, n];
	CJoin = MapThread[ Join, CList];
	MatrixRank[CJoin] == n
]


Accessibility[ stEq_StateSpace, opts___Rule ] :=
Module[ {res, met, prn},
	met = Method /. {opts} /. Options[ Accessibility ];
	prn = PrintInfo /. {opts} /. Options[ Accessibility ];
	res = Switch[ met,
	SubspaceI, SequenceI[ stEq, -1 ][[-1,1]] === {},
	SubspaceH, SequenceH[ stEq, -1 ][[-1,1]] === {},
	OperatorM, SequenceDelta[stEq][[-1]] === Const,
	2,         iAccsTest[stEq],
	_,         badinput ]; 
If[ res === False && prn, PrintNonAccessible[met] ]; 
res /; res =!= badinput
] (* End Module *)

Options[ Accessibility ] = { Method -> SubspaceH, PrintInfo -> False };


AccessibilityDecomposition[ stEq:StateSpace[ _, Xt_, Ut_, ___ ], stvar_] :=
Module[ {n, Hk, phi, subx, i, vars, jacbn },
	n = Length[Xt];
	Hk = SequenceH[stEq, All];
	phi = IntegrateOneForms[Hk[[-1]]];
	subx = Subsets[Xt,{n-Length[phi]}];
	i=1;	
	While[ Length[phi] < n,
		vars = Join[ phi, {Xt[[i]]}];
		jacbn = Outer[D, vars, Join[Xt, Ut]];
	If[ MatrixRank[jacbn] > Length[phi], phi=vars ]; (* End If *)
	i++
  ]; (* End While *)
ChangeOfVariables[ stEq, stvar, vars]
] (* End Module *)


(* ::Subsection::Closed:: *)
(*Identifiability*)


Identifiability[ssttqq:StateSpace[_,_,_,t_,___], theta_, opts___] := IdentifiabilityAlgebraic[ssttqq,theta,opts]

Options[Identifiability]= {KnownInitialConditions-> False, PartialDerivativesMatrix->False}
(*{KnownInitialConditions-> None, PartialDerivativesMatrix->None}*)
KnownInitialConditions::bln = "Missing KnownInitialConditions, must be True or False"


(*================================= Pearson  ===================================*)

sepfun[ ssttqq:StateSpace[f_,Xt_,Ut_,t_,h_,Yt_, type_], theta_] :=
      Module[{spfun,sep,fi1,fi},
		fi=TrigExpand[FunctionExpand[PowerExpand[ExpandAll[#2-#1]&@@StateSpaceToIO[ssttqq][[1,1]]]]];
		Do[{If[j>1&&!spfun==="System is not separable",{fi=Collect[spfundot,Join[Cases[spfun[[2]],-_](-1),DeleteCases[spfun[[2]],-_]]]}];
			fi1= If[ Head[fi]=== Plus, List @@ fi, {fi}];
                  sep=Which[
                  	FreeQ[#, Alternatives @@ theta, -1], {1, #},
                        FreeQ[#, t, -1], {#, 1},
                        MatchQ[#, (p_/; FreeQ[p, t, -1])*
                        	(uy_/; FreeQ[uy, Alternatives @@ theta, -1])],
                              # /. (p_/; FreeQ[p, t, -1])*
                              (uy_/; FreeQ[uy, Alternatives @@ theta, -1]) -> {p, uy},
                              True, "notseparable"
                              ] & /@ fi1;
                     	spfun=If[! FreeQ[sep,"notseparable"], "System is not separable",
                              Transpose[sep]],
      	spfundot=Dot@@spfun},{j,1,2}];

               spfun
       ](*End Module*)

TransformToSeparable[ssttqq:StateSpace[f_,Xt_,Ut_,t_,h_,Yt_, type_], theta_] :=
Module[
       {sep = sepfun[ssttqq,theta]},
       If[sep === "System is not separable", "System is not separable",
               Dot@@sep]
](*End Module*)

Off[Solve::"incnst",Solve::"svars",Solve::"ifun",General::"stop",Nest::"intnm",
       Take::"normal"]

IdentifiabilityLSq[ssttqq:StateSpace[f_,Xt_,Ut_,t_,h_,Yt_],theta_]:=
Module[
       {idsep,gi,xi},
       gi=DeleteCases[Flatten[Take[sepfun[ssttqq, theta], {1, 1}]], 1];
       xi=Table[xx[i],{i,Length@theta}];
       idsep=If[! FreeQ[sepfun[ssttqq, theta], "System is not separable"],
                       "False, system is not separable",Sort[Flatten[Solve[gi==
                       (gi/.Thread[theta->xi]),theta]]]===Sort[Thread[theta->xi]]];
       idsep
](*End Module*)



(*=============================== Alcebraic Method =============================*)
(*=== Continues ===*)
IdentifiabilityAlgebraic[ StateSpace[f_,Xt_,Ut_,t_,h_,Yt_, TimeDerivative],theta_,opts___]:=
Module[
       {opt1,opt2,rankdYdXthetaKIC,dYdXtheta0KIC,identfContinues,identfKIC,identf,rankdYdXtheta,dYdXtheta,dYdXtheta0,rankdYdX,
               dYdX0,outputYeq,statevariableX1,statevariables0,differ,outputYeqM},
       opt1=KnownInitialConditions/.{opts}/.Options[Identifiability];
       opt2=PartialDerivativesMatrix/.{opts}/.Options[Identifiability];
       differ=Length@Xt+Length@theta-1;
       statevariables0=Flatten[NestList[D[#,t]&,Xt,differ]];
       outputYeqM=Do[{
       statevariableX1=D[h,{t,i}],
               outputYeq=Insert[If[i==1,{h},outputYeq],Flatten[ReplaceRepeated[statevariableX1,
                       Thread[Flatten[NestList[D[#,t]&,D[Xt,t],i-1]]->Flatten[NestList[D[#,t]&,f,i-1]]]]],i+1]},{i,1,differ}];
       If[opt1===False,{dYdX0=Transpose[DeleteCases[Table[Flatten[D[Flatten[outputYeq],statevariables0[[i]]]],{i,1,Length@statevariables0}],{__?(#===0&)}]];,
               rankdYdX=Length[DeleteCases[RowReduce[dYdX0],{__?(#===0&)}]];,
               dYdXtheta=Transpose[Join[Transpose[dYdX0],Partition[Flatten[D[Flatten[outputYeq],#]&/@theta],Length@Flatten[outputYeq]]]];,
               identf = rankdYdXtheta  == Length[theta]+rankdYdX;,
               rankdYdXtheta=Length[DeleteCases[RowReduce[dYdXtheta],{__?(#===0&)}]]}];
       If[opt1===True,{dYdXtheta0KIC=Transpose[Partition[Flatten[D[Take[Flatten[outputYeq],{Length@Xt+1,differ+1}],#]&/@theta],differ-Length@Xt+1]];,(*WithInitialConditions*)
               rankdYdXthetaKIC=Length[DeleteCases[RowReduce[dYdXtheta0KIC],{__?(#===0&)}]];,(*WithInitialConditions*)
               identfKIC=rankdYdXthetaKIC == Length[theta]}];(*WIthInitialConditions*)

       identfContinues=If[opt1===True,identfKIC,identf];

       If[opt1===False && (opt2===Print||opt2===True),{Print["\!\(\[PartialD]\((y,  ... , y\^(n))\)\/\[PartialD]\((x, \[Theta])\)\)="];,
               Print[TraditionalForm[FullSimplify[dYdXtheta]]];,
               Print[];,
               Print["\!\(\[PartialD]\((y,  ... , y\^(n))\)\/\[PartialD]\( x \)\)="];,
               Print[TraditionalForm[FullSimplify[dYdX0]]]}];
       If[opt1===False && opt2===Export,Put["\!\(\((y,  ... , y\^(n))\)\)=",outputYeq,"\!\(\[PartialD]\((y,  ... , y\^(n))\)\/\[PartialD]\((x, \[Theta])\)\)=",dYdXtheta,"\!\(\[PartialD]\((y,  ... , y\^(n))\)\/\[PartialD]\( x \)\)=",dYdX0,"PartialDerivativesMatrix.nb"]];

       If[opt1===True && (opt2===Print||opt2===True),{Print["\!\(\[PartialD]\((y\^(m), ...,y\^(n))\)\/\[PartialD]\( \[Theta]\)\)="];,
               Print[TraditionalForm[FullSimplify[dYdXtheta0KIC]]]}];
       If[opt1===True && opt2===Export,Put["\!\(\((y,  ... , y\^(n))\)\)=",outputYeq,"\!\(\[PartialD]\((y\^(m), ...,y\^(n))\)\/\[PartialD]\( \[Theta]\)\)=",dYdXtheta0KIC,"PartialDerivativesMatrix.nb"]];
       If[opt1===None, {Message[KnownInitialConditions::bln],Abort[ ]}];

       identfContinues

](*End Module*)

(*=== Discrete ===*)
IdentifiabilityAlgebraic[ StateSpace[f_,Xt_,Ut_,t_,h_,Yt_, Shift],theta_,opts___]:=
Module[
       {opt1,opt2,identfDiscrete,identfD,identfDKIC,rankdYdXthetaKICD,rankdYdXthetaD,dYdXtheta0KICD,
               dYdXthetaD,rankdYdXD,dYdX0D,outputYeqD,statevariables0D,differD},
       opt1=KnownInitialConditions/.{opts}/.Options[Identifiability];
       opt2=PartialDerivativesMatrix/.{opts}/.Options[Identifiability];
       differD=Length@Xt+Length@theta-1;
       statevariables0D=Flatten[NestList[#/.t->t+1&,Xt,differD]];
       outputYeqD=Flatten[ReplaceRepeated[Flatten[NestList[#/.t->t+1&,h,differD]],
               Thread[Flatten[NestList[#/.t->t+1&,Xt/.t->t+1,differD]]->Flatten[NestList[#/.t->t+1&,f,differD]]]]];
       If[opt1===False,{dYdX0D=Transpose[Table[D[Flatten[outputYeqD],statevariables0D[[i]]],{i,1,Length@statevariables0D}]];,
               rankdYdXD=Length[DeleteCases[RowReduce[dYdX0D],{__?(#===0&)}]];,
               dYdXthetaD=Transpose[Join[Transpose[dYdX0D],D[Flatten[outputYeqD],#]&/@theta]];,
               identfD=rankdYdXthetaD == Length[theta]+rankdYdXD;,
               rankdYdXthetaD=Length[DeleteCases[RowReduce[dYdXthetaD],{__?(#===0&)}]]}];
       If[opt1===True,{dYdXtheta0KICD=Transpose[D[Take[Flatten[outputYeqD],{Length@Xt+1,differD+1}],#]&/@theta];
               ,rankdYdXthetaKICD=Length[DeleteCases[RowReduce[dYdXtheta0KICD],{__?(#===0&)}]];
               ,identfDKIC=rankdYdXthetaKICD == Length[theta]}];(*WIthInitialConditions*)

       identfDiscrete=If[opt1===True,identfDKIC,identfD];

       If[opt1===False && (opt2===Print||opt2===True),{Print["\!\(\[PartialD]\((y(0),  ... , y(n))\)\/\[PartialD]\((x, \[Theta])\)\)="];
               ,Print[TraditionalForm[FullSimplify[dYdXthetaD]]];
               ,Print[];
               ,Print["\!\(\[PartialD]\((y(0),  ... , y(n))\)\/\[PartialD]\( x \)\)="];
               ,Print[TraditionalForm[FullSimplify[dYdX0D]]]}];
       If[opt1===False && opt2===Export,Put["\!\(\((y(0),  ... , y(n))\)\)=",outputYeqD,"\!\(\[PartialD]\((y(0),  ... , y(n))\)\/\[PartialD]\((x, \[Theta])\)\)=",dYdXthetaD,"\!\(\[PartialD]\((y(0),  ... , y(n))\)\/\[PartialD]\( x \)\)=",dYdX0D,"PartialDerivativesMatrix.nb"]];

       If[opt1===True && (opt2===Print||opt2===True),{Print["\!\(\[PartialD]\((y(m), ...,y(n))\)\/\[PartialD]\( \[Theta]\)\)="];
               ,Print[TraditionalForm[FullSimplify[dYdXtheta0KICD]]]}];
       If[opt1===True && opt2===Export,Put["\!\(\((y(0),  ... , y(n))\)\)=",outputYeqD,"\!\(\[PartialD]\((y(m), ...,y(n))\)\/\[PartialD]\( \[Theta]\)\)=",dYdXtheta0KICD,"PartialDerivativesMatrix.nb"]];
       If[opt1===None, {Message[KnownInitialConditions::bln],Abort[ ]}];

       identfDiscrete
](*End Module*)


(* ::Subsection:: *)
(*GUI*)


AccessibilityGUI[sys0_, opts___] := Module[{ sys, comment},
	sys = ConvertToSubscripted[sys0];
	comment = If[ Accessibility[sys],
		"The system is accessible.",
		"The system is not accessible."
	];
	GUIFormat[{"System equation(s)", sys, comment}]
]

IdentifiabilityGUI[sys_, opts___] := Module[{res},{}]




(* ::Subsection::Closed:: *)
(*End package*)


On[General::spell1]
On[General::spell]

End[]

Protect[{}
]

EndPackage[]

