(* ::Package:: *)

(* :Title:  NLControl`TimeScale` *)
(* :Context: NLControl`TimeScale` *)
(* :Author: Maris Tonso *)
(* :Package Version: 2011 *)
(* :Mathematica Version: 8.0 *)
(* :Copyright: Institute of Cybernetics at TUT, Tallinn, Estonia *)

BeginPackage["NLControl`TimeScale`", {"NLControl`Core`", "NLControl`Ore`"}]
(*Ore vajalik, sest def-me shift and derivative rules*)


(* ::Subsection:: *)
(*Usage messages*)


TimeScale::usage = "indicates the time-scale system."

DeltaDerivative::usage = "DeltaDerivative[n][x][t] represnets a n-th delta derivative of x(t). 
Analogical to Derivative[n][x][t]"

DeltaD::usage = "DeltaD[f,x] computes delta derivative of f with respect to x. Analogical to D[f,x]."

Jump::usage = " Represents a shift operator."

\[Mu]::usage = "Graininess function."

\[Sigma]::usage = "Represents a general shift operator."

\[CapitalDelta]::usage = "used to represent delta derivative."

FromTimeScaleToShift::usage = "Transforms expression/system from time scale to shift."


(* ::Subsection::Closed:: *)
(*Begin programs*)


Begin["`Private`"]

Off[General::spell1]
Off[General::spell]
epsilon = 10^-15;


(* ::Subsection:: *)
(*Pre-definitions for Ore polynomials*)


       ShiftAndDerivativeRules[t_, TimeScale] := {TimeScale, t->Jump[t,1], t->Jump[t, -1], DeltaD[#,t]&}
ShiftAndDerivativeAdjointRules[t_, TimeScale] := {Adjoint[TimeScale], t->Jump[t,-1], t->Jump[t,1], -DeltaD[#, t]&}


(* ::Subsection:: *)
(*Delta derivative and Shift operator*)


(* sigmadega *)
Jump /: MakeBoxes[ Jump[t_,n_], form_]:=
	With[{pwr = n*\[Sigma]},
	SuperscriptBox[ MakeBoxes[t, form], MakeBoxes[pwr, form]]
]
(* ylaindeksis plussid *)
(*Jump /: MakeBoxes[ Jump[t_,n_], StandardForm]:=
	With[{pwr = If[ n>0, StringJoin@@Array["+"&,n],
		StringJoin@@Array["-"&,-n]]},
	SuperscriptBox["\[NegativeThinSpace]" , pwr]
]*)

Jump[ Jump[t_, m_], n_] := Jump[t, m+n]
Jump[t_, 0] := t
Jump /: x_[Jump[t_Symbol, m_/;m>0]] := x[Jump[t,m-1]] + \[Mu] DeltaDerivative[1][x][Jump[t,m-1]]


(* ========================================================================================================== *)

(*DeltaDerivative /: MakeBoxes[ DeltaDerivative[1][x_],StandardForm] := SuperscriptBox[ MakeBoxes[x, StandardForm], "\[CapitalDelta]"]*)
DeltaDerivative /: MakeBoxes[ DeltaDerivative[n_][x_],form_] := 
	SuperscriptBox[ MakeBoxes[x, form], RowBox[ {"\[LeftAngleBracket]", ToString[n], "\[RightAngleBracket]" } ]]

DeltaDerivative[0][x_][t_] := x[t]
DeltaDerivative[n_][DeltaDerivative[m_][x_]][t_] := DeltaDerivative[n+m][x][t]

DeltaDerivative[m_][x_][Jump[t_,n_/;n<0]] := 
	(DeltaDerivative[m-1][x][Jump[t,n+1]]-DeltaDerivative[m-1][x][Jump[t,n]])/\[Mu]


DeltaD[c_, t_Symbol] := 0 /; FreeQ[c, t]
DeltaD[x_[t_], t_Symbol] := DeltaDerivative[1][x][t]

DeltaD[x_[Jump[t_, i_/;i<0]],t_Symbol] := (x[Jump[t,i+1]] - x[Jump[t,i]])/\[Mu] (* Bugfix 2013 *)
DeltaD[expr_, t_Symbol] := Simplify[((expr/.x_[Jt:(t|Jump[t, i_/;i<0])] :> x[Jt] + \[Mu] DeltaD[x[Jt],t]) - expr) / \[Mu]] (*Bugfix 2014 *)

DeltaD[f_List, t_Symbol] := DeltaD[#, t]& /@ f
DeltaD[x_[t_], {t_Symbol, n_}] := DeltaDerivative[n][x][t]   (* Important to DefineOreRing *)
DeltaD[expr_, {t_Symbol, n_Integer}] := Nest[ DeltaD[#, t]&, expr, n] (* Integer is important to DefineOreRing *)

(* Perhsps not so important option *)
DeltaD[expr_, StateSpace[f_, Xt_, Ut_, t_, _,_, TimeScale. ___ ]] := DeltaD[ expr, t] /. Thread[ DeltaD[Xt, t] -> f]


(* ::Subsection:: *)
(*System properties*)


IdentityShiftQ[ (StateSpace|IO)[__, TimeScale,___]] := False
ZeroDerivativeQ[ (StateSpace|IO)[__, TimeScale,___]] := False

ClassicStateQ[ StateSpace[f_, Xt_, Ut_, t_, _, _, TimeScale,___]] := FreeQ[f, DeltaDerivative[_][_][t]]

MaxPLMOrder[ expr_, x_[t_], TimeScale ] := 
	Max[ Cases[ {expr}, DeltaDerivative[_][x][t]|x[t], -1] /. {DeltaDerivative[p_][x][t] -> p, x[t] -> 0}]

DetectVariables[expr_, t_, TimeScale] := DeleteCases[Union[ Cases[ {expr}, DeltaDerivative[_][_][t+i_.]|x[Jump[t,_]]|x_[t], -1] ],{_},1]
RemoveAppliedOperator[expr_, t_, TimeScale] := expr/.DeltaDerivative[n_][x_][t]->x[t]

Submersivity[ steq:StateSpace[f_, Xt_, Ut_, t_, __, TimeScale,___] ] :=
	MatrixRank[ Outer[ D, ForwardShift[Xt, steq], Join[Xt,Ut]] ] == Length@f /; FreeQ[f, DeltaDerivative[_][_][t] ]

FromTimeScaleToShift[StateSpace[f_,Xt_, Ut_,t_,h_,Yt_,TimeScale,opts___]]:=
	StateSpace[ \[Mu]*f+Xt, Xt, Ut, t, h, Yt, Shift, opts]
FromTimeScaleToShift[ IO[eqs_, Ut_, Yt_, t_, TimeScale, opts___ ]]:=IO[FromTimeScaleToShift[Equal@@@eqs,t], Ut,Yt,t, Shift]
FromTimeScaleToShift[expr_,t_] := Simplify[expr//.DeltaDerivative[n_][x_][t+i_.]-> 
	(DeltaDerivative[n-1][x][t+i+1]-DeltaDerivative[n-1][x][t+i])/\[Mu]]




(* ::Subsection:: *)
(*ForwardShift, BackwarShift, PseudoD and PseudoLinearMap*)


ForwardShift[expr_, t_, TimeScale ] := expr /. t->Jump[t, 1] 
ForwardShift[expr_, StateSpace[f_, Xt_, Ut_, t_, _,_, TimeScale, ___ ]] := 
	expr /. t->Jump[t, 1] /. Thread[ DeltaD[Xt, t] -> f]
ForwardShift[ expr_, {t_, k_}, TimeScale ] := expr /. t -> Jump[t, k]


BackwardShift[expr_, steq:StateSpace[_, _, _, t_, _,_, TimeScale, ___ ], bwSpec___] := 
	expr/. t->Jump[t, -1] /. BackwardShiftOperator[steq, bwSpec]

BackwardShift[ expr_, {t_, k_}, TimeScale ] := expr /. t -> Jump[t, -k] (* for an arbitrary k *)
BackwardShift[ expr_, t_Symbol, TimeScale ] := expr /. t -> Jump[t, -1]


NegativeTimeShifts[ ioeq:IO[__, TimeScale] ] := NegativeTimeShifts[ ModelToRules[ FromTimeScaleToShift[ioeq] ] ]
NegativeTimeShifts[ steq:StateSpace[__, TimeScale, ___] ] := NegativeTimeShifts[ FromTimeScaleToShift[steq] ]

BackwardShiftOperator[ steq:StateSpace[ _, _, _, t_, _, _, TimeScale,___ ], bwspec___ ] := 
	BackwardShiftOperator[ FromTimeScaleToShift[steq], bwspec ] /. 
	(t-1) -> Jump[t, -1]

BackwardShiftOperator[ioeq:IO[_, _, _, t_, TimeScale]] := (*BackwardShift specificatoin bwSpec does not work here.*)
	BackwardShiftOperator[ ModelToRules[ FromTimeScaleToShift[ioeq] ]] /.
	t+i_ -> Jump[t, i]


PseudoD[expr_, StateSpace[f_, Xt_, Ut_, t_, _,_, TimeScale, ___ ]] := DeltaD[ expr, t] /. Thread[ DeltaD[Xt, t] -> f]

PseudoLinearMap[ f_, {t_Symbol, n_}, TimeScale ] := DeltaD[f, {t, n}]
PseudoLinearMap[ f_, t_, TimeScale ] := DeltaD[f, t]
PseudoLinearMap[expr_, steq:StateSpace[__, TimeScale, ___ ]] := PseudoD[expr, steq]



(* ::Subsection:: *)
(*Exterior differential*)


De[a_,{\[Mu]*x_}]:=De[\[Mu] a,{x}]


(* ::Subsection::Closed:: *)
(*End package*)


On[General::spell1]
On[General::spell]


End[]

Protect[{}
]

EndPackage[]


