(* ::Package:: *)

(* :Title:  NLControl`Modelling` *)
(* :Context: NLControl`Modelling` *)
(* :Authors: Maris Tonso, Juri Belikov*)
(* :Package Version: 2021 *)
(* :Mathematica Version: 11.3, 12 *)
(* :Institution: Department of Software Science at Tallinn University of Technology, Estonia *)

BeginPackage["NLControl`Modelling`", {"NLControl`Core`", "NLControl`OneForms`", "NLControl`Ore`"}]


(* ::Subsection:: *)
(*Usage messages*)


StateSpaceToIO::usage = "transforms i/o equations to classical state-space form"

StateElimination::usage = "Finds list of rules {x1[t] -> phi1(u,y), x2[t] -> phi2(u,y),...} from classical state equtions."

ReducedDifferentialForm::usage = "finds reduced differential form"

Irreducibility::usage = "tests if i/o equations are irreducible"

Reduction::usage = "reduces i/o equatons"

AutonomousElementsD::usage = "Nonlinear counterpart of AutonomousElements."

StateDifferentialsLeftQuotient::usage = "finds differentials of the state coordinates using polynomial quotients."

StateDifferentialsAdjoint::usage = "finds differentials of the state coordinates using adjoint polynomials."

Realizability::usage = "tests if the i/o equations are transformable into
classical state-space form"

Realization::usage = "transforms i/o equatons into classsical state-space form"

RealizationGUI::usega = "";

ClassicTransformability::usage = "tests if generalized state equations are
transformable into classical state-space from."

ClassicTransform::usage = "transforms generalized state equatons into classsical 
state-space form"

Lower::usage = "lowers the maximum input shift of the generalized state 
equations"

LoweringOrder::usage = "LoweringOrder->n tries to lower the order exactly by n."

SubspaceI::usage = "Method->SubspaceI uses sequence Ik to solve the problem"

SubspaceH::usage = "Method->SubspaceH uses sequence Hk to solve the problem"

SubspaceD::usage = "Method->SubspaceD uses sequence Dk to solve the problem"

OrePolynomials::usage = "Method->TwistedPolynomials uses twisted polynomials 
to solve the problem"


TransferFunction::usage = "Finds the generalized transfer function for io or state equations."


LCMRow::usage = ""
LCMColumn::usage = ""

ColumnReducedQ::usage = "Checks if the io equations/matrix is in column reduced form"
RowReducedQ::usage    = "Checks if the io equations/matrix is in row reduced form"
DoubleReducedQ::usage = "Checks if the io equations/matrix is in row and column reduced form."
DoublyReducedQ::usage = "Checks if the conditions required in our Reduction paper (Proc. Est. Acad. Sci. 2014)"
PopovFormQByOreP::usage = "Tests if the matrix or i/o equations are in Popov form,"
WeakPopovFormQ::usage = ""
StrongPopovFormQ::usage ="Checks if the io equations are in strong Popov form."



PLMOrder::usage=""
OreRowTransformation::usage = ""
RowReduction::usage = "RowReduction[ioeq]/RowReduction[P,R]"
ColumnReduction::usage = "ColumnReduction[ioeq]/ColumnReduction[P,R]"
DoubleReduction::usage = "Transforms ioeq/matrix first into row and then into column reduced form"
DoublyReduce::usage = "Transforms i/o equatons doubly reduced form, based on ECC 1999 approach (Discrete only)."

PopovForm::usage = "PopovForm[ioeq] finds (explicit) Popov form; based on LeftKernel."

InverseSystem::usage = "finds explict equations with respect to the highest shifts/derivatives of the inputs."

ReduceConstantTerms::usage = "reduces constant terms of the i/o equations"

CanonicalIO::usage = "reduces constant terms and transforms to doubly reduced form"


RelativeDegree::usage = "RelativeDegree[steq]"


ReductionGUI::usage =""
RealizationGUI::usage =""
StateSpaceToIOGUI::usage=""
TransferFunctionGUI::usage = ""


(* ::Subsection::Closed:: *)
(*Begin package*)


Begin["`Private`"]

Off[General::spell1]
Off[General::spell]
epsilon = 10.^-15;


(* ::Subsection::Closed:: *)
(*Helpful functions*)


PrintNonIntegrable[name_, n_]:= Module[{nstr, str},
nstr = ToString[n];
str = Switch[ name,
		SubspaceD, "The subspace D"<>nstr<>" is not involutive.", 
		SubspaceI, "The subspace I"<>nstr<>" is not integrable.", 
		SubspaceH, "The subspace H"<>nstr<>" is not integrable."
];
Print[str]
]


MBF[P_,R_]:=MatrixForm[BookForm[P, R]];
MBF[P_]:=MatrixForm[BookForm[P, $OreRing]]


RelativeDegree[steq:StateSpace[f_,Xt_,Ut_,t_,h_,Yt_,type_]] :=
(Length[ NestWhileList[ Simplify[PseudoLinearMap[#,steq]]&, #, FreeQ[#,Alternatives@@Ut]&]]& /@ h) -1


(* ::Subsection:: *)
(*Reduction. I/O equivalent form, transf matrix is unimodular*)


(* ::Subsubsection::Closed:: *)
(*Supporting functions*)


(* Leading element: P[[row,col]]
Active element: P[[rowC,col]]
dif shows how many degrees the element under change (active element) will be higher than it could be. 
Let deg of leading element be m.
If by default (dif=0) then the degree of active element will be at most m-1.
Setting dif=1 causes degree of active elemnt to be at most m.
dif=2 means active element has at most degree m+1 . Etc. 
*)
Clear[OreRowTransformation]
OreRowTransformation[P_, {row_,col_}, rowC_, R_OreRing, dif_Integer:0, opts___Rule ] := Module[{prn, Pnew, \[Gamma], r, Ek, div, \[Rho]act},
	prn = PrintInfo /. {opts} /. Options[ OreRowTransformation ];
	Pnew = P;
	\[Rho]act = OreExponent[P[[rowC,col]]];
	{\[Gamma], r} = RightQuotientRemainder[ P[[rowC,col]], P[[row,col]], R];
	If[ Head[\[Gamma]]===OreP, Do[ \[Gamma][[-j]]=0, {j,dif}] ];
	Pnew[[rowC]] = Chop @ Simplify[Pnew[[rowC]] - OreMultiply[\[Gamma], Pnew[[row]], R]];
	Ek = IdentityMatrix[Length@P ];
	Ek[[rowC,row]] = -\[Gamma];
	div = LeadingElement[P[[row,col]]];
	If[prn,
		Print["Leading element: row = ", row, "  col = ", col, ".  Row to change = ", rowC ];
		Print[row, ". row is multiplied by ", BookForm[-\[Gamma], R], " and added to ", rowC, ". row" ];
		Print["Pnew = ", MBF[Pnew] ];
		]; 
	{Pnew, Ek, div!=0}
]
Options[ OreRowTransformation ] = {PrintInfo->False};


iPivots[nij_,prn___] := Module[{ni, maxpos, ji},
	ni = Max /@ nij;
	maxpos = Flatten@MapThread[ Position[#1,#2,1,1]&, {nij,ni}];
	ji = If[ Unequal@@maxpos, maxpos, {}];
	If[TrueQ[prn], If[ ji =!= {}, Print["pivot indices: ", ji],
		 Print["Failed to choose pivots"] ]];
	ji
]
(*Alternative, finds the pivots on milder conditions.*)
(*iPivots[nij_,prn___] := Module[{ni, maxpos, dist, ji},
	ni = Max /@ nij;
	maxpos = MapThread[ Flatten@Position[#1,#2,1]&, {nij,ni}];
	dist = Distribute[maxpos, List];
	ji = Flatten@Select[dist, Unequal@@#&,1];
	If[TrueQ[prn], If[ ji =!= {}, Print["pivot indices: ", ji],
		 Print["Failed to choose pivots"] ]];
	ji
]*)

Clear[iInfBox];
(*iInfBox[int1_, symb1_, op1_, op2_, symb2_, int2_, test_]:= StyleBox[ RowBox[
	{int1 , "=", symb1, " ", If[test,op1,op2], " ", symb2, "=", int2, " "}
	], FontColor \[Rule] If[test, Black, Red] ]*)

iInfBox[int1_, symb1_, op1_, op2_, symb2_, int2_, test_]:= StyleBox[ RowBox[
	{int1 , "=", symb1, " ", If[test,op1,op2], " ", symb2, "=", int2, " "}
	], FontColor -> If[test, Black, Red] ]
iInfBox[symb_, int_] := RowBox[{symb," = ",int}]


Clear[iRowCond, iColCond, iRowCondU]
iRowCond[nij_, ji_, ni_, n_String, prn_] := Module[{p,q,cond,condPrint,test},
	{p, q} = Dimensions[nij];
	cond = condPrint = Array[True&, {p,q}];
	Do[
		cond[[i,k]] = test = If[ k<ji[[i]], nij[[i,k]]<ni[[i]], nij[[i,k]]<=ni[[i]] ];
		condPrint[[i,k]] = Which[
			k<ji[[i]], iInfBox[ nij[[i,k]], Subscript[n, i,k], "<", "\[NotLess]", Subscript[n, i], ni[[i]], test],
			k==ji[[i]], iInfBox[ Subscript[n, i], ni[[i]] ],
			k>ji[[i]], iInfBox[ nij[[i,k]], Subscript[n, i,k], "\[LessEqual]", "\[NotLessSlantEqual]", Subscript[n, i], ni[[i]], test]
		],
	{i,p}, {k,q}];
	If[prn, Print["conditions on rows: ", AllTrue[Flatten@cond, TrueQ]];
		Print[ DisplayForm[ GridBox[ condPrint ]] ]  ]; 
	cond
]
iColCond[nij_, ji_, ni_, n_String, prn_] := Module[{p, q, condPrint, cond, test, col},
	{p, q} = Dimensions[nij];
	cond = condPrint = Array[True&, {p,q}];
	Do[ col = ji[[i]]; Table[ 
		 cond[[k,col]] = test = If[k!=i, nij[[k,col]]<nij[[i,col]] ,True];
		 condPrint[[k,col]] = Which[
			k!=i, iInfBox[ nij[[k,col]], Subscript[n, k,col], "<", "\[NotLess]", Subscript[n, i], ni[[i]], test],
			k==i, iInfBox[Subscript[n, i], ni[[i]] ]
		],
	{k,p}], {i,p}];
	If[prn, Print["Conditions on columns: ", AllTrue[Flatten@cond,TrueQ]];
		Print[ DisplayForm[GridBox[condPrint]]] ];
	cond
]
iRowCondU[sik_, ji_, ni_, s_String, n_String, prn_] := Module[{p,m,cond,condPrint,test},
{p, m} = Dimensions[sik];
	cond = condPrint = Array[True&, {p,m}];
	Do[ 
		cond[[i,k]] = test = sik[[i,k]]<ni[[i]];
		condPrint[[i,k]] = iInfBox[ sik[[i,k]], Subscript[s, i,k], "<", "\[NotLess]", Subscript[n, i], ni[[i]], test],
	{i,p}, {k,m}];
	If[prn, Print["uk: conditions on rows: ", AllTrue[Flatten@cond,TrueQ]]; 
		Print[ DisplayForm[ GridBox[ condPrint]]]
	]; cond
]


(* ::Subsubsection:: *)
(*iSortRows, LCMRow, LCMColumn, RowReducedQ, ColumnReducedQ, PopovFormQByOreP*)


iSortRows[P_, R___]:= Module[{p, \[Sigma], idx},
	p = Length@P;
	\[Sigma] = Max /@ OreExponent[P];
	idx = Last /@ Sort[ Transpose[ {\[Sigma], Range[p]}], #1[[1]]<=#2[[1]]& ];
	{P[[idx]], IdentityMatrix[p][[idx]]}
]


LCMRow[P_?MatrixQ, R_OreRing] := Module[{rowdeg, Nmax, DiagM, MdotP, L},
	rowdeg = Max /@ OreExponent[P];
	Nmax = Max[rowdeg];
	DiagM = DiagonalMatrix[ GenerateOreP  /@ (Nmax-rowdeg)];
	MdotP = OreDot[DiagM, P, R];
	Map[ If[ OreExponent[#]===Nmax, LeadingElement[#], 0]&, MdotP, {2}]
]

LCMColumn[P_?MatrixQ, R_OreRing] := Transpose[ LCMRow[ Transpose[P], R]]


(*Zero rows are allowed.*)
RowReducedQ[P0_?MatrixQ, R_OreRing] := Module[ {P}, 
	P = DeleteCases[ OreSimplify[P0, R], {0..}]; 
	MatrixRank[ LCMRow[P,R] ] === Length@P
]

(*Make General function, add OreImp Simplify? Ei!! *)
RowReducedQ[ioeq:IO[eqs_,Ut_,Yt_,t_, type_]] := RowReducedQ[ ToOrePD[ioeq, Yt], DefineOreRing[\[Eth],t,type] ]


ColumnReducedQ[P_?MatrixQ, R_OreRing] := MatrixRank[ LCMColumn[ OreSimplify[P,R], R ]] === Length@P
ColumnReducedQ[ioeq:IO[eqs_,Ut_,Yt_,t_, type_]] := ColumnReducedQ[ ToOrePD[ioeq, Yt], DefineOreRing[\[Eth],t,type] ]


Clear[PopovFormQByOreP];
PopovFormQByOreP[P0_?MatrixQ, R_OreRing, opts___Rule] := Module[
{P, Lr, prn, p, q, m, fi, nij, sik, ni, ij, dist, idx, ji, 
cond1, maxpos, condPD, leading, condYrow, condYrowPrint, condYcol, condYcolPrint, condUrow, condUrowPrint, condPivotInc, test},
prn = PrintInfo /. {opts} /. Options[ StrongPopovFormQ ];
P = OreSimplify[P0, R];
Lr = LCMRow[P,R];
p = Length@P;
q = Length@P[[1]]; (*q \[GreaterEqual] p*)
nij = OreExponent[P];
If[prn, Print[Subscript["n","ij"], " = ", MatrixForm[nij] ]];
ni = Max /@ nij;
ji = iPivots[nij, prn];
If[ ji==={}, False,	
	cond1 = LessEqual@@ni;
	If[prn, 
		Print[ Table[Subscript["n", i],{i,p}]," = ", ni]; 
		Do[ Print[ DisplayForm[ 
			iInfBox[ ni[[i]],Subscript["n", i],"\[LessEqual]", "\[NotLessSlantEqual]",Subscript["n", i+1],ni[[i+1]], ni[[i]]<=ni[[i+1]] ]
		]], {i, p-1}]
	];
	leading = Table[ LeadingElement[P[[i,ji[[i]] ]] ],{i,p}];
	condPD = #===1 &/@ leading;
	If[prn, Print["Leading elements: ", leading," ; ", condPD]];

	condYrow = iRowCond[nij, ji, ni, "n", prn];
	condYcol = iColCond[nij, ji, ni, "n", prn];

	condPivotInc = Table[If[ni[[i]]===ni[[i+1]],ji[[i]]<ji[[i+1]], True],{i,p-1}];
	If[prn, Print["Check if the pivot indices are increasing in case of the equal row degrees: ", condPivotInc]];
	AllTrue[Flatten[{cond1,condPD,condYrow, condYcol, condPivotInc}], TrueQ]
](*End If *)
]

PopovFormQByOreP[ioeq:IO[eqs_,Ut_,Yt_,t_, type_], opts___] := PopovFormQByOreP[ ToOrePD[ioeq, Yt], DefineOreRing[\[Eth],t,type], opts]

Options[PopovFormQByOreP] := {PrintInfo->False}


RowEchelonQ[mat_,R___OreRing]:=Module[{idx,pivots},
	idx = Range[Length[mat]];
	pivots = Position[#, _?(#=!=0&), {1}, 1, Heads->False][[1,1]]& /@ mat;
	Sort[pivots]===idx
]

(*Make General function for IOeq*)


WeakPopovQ[P_?MatrixQ,R_] := Module[{L=LCMRow[P,R]}, MatrixRank[L]===Length@P && RowEchelonQ[L]]

WeakPopovQ[ioeq:IO[eqs_,Ut_,Yt_,t_, type_], opts___] := WeakPopovQ[ ToOrePD[ioeq, Yt], DefineOreRing[\[Eth],t,type], opts]


(* ::Subsubsection::Closed:: *)
(*RowReduction*)


Clear[RowReduction];

RowReduction[P_?MatrixQ, OreRing[x_, t_, typespec_], opts___] := RowReduction[P, OreRing[x, t, typespec, {{}, {}}], opts]
RowReduction[P_?MatrixQ, Rimp:OreRing[x_, t_, typespec_, {_, ass_List}], opts___Rule] := Module[
{prn, R, PBar, p, k, Uk, Rk, Ek, \[Sigma], Nmax, L, i, \[Alpha], \[Gamma], rowi1, Umat, Ltest, restr},
	prn = PrintInfo /. {opts} /. Options[ RowReduction ];
	R = OreRing[x, t, typespec];
	PBar = OreSimplify[ P, Rimp];
		If[ prn, Print["PBar = ", MBF[PBar, R], "  (Assumptions used)" ]];
	p = Length@P;
	k = 1; Uk = {R}; restr={};
	While[
		{PBar, Rk} = iSortRows[ PBar ];
		PrependTo[Uk, Rk];
		\[Sigma] = Max /@ OreExponent[PBar]; (* Row degrees *)
		Nmax = Max[\[Sigma]];
		L = LCMRow[PBar, R];
		i = MatrixRank[L];
			If[ prn, CellPrint[ Cell["RowReduction Step "<>ToString[k], "ItemParagraph"]]];
			If[ prn, Print["Perm matrix R",k, " = ", MatrixForm[Rk]] ];
			If[ prn, Print["PBar = R",k,"*PBar = ",MBF[PBar,R]] ];
			If[ prn, Print["rowdegree = ", \[Sigma]] ];
			If[ prn, Print["N = ", Nmax]];
			If[ prn, Print["L = ", MatrixForm[ Map[ BookForm[#, R]&, L, {2}] ]] ];
			If[ prn, Print["MatrixRank[L] = ",i] ];
	i < p && k<20,
		i = 2;
		Ltest = Take[L,2];
		While[ MatrixRank[Ltest]===i, AppendTo[Ltest,L[[++i]]] ];
		--i; 
		\[Alpha] = NullSpace[ Transpose@L ];
		\[Gamma] = Nmax - \[Sigma][[i+1]];
		rowi1 = MapThread[ #1 GenerateOreP [\[Sigma][[i+1]]-#2]&, {Take[ \[Alpha][[-1]],i]/.t->t-\[Gamma], Take[\[Sigma], i]}];
		rowi1 = Join[rowi1, {1}, Array[0&, p-i-1] ];
		Ek = IdentityMatrix[p];
		Ek[[i+1]] = rowi1;
		PBar = OreDot[Ek, PBar, R];
		PBar = OreSimplify[PBar, Rimp]; 
		PrependTo[Uk, Ek]; k++;
			If[prn, Print["i = ",i] ];
            If[prn, Print["\[Alpha] = ",\[Alpha]] ];
			If[prn, Print["E",k," = ",MBF[Ek,R]] ];
			If[prn, Print["PBar = E",k, "*PBar = ",MBF[PBar, R]]];

	]; (* End While *)
	Umat = If[ Length[Uk]>2, OreDot @@ Uk, Uk[[1]] ];
	{PBar, Umat, restr}
]

RowReduction[ioeq:IO[eqs_, Ut_, Yt_, t_, type_]] := Module[
{P, fi, R, Rimp, Umat, finew},
	P = ToOrePD[ioeq, Yt];
	fi = #1-#2& @@@ eqs;
	R = DefineOreRing[\[Eth], t, type];
	Rimp = DefineOreRing[\[Eth], ioeq];
	Umat = RowReduction[P, Rimp][[2]];
	finew = ApplyMatrix[Umat, fi, R];
	IO[ Thread[finew==0], Ut, Yt, t, type]
]
Options[RowReduction] = {PrintInfo->False, PLMOrder->1};

(* KEEP. How to check ComplexInfinity?
RowReduction[ioeq:IO[eqs0_, Ut_, Yt_, t_, type_], opts___Rule] := Module[
{prn, plmorder, eqs, ass, K, P, PBar, correctsystem},
	prn      = PrintInfo /. {opts} /. Options[ RowReduction ];
	plmorder = PLMOrder  /. {opts} /. Options[ RowReduction ];
	eqs = Equal @@@ eqs0;
	ass = NestList[ PseudoLinearMap[#, t, type]&, eqs, plmorder]//Flatten;
	K = DefineOreRing[\[Eth], t, type];
	P = ToOrePD[ioeq, Yt];
		If[ prn, Print["P = ", MBF[P,K]] ];
	correctsystem = Catch[
		PBar = Simplify[P, Assumptions -> ass ];
			If[ prn, Print["PBar = ", MBF[PBar,K], "  (Assumptions used)" ]];
		If[!FreeQ[PBar,ComplexInfinity|Indeterminate], Throw[False]];
	]; (* End Catch *)
	If[ correctsystem === False, 
		Print["Inconsistent system"]; {},
		RowReduction[PBar, K, opts, Assumptions -> ass]
	](* End If *)
]*)



(* ::Subsubsection::Closed:: *)
(*ColumnReduction*)


Clear[ColumnReduction];
ColumnReduction[P_?MatrixQ, OreRing[x_, t_, typespec_], opts___] := ColumnReduction[P, OreRing[x, t, typespec, {{}, {}}], opts]
(*This method is slower and gives more difficult unimodular matrix, but simpler matrix P than the main method*)
ColumnReduction[Pmat_, Rimp:OreRing[x_, t_,typespec_, {_, ass_List} ], opts___Rule] := Module[
{prn, R, P, p, \[Gamma], r, col, count, rstr0, rstr, pivots, pivotLC, Emonic, \[Rho], \[Rho]0, \[Rho]k, \[Rho]act, \[Rho]dif, Emat, Ek, div, Lcol},
prn = PrintInfo /. {opts} /. Options[ ColumnReduction ];
R = OreRing[x, t, typespec];
P = OreSimplify[ Pmat, Rimp];
	If[ prn, Print["PBar = ", MBF[P, R], "  (Assumptions used)" ]];
p = Length@P;
Emat = IdentityMatrix[p];
rstr = {};
count = 0; (* Total counter of transformations *)
Do[
If[prn, Print[ Style[ "Main loop index \[Lambda] = " <> ToString[\[Lambda]], Background->Yellow] ]];
pivots = {};
Do[ 
	If[prn, Print[ Style["Row " <> ToString[row], Background->Pink] ]];
	\[Rho] = OreExponent[P[[row]]];
	\[Rho]0 = Max[\[Rho]]; (* deg leading *)
	col = Position[\[Rho], \[Rho]0, 1, 1][[1,1]]; (* col leading *)
	AppendTo[pivots, col ];
	Do[ If[ k>row,
		\[Rho]k = Max@OreExponent@P[[k]]; (* max deg of active row *)
		If[ \[Rho]dif = OreExponent[P[[k,col]]] - Min[\[Rho]0, \[Rho]k-1]; \[Rho]dif >0,
			count++;
			{P, Ek, rstr0} = OreRowTransformation[P, {row, col}, k, R, \[Rho]dif, opts]; (*Korrigeeridas!!*)
			Emat = Chop@Simplify@Chop@OreDot[Ek, Emat, R];
			AppendTo[rstr, rstr0 ]
		]], (*End If, If *)
	{k, p}],
{row, p}], (* row=p is necessary, cause pivot element is fixed.*)
{\[Lambda], 2} ]; (*End While*) 
If[prn, Print["Counter of divisions = ", count]];

(* ========== Making pivot elements monic =================== *)
	pivotLC = LeadingElement/@ MapThread[Part, {P, pivots}]; 
	Emonic = DiagonalMatrix[ OreP[1/#]& /@ pivotLC ];
	rstr = Join[rstr, #!=0& /@ pivotLC ];
	P =  Chop@Simplify@Chop@OreDot[Emonic, P, R];
	Emat =  Chop@Simplify@Chop@OreDot[Emonic, Emat, R];
	If[prn, 
		Print["pivotite juhtelemendid: ",pivotLC];
		Print["Original restrictions: ", rstr]
	];(*End Printing If *)
rstr = Reduce[rstr];
{P, Emat, rstr}
]


ColumnReduction[ioeq:IO[eqs_, Ut_, Yt_, t_, type_]] := Module[
{fi, R, Rimp, P, Umat, Pnew, finew, leadEls,cond},
	P = ToOrePD[ioeq, Yt];
	fi = #1-#2& @@@ eqs;
	R = DefineOreRing[\[Eth], t, type];
	Rimp = DefineOreRing[\[Eth], ioeq];
	Umat = ColumnReduction[P, Rimp][[2]];
	finew = ApplyMatrix[Umat, fi, R];
	IO[ Thread[finew==0], Ut, Yt, t, type]
]

Options[ColumnReduction] = {PrintInfo->False};



(* ::Subsubsection::Closed:: *)
(*PopovFrom, InvserseSystem*)


(*This method is slower and gives more difficult unimodular matrix, but simpler matrix P than the main method*)

PopovForm[P_?MatrixQ, OreRing[x_, t_, typespec_], opts___] := 
	PopovForm[P, OreRing[x, t, typespec, {{}, {}}], opts]
PopovForm[P0_?MatrixQ, Rimp:OreRing[x_, t_, typespec_, {_, ass_List}], opts___Rule] := Module[
{prn, P, R, p, \[Gamma], col, count, rstr0, rstr, pivots, L, Lcol, pivotLC, Emonic,  \[Rho], \[Rho]0, \[Rho]i, Emat, Ek},
prn = PrintInfo /. {opts} /. Options[ PopovForm ];
R = OreRing[x, t, typespec];
p = Length@P0;
{P, Emat, rstr} = RowReduction[P0, Rimp, opts];
If[prn, Print[ Style[ "The matrix is rowreduced now. Transforming to Popov form...", Background->Yellow] ]];
count = 0; (* Total counter of transformations *)
Do[
	If[prn, Print[ Style[ "Main loop index \[Lambda] = " <> ToString[\[Lambda]], Background->Yellow] ]];
	pivots = {};
	Do[ 
		If[prn, Print[ Style["Row " <> ToString[row], Background->Pink] ]];
		\[Rho] = OreExponent[P[[row]]];
		\[Rho]0 = Max[\[Rho]];
		col = Position[\[Rho], \[Rho]0, 1, 1][[1,1]]; 
		AppendTo[pivots, col ];
		Do[ If[ k != row,
			If[ OreExponent[P[[k,col]]] >= \[Rho]0,
				count++;
				{P, Ek, rstr0} = OreRowTransformation[P, {row, col}, k, R, opts];
				Emat = Chop@Simplify@Chop@OreDot[Ek, Emat, R];
				AppendTo[rstr, rstr0 ]
			]], (*End If, If *)
		{k, p}],
	{row, p}], (* row=p is necessary, cause pivot element is fixed.*)
{\[Lambda], 2}]; (*End \[Lambda]-loop*) 
If[prn, Print["Total number of row transformations = ", count]];

(* ========== Making pivot elements monic =================== *)
	pivotLC = LeadingElement/@ MapThread[Part, {P, pivots}]; 
	Emonic = DiagonalMatrix[ OreP[1/#]& /@ pivotLC ];
	rstr = Join[rstr, #!=0& /@ pivotLC ];
	P =  Chop@Simplify@Chop@OreDot[Emonic, P, R];
	Emat =  Chop@Simplify@Chop@OreDot[Emonic, Emat, R];
	If[prn, 
		Print["Pivot indices: ",pivots];
		Print["Original restrictions: ", rstr]
	];(*End Printing If *)
rstr = Reduce[rstr];
{P, Emat, rstr}
]


(*Elements of P are supposed to OrePolynomial objects*)
PopovForm[P_?MatrixQ, A_OreAlgebraObject,b0___Integer, opts___Rule] := Module[{p, Ip, P1,Kl,b},
	If[ b0===Null,b=1,b=b0];
	p = Length@P;
	Ip = ToOrePolynomial[IdentityMatrix[p], A];
	P1 = Map[#**ToOrePolynomial[1, A]&, P, {2}];
	Kl = LeftKernel[ Join[P1,Ip], A];
	{Take[Kl, All, -p],Take[Kl, All, p], {}} (* Returns: {Pnew,U, {}} *)
]


(*PopovForm[ ioeq:IO[ eqs:{__Rule}, __], opts___] := ioeq*)

PopovForm[ioeq:IO[eqs:{__Equal|__Rule}, Ut_, Yt_, t_, type_], opts___] := 
Module[{met, \[CurlyPhi], A, R, P, Pnew, Umat, finew, restr, cond, pivots, deg, \[Sigma], pos, Ytmax, eqsrule, ioeq1},
	met = Method /. {opts} /. Options[ PopovForm ];
	\[CurlyPhi] = #1-#2&@@@eqs;
If[ met === LeftKernel,
	A = Switch[type, 
		TimeDerivative, OreAlgebra[Der[t]],
		Shift, OreAlgebra[S[t]],
		TimeDelay, OreAlgebra[Der[t],S[t]] ];
	P = ToOrePolynomialD[\[CurlyPhi], Yt, A];
	{Pnew, Umat, restr} = PopovFormMatrix[P,A, opts]; cond={};pivots={};
	finew = ApplyMatrix[Umat, \[CurlyPhi]], 
(*Else*)
	P = ToOrePD[ioeq, Yt];
	R = DefineOreRing[\[Eth],t,type];
	{Pnew, Umat, restr} = PopovForm[P, R, opts];
	finew = ApplyMatrix[Umat, \[CurlyPhi], R]
];(* End If *)
	ioeq1 = IO[ Thread[finew==0], Ut, Yt, t, type];
	If[ True, (* allj\[ADoubleDot]rgnev Ei ole p\[ADoubleDot]ris sama mis ModelToRules *)
		deg = Outer[ MaxPLMOrder[#1,#2,type]&, finew, Yt];
		\[Sigma] = Max /@ Transpose[deg];
		pos = Flatten @ MapThread[Position[#1,#2]&, {Transpose[deg], \[Sigma]}];
		Ytmax = PseudoLinearMap[Yt, {t,\[Sigma]}, type];
		eqsrule = MapThread[ Solve[#1==0,#2][[1,1]]&, {finew[[pos]], Ytmax}];
		IO[eqsrule, Ut, Yt, t, type],
	(* Else  *)
		Print["Nonlinear transformations may be required to bring the system into Popov form."];
		ioeq1
	]
]

Options[PopovForm] = {Method->LeftQuotient, PrintInfo->False};


(* ::Subsubsection::Closed:: *)
(*Reducing constant terms*)


CanonicalIO::const = "Constant terms are reduced.";
CanonicalIO::dred = "IO equations are turned into doubly-reduced form.";

CanonicalIO[ IO[eqs_, Ut_, Yt_, t_, Shift], outvar_] := 
Module[ {patt, minshift, eqs1, iored, repl },
	patt = Alternatives @@ (Head /@ Join[Ut, Yt]);
	minshift = Min @ Cases[ #, patt[t+i_.] -> i, -1]& /@ eqs;
	eqs1 = MapThread[#1 /. t -> t-#2&, {eqs,minshift}];
	{iored, repl} = ReduceConstantTerms[ IO[eqs1, Ut, Yt, t, Shift], outvar ][[1]];
	If[ !MatchQ[ repl, {(_[t] -> _[t])..}], 
		Message[ CanonicalIO::const ]];
	{Simplify @ ExpandAll @ ModelToRules[iored], repl }
]


ReduceConstantTerms[ IO[eqs_, Ut_, Yt_, t_, Shift], outvar_ ] := 
Module[ {Wt, Utpat, Ytpat, system, phi, Ye, ye, sol },
	Wt = NewVariables[outvar, Length@Yt, t ];
	phi = #1-#2 == 0 & @@@ eqs;
	Ye = Array[ye, Length@Yt ];
	Utpat = Ut/.t -> t+_.;
	Ytpat = Yt/.t -> t+_.;
	system = phi /. Thread[Utpat->0] /. Thread[Ytpat->Ye];
	sol = Ye /. Solve[ system, Ye ];
	sol = Select[sol, FreeQ[#, Complex]&]; (* Removes Complex solutions*)
	{IO[ Chop[ Simplify[
		phi /. Thread[(Yt/.t -> t+i_.) -> (Wt/.t -> t+i)+# ]
	]], Ut, Wt, t, Shift ], Thread[Wt -> Yt-#] }& /@ sol
]


(* ::Subsubsection::Closed:: *)
(*Strong Popov form*)


Clear[StrongPopovFormQ];
StrongPopovFormQ[IO[eqs_,Ut_,Yt_, t_, type_],opts___?OptionQ] := Module[
{prn, p, m, fi, nij, sik, ni, ij, dist, idx, ji, 
cond1, maxpos, condPD, leading, condYrow, condYrowPrint, condYcol, condYcolPrint, condUrow, condUrowPrint, condPivotInc, test},
prn = PrintInfo /. {opts} /. Options[ StrongPopovFormQ ];
p = Length@Yt;
m = Length@Ut;
fi = (#1-#2)&@@@eqs;
nij = Outer[ MaxPLMOrder[#1, #2, type]&, fi,Yt];
sik = Outer[ MaxPLMOrder[#1, #2, type]&, fi,Ut];
If[prn, Print[
Subscript["n","ij"], " = ", MatrixForm[nij],"  ", Subscript["s", "ik"], " = ",MatrixForm[sik]]];
ni = Max /@ nij;
ji = iPivots[nij, prn];
If[ ji==={}, False,	
	cond1 = LessEqual@@ni;
	If[prn, 
		Print[ Table[Subscript["n", i],{i,p}]," = ", ni]; 
		Do[ Print[ DisplayForm[ 
			iInfBox[ ni[[i]],Subscript["n", i],"\[LessEqual]", "\[NotLessSlantEqual]",Subscript["n", i+1],ni[[i+1]],ni[[i]]<=ni[[i+1]] ]
		]], {i, p-1}]
	];
	leading = MapThread[PseudoLinearMap[Yt[[#1]],{t,#2},type]&,{ji,ni}];
	If[prn, Print["leading elements: ", leading]];

	condPD = MapThread[D[#1,#2]===1&,{fi,leading}];
	If[prn, Print["partial derivatives equal to 1: ", condPD]];

	condYrow = iRowCond[nij, ji, ni, "n", prn];
	condYcol = iColCond[nij, ji, ni, "n", prn];
	condUrow = iRowCondU[sik, ji, ni, "s", "n", prn];

	condPivotInc = Table[If[ni[[i]]===ni[[i+1]],ji[[i]]<ji[[i+1]], True],{i,p-1}];
	If[prn, Print["Check if the pivot indices are increasing in case of the equal row degrees: ", condPivotInc]];

	AllTrue[Flatten[{cond1,condPD,condYrow, condYcol,condPivotInc, condUrow}], TrueQ]
](*End If *)
]

Options[StrongPopovFormQ]:={PrintInfo->False}
(* indekseeritud muutujad: 
nijtab = Outer[ MaxPLMOrder[#1, #2, type]&, fi,Yt];
MapIndexed[(nij[##]=#1)&,nijtab]
*)


Clear[StrongPopovFormUQ];
(*Yt and Ut are in reversed order.*)
StrongPopovFormUQ[IO[eqs_, Yt_, Ut_, t_, type_],opts___?OptionQ] := Module[
{prn, n,s, p, m, fi, nij, sik, si, ij, dist, idx, ji, 
cond1, maxpos, condPD, leading, condUrow, condUcol, condPivotInc, test},
prn = PrintInfo /. {opts} /. Options[ StrongPopovFormQ ];
p = Length@eqs;
m = Length@Ut;
fi = (#1-#2)&@@@eqs;
s="s"; n="n";
nij = Outer[ MaxPLMOrder[#1, #2, type]&, fi,Yt];
sik = Outer[ MaxPLMOrder[#1, #2, type]&, fi,Ut];
If[prn, Print[


\!\(\*SubscriptBox[\(n\), \("\<ij\>"\)]\)," = ", MatrixForm[nij],"  ", 


\!\(\*SubscriptBox[\("\<s\>"\), \("\<ik\>"\)]\), " = ",MatrixForm[sik]]];
si = Max /@ sik;
ji = iPivots[sik,prn];
If[ ji === {}, False,
	cond1 = LessEqual@@si;
	If[prn, 
		Print[ Table[Subscript[s, i],{i,p}]," = ", si]; 
		Do[ Print[ DisplayForm[ test = si[[i]]<=si[[i+1]];
			iInfBox[ si[[i]], Subscript[s, i], "\[LessEqual]", "\[NotLessSlantEqual]", Subscript[s, i+1], si[[i+1]], test ]
		]], {i, p-1}]
	];
	leading = MapThread[PseudoLinearMap[Ut[[#1]],{t,#2},type]&,{ji,si}];
	If[prn, Print["leading elements: ", leading]];

	condPD = MapThread[D[#1,#2]===1&,{fi,leading}];
	If[prn, Print["partial derivatives equal to 1: ", condPD]];

	condUrow = iRowCond[sik, ji, si, s, prn];
	condUcol = iColCond[sik, ji, si, s, prn];

	condPivotInc = Table[If[si[[i]]===si[[i+1]],ji[[i]]<ji[[i+1]], True],{i,p-1}];
	If[prn, Print["Check if the pivot indices are increasing in case of the equal row degrees: ", condPivotInc]];

	AllTrue[Flatten[{cond1,condPD,condUrow, condUcol,condPivotInc}],TrueQ]
](*End If *)
]

Options[StrongPopovFormQ]:={PrintInfo->False}
(* indekseeritud muutujad: 
nijtab = Outer[ MaxPLMOrder[#1, #2, type]&, fi,Yt];
MapIndexed[(nij[##]=#1)&,nijtab]
*)


(* ::Subsection::Closed:: *)
(*Reduction. Transfer equivalent form, transf matrix is not unimodular*)


Irreducibility[ ioeq:IO[{__Rule}, __], opts___ ] :=
Module[ {x, PQ, K, G, met, res, badinput },
    met = Method /. {opts} /. Options[ Irreducibility ];
	res = Switch[ met,
	OrePolynomials,
		K = DefineOreRing[x,ioeq];
		PQ = MapThread[ Join, FromIOToOreP[ioeq] ];
		G = LowerLeftTriangularMatrix[PQ, K];
		Max[ Map[ Length, G, {2} ]] < 2,
	SubspaceI, SequenceI[ ioeq, All][[-1,1]] === {},
	SubspaceH, SequenceH[ ioeq, All][[-1,1]] === {},
	_, badinput
	]; 
	res /; res =!= badinput
] (* End Module *)

Irreducibility[ ioeq:IO[{__Rule}, __], opts___ ] := Irreducibility[ ModelToRules[ioeq] ]

Options[ Irreducibility ] = { Method -> OrePolynomials };


ReducedDifferentialForm[ ioeq:IO[ {__Rule}, __], opts___ ] :=
Module[ { met, res, x, P, Q, PQ, G, P1, Q1, K, Hinf },
	met = Method /. {opts} /. Options[ ReducedDifferentialForm ];
	res = Switch[ met,
	OrePolynomials,
		K = DefineOreRing[x,ioeq];
		{P, Q} = FromIOToOreP[ioeq];
		PQ = MapThread[ Join, {P, Q}];
		G = LowerLeftTriangularMatrix[PQ, K];
		If[ Max@ Map[Length, G, {2} ] < 2,
			{}, (* Else *)
			P1 = LeftQuotient[P, G, K];
			Q1 = LeftQuotient[Q, G, K];
			SimplifyBasis[ FromOrePToSpanK[ P1, Q1, ioeq ]]
		],
	SubspaceH,
		Hinf = SequenceH[ ioeq, All][[-1]];
		If[ Hinf[[1]] === {}, {}, Hinf ],
	_,  
		badinput
	]; (* End Switch *)
	res /; res =!= badinput
] (* End Module *)

Options[ ReducedDifferentialForm ] = { Method -> OrePolynomials };


Reduction::irred = "The system is irreducible.";

Reduction[ ioeq:IO[ eqs:{__Rule}, Ut_, Yt_, t_, type_ ], opts___] := 
Module[{ dphi, res, badinput }, 
	dphi = ReducedDifferentialForm[ ioeq, opts ];
	res = Switch[ dphi,
		{}, Message[ Reduction::irred ]; ioeq, 
		_SpanK, IO[ Thread[ IntegrateOneForms[dphi] == 0], Ut, Yt, t, type],
		_, badinput
	];
res /; res=!=badinput
] (* End Module *)

Reduction[ ioeq:IO[{__Equal}, __ ], ___] :=  Reduction[ ModelToRules[ioeq]]

Options[ Reduction ] = { Method -> OrePolynomials };


(* ::Subsection::Closed:: *)
(*AutonomousElementsD*)


Clear[AutonomousElementsD];
AutonomousElementsD[R_?MatrixQ, vars_?VectorQ, T_, A_, opts___Rule]:=
Module[{rels, Rinv,R1,R2,R3, ops, Xvars,Tvec,eqs,Rp,Rpp,R2p},
	rels = Relations /. {opts} /. Options[ AutonomousElementsD ];
	Rinv = Involution[R,A];
	{R1, R2, R3} = Ext1[Rinv,A];
If[Max[OrePolynomialDegree/@Flatten[R1]]===0,
	Print["No autonomous elements"];{},
	ops = OreAlgebraOperators[A];
	Xvars = Union @ Cases[ops,_[t_]->t];
	Tvec = Table[T[i] @@ Xvars, {i,Length[ R1[[1]] ]}];
	eqs = Thread[ ApplyMatrixD[R1, Tvec] == 0];
	If[rels === False,
		{ Thread[ Tvec -> ApplyMatrixD[R2,vars] ], eqs},
		Rpp = LeftFactorize[R,R2,A];
		R2p = LeftKernel[R2,A];
		If[ Head[R2p] === Inj,
			{Thread[ Tvec -> ApplyMatrixD[R2,vars]], eqs, Thread[ ApplyMatrixD[ Rpp,Tvec] ] },
			{Thread[ Tvec -> ApplyMatrixD[R2,vars]], eqs, Thread[ ApplyMatrixD[ Join[Rpp,R2p], Tvec ]] }
		]
	]
]
]
Options[AutonomousElements] = {Relations->False};(*Default*)


(* ::Subsection::Closed:: *)
(*State elimination*)


(*StateElimination[steq:StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_]] := 
Module[ {eqs, difs, r, testdif, testeq, gi, eqsgi, maxplm},	
	eqs = {};  (* Set of io equations and their derivatives/shifts *)
	difs = {}; (* Jacobean matrix corresponging to rhs of eqs *)
	r = 0;     (* rank of the Jacobean matrix*)
	Do[ testeq = Yt[[i]] == h[[i]];
		While[
			testdif = D[ Last[testeq], {Xt}];
		MatrixRank[ Join[ difs, {testdif}] ] > r, (* Condition *)
			difs = Join[ difs, {testdif} ];
			eqs = Join[ eqs, {testeq}];
			r = r+1;
			testeq = PseudoLinearMap[ Last[eqs], steq]
		], (* End While *)
	{i, Length[Yt]}];
	If[ Length[eqs] < Length[Xt], (* non-observable case *)
		gi = ComplementSpace[ IdentityMatrix[ Length[Xt]], difs].Xt; 
		maxplm = MaxPLMOrder[ Last[eqs], Last[Yt], type];
		If[ maxplm === -Infinity, maxplm = -1];
		eqsgi = Table[ PseudoLinearMap[ Last[Yt], {t,maxplm+i}, type] == gi[[i]], 
			{i, Length[gi]} ]; (*Formulate equations using gi-s *)
		eqs = Join[ eqs, eqsgi ];
	]; (* End If *)
	Solve[eqs, Xt]
]*)


(*IO[steq_StateSpace] := StateSpaceToIO[steq]

StateSpaceToIO::output = "Output not defined."; 

StateSpaceToIO[steq:StateSpace[f_, Xt_, Ut_, t_, {}, {}, type_]] := 
	With[{}, Message[ StateSpaceToIO::output ]; {}]

StateSpaceToIO[steq:StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_]] := 
Module[ {Xtexpr, ni, eq },
	Xtexpr = StateElimination[steq][[1]];
	ni = MaxPLMOrder[Xtexpr, #1, type] & /@ Yt;
	ni = ni /. DirectedInfinity[-1] -> -1;
	eq = MapThread[ PseudoLinearMap[#1, {steq, #2+1}]&, {Thread[Yt == h], ni}];
	IO[ Simplify[ eq /. Xtexpr ], Ut, Yt, t, type]
] (* End Module *)

StateSpaceToIO[ ioeq:IO[{__Equal}, __ ]] :=  StateSpaceToIO[ ModelToRules[ioeq]]*)
(*I don't remember why it is necessary. Probably not necessary at all *)


iStateElimClaude[steq:StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_, ___]] := 
Module[ {eqs, difs, r, testdif, testeq, ni, yeqs},	
	eqs = {};  (* Set of io equations and their derivatives/shifts *)
	difs = {}; (* Jacobean matrix corresponging to rhs of eqs *)
	r = 0;     (* rank of the Jacobean matrix*)
	Do[ testeq = Yt[[i]] == h[[i]];
		While[
			testdif = D[ Last[testeq], {Xt}];
		MatrixRank[ Join[ difs, {testdif}] ] > r, (* Condition *)
			difs = Join[ difs, {testdif} ];
			eqs = Join[ eqs, {testeq}];
			r = r+1;
			testeq = PseudoLinearMap[ Last[eqs], steq]
		], (* End While *)
	{i, Length[Yt]}]; (*End Do *)
	ni = MaxPLMOrder[eqs, #1, type] & /@ Yt;
	ni = ni /. DirectedInfinity[-1] -> -1;
	yeqs = MapThread[ PseudoLinearMap[#1, {steq, #2+1}]&, {Thread[Yt == h], ni}];
	{yeqs, First@Solve[eqs,Xt]}
]

iStateElimUlle[steq:StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_,___]] := 
Module[ {Yeqs, Xeqs, eqs1, eqs2, testeq, mat,
	difs, gi, maxplm, eqsgi, ni,
	pos, YeqsOrder},
	Yeqs = {}; (* all eqations for yi*)
	Xeqs = Thread[Yt == h]; (* all equations for finding xi*)
	eqs1 = Xeqs; (* the equations obtained on prvious While loop*)
	While[ eqs2 = {}; (*the equations selected on present Do loop*)
		Do[ testeq = Simplify[PseudoLinearMap[ #, steq]]& /@ eqs1[[i]];
			mat = D[Last /@ Join[Xeqs, eqs2, {testeq}], {Xt}];
			If[  MatrixRank[mat] > Length[Xeqs]+Length[eqs2],
				AppendTo[eqs2, testeq], (* linearly independent *)
				AppendTo[Yeqs, testeq] (* testeq depends on previous equations *)
			], (* End If *)
		{i, Length[eqs1]} ]; (* End Do *)
		Xeqs = Join[Xeqs, eqs2];
		eqs1 = eqs2;
	eqs2=!={} ]; (*End While*);
(*Non-observable system*)
	If[ Length[Xeqs] < Length[Xt],
	Print["non-observable"];
		difs = D[ Last/@Xeqs, {Xt}];
		gi = ComplementSpace[ IdentityMatrix[ Length[Xt]], difs].Xt;
		maxplm = MaxPLMOrder[ Last[Xeqs], Last[Yt], type];
		If[ maxplm === -Infinity, maxplm = -1];
		eqsgi = Table[ PseudoLinearMap[ Last[Yt], {t,maxplm+i}, type] == gi[[i]], 
			{i, Length[gi]} ]; (*Formulate equations using gi-s *)
		Xeqs = Join[ Xeqs, eqsgi ];
		ni = MaxPLMOrder[Xeqs, #1, type] & /@ Yt;
		ni = ni /. DirectedInfinity[-1] -> -1;
		Yeqs = MapThread[ PseudoLinearMap[#1, {steq, #2+1}]&, {Thread[Yt == h], ni}]
	];
	YeqsOrder = (First/@Yeqs) /. { Derivative[n_][y_][t] -> y[t], y_[t+n_.]->y[t] }; 
		(* Only Shift and Derivative here. *)
	pos = Flatten[ Position[Yt, #]& /@ YeqsOrder ];
	{Yeqs[[pos]], First@Solve[Xeqs, Xt]}
]




Clear[StateSpaceToIO]
StateElimination[steq_]:= iStateElimUlle[steq][[2]] 
(*For computing Transfer funtion only, shoulde be replaced later!*)

IO[steq_StateSpace] := StateSpaceToIO[steq]

StateSpaceToIO::output = "Output not defined."; 

StateSpaceToIO[steq:StateSpace[f_, Xt_, Ut_, t_, {}, {}, type_,___]] := 
	With[{}, Message[ StateSpaceToIO::output ]; {}]

StateSpaceToIO[steq:StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_,___], opts___] := 
Module[ {met, Yeqs, Xrepl},
    met = Method /. {opts} /. Options[ StateSpaceToIO ];
	If[ met === Automatic,
		{Yeqs, Xrepl} = iStateElimUlle[steq],
		{Yeqs, Xrepl} = iStateElimClaude[steq]
	];
	IO[ Simplify[ Rule@@@(Yeqs /. Xrepl) ], Ut, Yt, t, type]
] (* End Module *)

StateSpaceToIO[ ioeq:IO[{__Equal}, __ ]] :=  StateSpaceToIO[ ModelToRules[ioeq]]
(*I don't remember why it is necessary. Probably not necessary at all *)

Options[StateSpaceToIO] = { Method -> Automatic};


(* ::Subsection:: *)
(*Realization*)


StateDifferentialsAdjoint[ioeq:IO[eqs_, Ut_, Yt_, t_, type_ ]] := 
Module[{K, adK, PQ, adPQ, omegaij, dx},
	K = DefineOreRing[Z, ioeq];
	adK = DefineAdjointOreRing[Z, ioeq];
	PQ = MapThread[Join, FromIOToOreP[ioeq]];
	adPQ = Map[Adjoint[#, adK]&, PQ, {2}];
	adPQ = Map[ If[Head[#]===OreP, Drop[ List@@OreP/@#,-1], {}]&, adPQ, {2}];
	omegaij = Transpose/@ PadLeft/@ adPQ;
	dx = Rest @ FoldList[ OreMultiply[ OreP[1,0], #1, K] + #2&, 0, #]& /@ omegaij;
	FromOrePToSpanK[ Join@@dx, ioeq]
]


StateDifferentialsLeftQuotient[ioeq:IO[eqs_, Ut_, Yt_, t_, type_], bwop___] := Module[{K, x, ni, PQ, PQsc},
	K = DefineOreRing[x, ioeq, bwop];
	ni = MaxPLMOrder[eqs, #, type]& /@ Yt;
	PQ = MapThread[Join, FromIOToOreP[ioeq]];
	PQsc = MapThread[ Reverse@Rest@NestList[ CutAndShift[#, K]&, #1, #2]&, {PQ, ni}];
	FromOrePToSpanK[ Join @@ PQsc, ioeq]
] (* by abuse of notation we have defined CutAndShift operator for cont-time systems too, using left quotient.*)


StateSpace[ioeq_IO, args___] := First @ Realization[ioeq, args]


Realizability[ ioeq:IO[ eqs:{__Rule}, Ut_, Yt_, t_, type_ ], opts___Rule ] := 
Module[ { s, subsp, met, shi, difstate, res }, 
    s = MaxPLMOrder[ eqs, Ut, type ];
    shi = PrintInfo /. {opts} /. Options[ Realizability ];
    met = Method /. {opts} /. Options[ Realizability ];
	Which[ 
		met === Automatic && type === Shift,          met = LeftQuotient,
		met === Automatic && type === TimeDerivative, met = SubspaceH
	];
	If[type === Shift          && met === SubspaceS, Print["Uses SequenceD!"]; met = SubspaceD];
	If[type === TimeDerivative && met === SubspaceD, Print["Uses SequenceS!"]; met = SubspaceS];
	If[type === TimeDerivative && met === SubspaceI, Print["Uses SequenceH!"]; met = SubspaceH];
	res = Switch[ met,
		SubspaceD,    Involutivity[  Last[ SequenceD[ioeq, s+2]]], 
		SubspaceI,    Integrability[ Last[ SequenceI[ioeq, s+2]]], 
		SubspaceH,    Integrability[ Last[ SequenceH[ioeq, s+2]]],
		LeftQuotient, Integrability[ SimplifyBasis[ StateDifferentialsLeftQuotient[ ioeq ]]],
		Adjoint,      Integrability[ SimplifyBasis[ StateDifferentialsAdjoint[ ioeq ]]]
	];
	If[ !res && shi, PrintNonIntegrable[met, s+2]];
	res
]
(* Dont remove, neccessary for webMathematica *)
Realizability[ ioeq:IO[{__Equal}, __ ], opts___Rule ] :=  Realizability[ ModelToRules[ioeq], opts]

Options[ Realizability ] = { Method -> Automatic, PrintInfo -> False };


Realization::nonrealizable = "State-space representation does not exist for the i/o equations.";
Realization::unable = "State-space representation cannot be computed for the i/o equations.";

Realization[ ioeq:IO[ eqs:{__Rule}, Ut_, Yt_, t_, type_], stvar_, opts___Rule ] := 
Module[{s, met, dx, states, result, unabletointegrate},
    met = Method /. {opts} /. Options[ Realization ];
	Which[ 
		met === Automatic && type === Shift,  met = LeftQuotient,
		met === Automatic,                    met = SubspaceH
	];
	dx = Switch[ met, 
		SubspaceH,    s = MaxPLMOrder[eqs, Ut, type]; Last[SequenceH[ ioeq, s+2]],
		LeftQuotient, SimplifyBasis[ StateDifferentialsLeftQuotient[ ioeq ]],
		Adjoint,      SimplifyBasis[ StateDifferentialsAdjoint[ ioeq ]]
	]; (* End Which *)
	states = IntegrateOneForms[dx];
	result = Switch[ states,
		{}, Message[Realization::nonrealizable]; {}, (*Nonintegrable H_ {s+2}*)
		IntegrateOneForms[_],  Message[Realization::unable]; unabletointegrate,
		_, Realization[ioeq, stvar, states]
	];
	result /; result =!= unabletointegrate
]


(* Actual realization, assumes state variables are given. *)
Realization[ ioeq:IO[ eqs:{_Rule..}, Ut_, Yt_, t_, type_], stvar_, states_List, opts___Rule ] := 
Module[{ni, Xt, Ytshifts, solvars, classicinvrule, f, h},
	ni = MaxPLMOrder[eqs, #, type]& /@ Yt;
	Xt = NewVariables[stvar, Plus @@ ni, t ];
	Ytshifts = MapThread[ Table[ PseudoLinearMap[#1, {t,i}, type], {i,0,#2}]&, {Yt,ni} ]//Flatten;
	solvars = Select[ Ytshifts, !FreeQ[states,#]& ];
	classicinvrule = Simplify[ First[ Solve[ Xt == states, solvars ]]];
	f = Simplify[ PseudoLinearMap[states, t, type] //. Join[eqs, classicinvrule]];
	h = Yt /. classicinvrule;
	StateSpace[f, Xt, Ut, t, h, Yt, type, StateDefinitions->Thread[Xt->states] ]
]

(* Dont remove, neccessary for web *)
Realization[ ioeq:IO[{__Equal}, __ ], args__ ] :=  Realization[ ModelToRules[ioeq], args]

Options[ Realization ] = { Method -> Automatic, PrintInfo -> False }


(* ::Subsection::Closed:: *)
(*ClassicTransform*)


ClassicTransformability[ genEq:StateSpace[f_, Xt_, Ut_, t_, _,_,type_ ], opts___] :=
Module[{ alpha, met, shi, res },
	alpha = MaxPLMOrder[ genEq, Ut, type ];
	met = Method /. {opts} /. Options[ ClassicTransformability ];
	shi = PrintInfo /. {opts} /. Options[ ClassicTransformability ];
	res = Switch[ met,
		SubspaceD, Involutivity[ Last[ SequenceD[genEq, alpha+2]]], 
		SubspaceI, Integrability[ Last[ SequenceI[genEq, alpha+2]]], 
		_, Integrability[ Last[ SequenceH[genEq, alpha+2]]]
	];
	If[ !res && shi, PrintNonIntegrable[met, alpha+2]];
	res
] (* End Module *)

Options[ ClassicTransformability ] = { Method -> SubspaceH, PrintInfo -> False };


ClassicTransform[ genEq:StateSpace[f_, Xt_, Ut_, t_, _, _, type_ ], stvar_] := 
	Lower[ genEq, stvar, LoweringOrder -> MaxPLMOrder[f, Ut, type ] ]


Lower[ genEq:StateSpace[f_, Xt_, Ut_, t_, _, _, type_ ], stvar_, opts___Rule] := 
	Module[ { low, alpha, beta, Hk, dphi, transf, result, Utshiftpat, unabletointegrate },
	low = LoweringOrder /. {opts} /. Options[Lower];
	alpha = MaxPLMOrder[ f, Ut, type];
	beta = If[low === Automatic, 0, alpha-low ];
	Hk = SequenceH[ genEq, alpha-beta+2 ];
	dphi = If[ low === Automatic,
		First@Select[ Reverse[Hk], Integrability, 1 ],
		Last[Hk]
    ];
	transf = IntegrateOneForms[dphi];
	result = Which[
		transf === {},
			{},
		Head[ transf ] === IntegrateOneForms,
			unabletointegrate,
		True,
			Utshiftpat = Join[Ut, PseudoLinearMap[Ut, {t, _}, type]];
			transf = DeleteCases[ transf, Alternatives @@ Utshiftpat, {1} ];
			ChangeOfVariables[ genEq, stvar, transf ]
	];  (* End Which *)
	result /; result =!= unabletointegrate
] (* End Module*)

Options[ Lower ] = { LoweringOrder -> Automatic };




(* ::Subsection::Closed:: *)
(*TransferFunction*)


TransferFunction::output = "Output not defined.";


TransferFunction[x_, F_, u_[t_], yt_, t_, type_ ]    := TransferFunction[x, F, {u[t]}, yt, t, type ]
TransferFunction[x_, F_, Ut_List, y_[t_], t_, type_] := TransferFunction[x, F, Ut, {y[t]}, t, type ]
TransferFunction[z_, tf:Except[_?MatrixQ], Ut_List, Yt_List, t_Symbol, type_] := 
	TransferFunction[z, {tf}, Ut, Yt, t, type]

TransferFunction[ x_, steq:StateSpace[f_, Xt_, Ut_, t_, {}, {}, type_]] := With[{}, Message[TransferFunction::output];{}]

TransferFunction[ x_, steq:StateSpace[f_, Xt_, Ut_, t_, h_, Yt_, type_]]:=
Module[{Amat, Bmat, Cmat, Dmat, K, M, invM, transf, Xtexpr },
	Amat = Outer[ OreP[D[#1,#2]]&, f, Xt];
	Bmat = Outer[ OreP[D[#1,#2]]&, f, Ut];
	Cmat = Outer[ OreP[D[#1,#2]]&, h, Xt];
	Dmat = Outer[ OreP[D[#1,#2]]&, h, Ut];
	K = DefineOreRing[x, steq]; (* must be defined using steq.  *)
	M = DiagonalMatrix[ Table[ OreP[1,0], {Length[Xt]}]] - Amat;
	invM = OreInverseMatrix[M, K];
	transf = OreDot[Cmat, invM, Bmat, K];
	transf = RemoveDoubleFractions[transf, K];
	Xtexpr = StateElimination[steq];
	TransferFunction[ x, Simplify[ transf /. Xtexpr ], Ut, Yt, t, type ]
] /; ClassicStateQ[steq]

TransferFunction[x_, ioeq:IO[{__Equal}, Ut_, Yt_, t_, type_]] := TransferFunction[x, ModelToRules[ioeq]]
(* This is for webMath *)

TransferFunction[x_, ioeq:IO[{__Rule}, Ut_, Yt_, t_, type_]]:=
Module[{P, Q, K, invP, transf },
	{P, Q} = FromIOToOreP[ioeq];
	K = DefineOreRing[x, ioeq];
	invP = OreInverseMatrix[P, K];
	transf = OreDot[invP, -Q, K];
	TransferFunction[x, RemoveDoubleFractions[transf, K], Ut, Yt, t, type ]
]


TransferFunction /: BookForm[ TransferFunction[x_Symbol, mat_?MatrixQ, Ut_, Yt_, t_, type_], opts___] := 
	BookForm[mat, DefineOreRing[x, t, type], opts]


(* ::Subsection:: *)
(*GUI functions*)


ReductionGUI[ioeq0_, opts___] := Module[{ioeq, res, result, reducible, toAdd},
	ioeq = ConvertToSubscripted[ioeq0];
	reducible = Check[res = Reduction[ioeq]; True, False, {Reduction::irred}];
	res = If[ reducible,
		{"The system is reducible, the reduced system is:", res},
		{"The system is irreducible."}
	];
	GUIFormat[ Join[{"System equations:", ioeq}, res] ]
]


RealizationGUI[ioeq0_, x_, opts___] := Module[{ioeq, res, comment1, comment2},
	ioeq = ConvertToSubscripted[ioeq0];
	comment1 = "I/o equation(s):";
	res = Realization[ioeq, x]; 
	comment2 = If[ Head[res] === StateSpace,
		"The system is realizable, state equations are:",
		"The system is not realizable."
	];
	GUIFormat[{comment1, ioeq, comment2, res}]
]


StateSpaceToIOGUI[steq0_StateSpace] := Module[{steq, ioeq},
	steq = ConvertToSubscripted[steq0];
	ioeq = StateSpaceToIO[steq];
	GUIFormat[ {"State space equations:", steq, "Input/output equation(s)", ioeq}]
]


TransferFunctionGUI[s_, sys0:_[_,_,_,t_,___]] := Module[{sys, tf},
	sys = ConvertToSubscripted[sys0];
	tf = TransferFunction[s,sys];
	GUIFormat[{"System equation(s):", sys, "Transfer function:", tf}]
]


(* ::Subsection::Closed:: *)
(*End package*)


On[General::spell1]
On[General::spell]

End[]

Protect[{}
]

EndPackage[]
